import React from "react";
import {wrapper} from "../../../../features/store";
import Link from "next/link";
import {requestCommunityNoticePageData} from "../../../../features/pageData/boardPage/communityCategory";
import {setBoardIndex, setBoardPostListAndPageInfoState} from "../../../../features/pageData/boardPage/actions";
import ErrorServerDown from "../../../500.ServerDown";
import _ from "underscore";
import {checkNumeric} from "../../../../utils/format";
import {SiteIndex} from "../../../../utils/index/siteIndex";
import style from "./notice.module.scss"


import PostBoard from "../../../../components/board/board";
import {AD_728X90} from "../../../../components/ad/index";
import LeftSidebar from "../../../../components/sideBar/left-sidebar";
import RightSidebar from "../../../../components/sideBar/right-sidebar";
import ArticleList from "../../../../components/board/modules/Panel.articleList";
import {BOARD_LIST_TYPE} from "../../../../utils/index/board";



export default function Notice({isServerDown, status}) {
    if (isServerDown) return <ErrorServerDown/>;



    return (
        <>
            <LeftSidebar/>

            <main className={style["container"]}>
                <section>
                    <div className={style["title"]}>
                        <Link href={`/board/${SiteIndex.Community.code}/${SiteIndex.Community.boards.Notice.code}`}>
                            <a>
                                <h1>{SiteIndex.Community.boards.Notice.fullName}</h1>
                            </a>
                        </Link>
                    </div>
                </section>

                <section>
                    <PostBoard category={SiteIndex.Community.code} board={SiteIndex.Community.boards.Notice.code}/>
                </section>
            </main>

            <RightSidebar/>
        </>
    )
}




export const getServerSideProps = wrapper.getServerSideProps(async (context) => {
    const {dispatch} = context.store;
    const {query} = context;

    const pageToSearch = (!_.isEmpty(query) && checkNumeric(query.page) && Number(query.page)>0)? query.page: 1;
    const listType = (!_.isEmpty(query) && query.list)? query.list: undefined;


    /**
     * Set board page index state
     * */
    dispatch(setBoardIndex(SiteIndex.Community.code, SiteIndex.Community.boards.Notice.code, BOARD_LIST_TYPE.NOTICE));


    /**
     * Request page data
     * */
    const {status, board: boardData, isServerDown} = await requestCommunityNoticePageData(pageToSearch, BOARD_LIST_TYPE.NOTICE, context);


    switch (status) {
        case 200:
            const {postList, noticePostList, searchedPage, totalPageCount, postPerPage} = boardData;
            dispatch(setBoardPostListAndPageInfoState(postList, noticePostList, searchedPage, totalPageCount, postPerPage));
            break;
    }



    return {
        props: {
            isServerDown: isServerDown,
            status: status,
        }
    }
});