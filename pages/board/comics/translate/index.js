import React, {useRef, useEffect} from "react";
import Link from "next/link";
import ErrorServerDown from "../../../500.ServerDown";
import {SiteIndex} from "../../../../utils/index/siteIndex";
import style from "./translate.module.scss"
import ComicsList from "../../../../components/comics/comicsList/comicsList";
import ComicsSeriesRegisterPanel from "../../../../components/comics/comicsList/modules/Panel.comicsSeriesRegister";
import LeftSidebar from "../../../../components/sideBar/left-sidebar";
import RightSidebar from "../../../../components/sideBar/right-sidebar";
import {wrapper} from "../../../../features/store";
import * as _ from "underscore";
import {checkNumeric} from "../../../../utils/format";
import {setBoardIndex, setBoardPostListAndPageInfoState} from "../../../../features/pageData/boardPage/actions";
import {requestComicsFreeboardPageData} from "../../../../features/pageData/boardPage/comicsCategory";
import DocHead from "../../../../components/meta/head";
import {requestComicsTranslatePageData} from "../../../../features/pageData/actions";
import {
    setComicsListPageState,
    setComicsListState,
    setComicsSearchTextState
} from "../../../../features/comics/actions";



const category = SiteIndex.Comics.code;
const board = SiteIndex.Comics.boards.Translate.code;



export default function Freeboard({isServerDown, status}) {
    if (isServerDown) return <ErrorServerDown/>;

    const comicsListRef = useRef(null);


    return (
        <>
            <DocHead title={`미만웹 | ${SiteIndex.Comics.boards.Translate.fullName}`}/>

            <LeftSidebar/>

            <div className={style["container"]}>
                <main>
                    {/** BOARD TITLE */}
                    <section className={style["board_title"]}>
                        <Link href={SiteIndex.Comics.boards.Translate.path}>
                            <a className={style["title"]}>
                                <div>
                                    {/*<p className={style["site_logo"]}>MIMANWEB</p>*/}
                                    <p className={style["board_title"]}>{SiteIndex.Comics.boards.Translate.fullName}</p>
                                </div>
                            </a>
                        </Link>
                    </section>


                    {/** HOW TO USE */}
                    <section className={style["how_to_use"]}>
                        <blockquote>
                            <span>번역 작품은 기본적으로 국내 출판사들을 통해 <u>정식 발매 될 가망이 없는</u> 작품들을 게시합니다.</span>
                            <span>작품의 장르가 마이너하고 매니악해서 내가 아니면 국내 독자들에게 알려지지 않을 작품들을 퀄리티 있게 번역한다는 취지로 부탁드립니다.</span>
                        </blockquote>
                        <blockquote>
                            <span>번역하고자 하는 코믹스를 <b>먼저 검색</b>하고, <b>없으면 새로 등록</b>합니다.</span>
                            {/*<span>등록된 코믹스는 게시 적합 여부를 판단 후 번역이 가능해집니다. (등록한 분에겐 쪽지 알림.)</span>*/}
                            <span>작품 번역시 [번역가] 칭호를 얻습니다.</span>
                            <span>번역 작업한 모든 작품의 업적이 각인됩니다.</span>
                        </blockquote>
                        <blockquote>
                            <span>추후 자기 작품이 국내 출판사에서 정식 발매되거나 항의로 인해 게시 중지될 수 있습니다.</span>
                            <span>너무 기분 나빠하지 말아주세요.</span>
                            <span>게시 중지 된 작품은 본인만 열람 가능해 집니다.</span>
                            <span>본인으로 인해 국내에 작품이 알려져 출시 된다면, 그 또한 큰 업적으로 적립됩니다.</span>
                        </blockquote>
                    </section>



                    {/** COMICS SERIES SEARCH */}
                    {/** REGISTER NEW COMICS SERIES */}
                    <section>
                        {/*<section>*/}
                        {/*    <ComicsSeriesSearchPanel/>*/}
                        {/*</section>*/}

                        <section>
                            <ComicsSeriesRegisterPanel comicsListRef={comicsListRef}/>
                        </section>
                    </section>

                    {/** COMICS TABLE OF INDEX */}
                    <section>
                        <ComicsList ref={comicsListRef}/>
                    </section>
                </main>
            </div>

            <RightSidebar/>
        </>
    );
}


export const getServerSideProps = wrapper.getServerSideProps(async (context) => {
    const {dispatch} = context.store;
    const {query} = context;


    /**
     * Request page data
     * */
    const {status, comicsList, totalCount, itemsPerPage, pageToSearch, isServerDown} = await requestComicsTranslatePageData(context);


    switch (status) {
        case 200:

            dispatch(setComicsListPageState(comicsList, itemsPerPage, totalCount, pageToSearch));
            break;
    }



    return {
        props: {
            isServerDown: isServerDown,
            status: status,
        }
    }
});