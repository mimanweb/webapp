import React from "react";
import {wrapper} from "../features/store";
import {useDispatch, useSelector, useStore} from 'react-redux';
import ErrorServerDown from "./500.ServerDown";
import {Ad_336x280} from "../components/ad/index";


// Style
import style from './index.module.scss';


// Actions
import {requestHomepageData} from "../features/pageData/actions"


// Components
import LeftSidebar from "../components/sideBar/left-sidebar";
import RightSidebar from "../components/sideBar/right-sidebar";
import ComicsCard, {COMICS_CARD_TYPE} from "../components/card/comicsCard";
import HomepageArticleListPanel from "../components/homepage/Panel.articleList";
import HomepageNoticeArticle from "../components/homepage/Panel.noticeArticle";
import {setHomepageState} from "../features/homepage/actions";
import DocHead from "../components/meta/head";





export default function Home({status, isServerDown}) {
    if (isServerDown) return <ErrorServerDown/>;


    const homepageState = useSelector(state => state.homepage);

    const {noticePost, bestHumorPostList, bestCombinedBoardPostList, communityFreeboardRecentPostList, comicsFreeboardRecentPostList, recentComicBookList} = homepageState;


    return (
        <>
            <DocHead desc="미국 만화/애니 및 마이너한 매니아들을 위한 아지트, 미만웹(MIMANWEB), Minority Maniacs Webzine입니다."/>

            <LeftSidebar/>

            <main className={style["container"]}>
                {/** FIRST SECTION */}
                <section>

                    {/** LEFT SECTION, BEST COMICS */}
                    <section>

                        {/** TOP SECTION, COMICS CARD, BIG CARDS */}
                        <section>
                            {recentComicBookList.slice(0, 3).map((comicBook, key) => {
                                return (
                                    <ComicsCard
                                        key={key}
                                        type={COMICS_CARD_TYPE.SMALL}
                                        comicBook={comicBook}/>
                                )
                            })}
                        </section>

                        {/** BOTTOM SECTION, COMICS CARD, SMALL CARDS */}
                        <section>
                            {recentComicBookList.slice(3, 6).map((comicBook, key) => {
                                return (
                                    <ComicsCard
                                        key={key}
                                        type={COMICS_CARD_TYPE.SMALL}
                                        comicBook={comicBook}/>
                                )
                            })}
                        </section>
                    </section>

                    {/** RIGHT SECTION, BEST POSTS */}
                    <section>
                        {/** SITE NOTICE SECTION */}
                        <section className={style["notice"]}>
                            {noticePost && (
                                <HomepageNoticeArticle postData={noticePost}/>
                            )}
                        </section>

                        {/** BEST POST SECTION */}
                        <section className={style["best_post"]}>
                            <div className={style["title"]}>인기글 <span>BEST</span></div>
                            <HomepageArticleListPanel postList={bestHumorPostList} max={8} showBoardTag={true} setMinHeight={true}/>
                            <div className={style["separator"]}/>
                            <HomepageArticleListPanel postList={bestCombinedBoardPostList} max={8} showBoardTag={true} setMinHeight={true}/>
                        </section>
                    </section>
                </section>


                {/** SECOND SECTION */}
                <section>

                    {/** AD */}
                    <section>
                        <Ad_336x280/>
                    </section>

                    {/**  */}
                    <section>
                        <section>
                            <div className={style["title"]}>자유게시판</div>
                            <HomepageArticleListPanel postList={communityFreeboardRecentPostList} max={8} setMinHeight={true}/>
                        </section>

                        <section>
                            <div className={style["title"]}>코믹스잡담</div>
                            <HomepageArticleListPanel postList={comicsFreeboardRecentPostList} max={8} setMinHeight={true}/>
                        </section>
                    </section>
                </section>
            </main>

            <RightSidebar/>
        </>
    )
}


export const getServerSideProps = wrapper.getServerSideProps(async (context) => {
    const {dispatch} = context.store;

    const {status, isServerDown, homepageData} = await requestHomepageData(context);


    switch (status) {
        case 200:
            dispatch(setHomepageState(homepageData));
            break;
    }

    return {
        props: {
            isServerDown: isServerDown,
            status: status,
            homepageData: homepageData
        }
    }
})


