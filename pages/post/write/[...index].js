import React, {useEffect, useState} from "react";
import {wrapper} from "../../../features/store";
import {useRouter} from "next/router"
import {useSelector, useDispatch} from "react-redux";
import Error404 from "../../404";
import ErrorServerDown from "../../500.ServerDown";
import {findBoardMapItem, findCategoryMapItem} from "../../../utils/index/siteIndex";
import LocalStorage_ from "../../../utils/index/localStorage"
import {setPostEditMode} from "../../../features/post/actions";
import POST_ from "../../../utils/index/post";
import {requestPostEditPageData} from "../../../features/pageData/actions";



/** ACTIONS */
import {requestLoginVerify} from "../../../features/user/actions";
import {
    setTempPostState,
    requestCreateTempPost,
    requestPreviouslySavedTempPost,
    setPostOwnerInfo,
    setEditPostInitState
} from "../../../features/post/actions";


/** STYLE */
import style from "./index.module.scss"


/** COMPONENTS */
import PostWriter from "../../../components/post/writer";
import LeftSidebar from "../../../components/sideBar/left-sidebar";
import RightSidebar from "../../../components/sideBar/right-sidebar";



export default function PostWrite({isServerDown, isInvalidIndexRequested}) {
    if (isServerDown) return <ErrorServerDown/>;
    if (isInvalidIndexRequested) return <Error404/>;


    const dispatch = useDispatch();
    const router = useRouter();

    const category = router.query.index[0];
    const board = router.query.index[1];


    const userState = useSelector(state => state.user);
    const postState = useSelector(state => state.post);

    const {editMode} = postState;




    /**
     * On page load,
     * Retrieve previously saved temp-post data
     * If none, save a new one
     * */
    useEffect(() => {

        const fetchAndSetTempPostData = async () => {
            const prevTempPost = await requestPreviouslySavedTempPost(undefined, category, board);

            /**
             * If previously saved temp-post data exists
             * */
            let postData;

            if (prevTempPost) {
                postData = prevTempPost;
            }
            else {
                postData = await requestCreateTempPost(category, board);
            }

            dispatch(setTempPostState(category, board, postData));

            if (userState.loggedIn) {
                dispatch(setPostOwnerInfo(userState.nickname, true));
            } else {
                const unregUserId = localStorage.getItem(LocalStorage_.UnregUser.NICKNAME) || null;
                const unregUserPw = localStorage.getItem(LocalStorage_.UnregUser.PW) || null;

                dispatch(setPostOwnerInfo(unregUserId, unregUserPw));
            }
        }


        if (editMode===POST_.EDIT_MODE.WRITING) fetchAndSetTempPostData();
    }, [editMode])



    return (
        <>
            <LeftSidebar/>

            <div className={style["container"]}>
                <section className={style["first"]}>
                    <PostWriter/>
                </section>
            </div>

            <RightSidebar/>
        </>
    )
}


export const getServerSideProps = wrapper.getServerSideProps(async (context) => {

    const isPostNumberIndex = context.query.index.length===3;

    const index = {
        category: context.query.index[0],
        board: context.query.index[1],
        postNumber: isPostNumberIndex? context.query.index[2]: null,
        postPassword: context.query.password
    }

    const isValidIndexCategory = findCategoryMapItem(index.category);
    const isValidIndexBoard = findBoardMapItem(index.category, index.board);


    /**
     * New post writing mode
     * */
    if (isValidIndexCategory && isValidIndexBoard && !isPostNumberIndex) {
        await requestLoginVerify(context);

        context.store.dispatch(setPostEditMode(POST_.EDIT_MODE.WRITING));
        return;
    }


    const {status, postData, isServerDown} = await requestPostEditPageData(index.postNumber, index.category, index.board, index.postPassword, context);

    if (isServerDown) return {props: {isServerDown: true}};


    /**
     * Check valid index
     * */
    if (
        !index.category ||
        !index.board ||
        !isValidIndexCategory ||
        !isValidIndexBoard ||
        status===400 ||
        status===403 ||
        status===404
    ) return {props: {isInvalidIndexRequested: true}};



    context.store.dispatch(setEditPostInitState(postData));
    context.store.dispatch(setPostEditMode(POST_.EDIT_MODE.EDIT));

})