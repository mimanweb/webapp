import React from "react";
import style from "./404.module.scss"
import Link from "next/link";

import SmallButton, {SmallButtonType} from "../components/button/small";


const Error404 = () => {

    return (
        <div className={style["container"]}>
            <section>
                <h1>404<br/>NOT<br/>FOUND</h1>

                <p>"존재하지 않는 페이지입니다."</p>

                <section className={style["buttons"]}>
                    <Link href="/">
                        <a className={style["to_home_button"]}>
                            <SmallButton buttonType={SmallButtonType.TO_HOME}/>
                        </a>
                    </Link>
                    <SmallButton buttonType={SmallButtonType.GO_BACK} onClick={()=>window.history.back()}/>
                </section>
            </section>
        </div>
    )
}



export default Error404;
