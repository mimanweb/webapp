import React from "react";
import ComicsWrite from "../../../write/comics/book";
import {wrapper} from "../../../../features/store";
import {requestComicBookEditPageData} from "../../../../features/pageData/actions";
import {setComicsState} from "../../../../features/comics/actions";
import {
    setComicBookState,
    setComicsBookEditMode,
    setUserPublishedComicBooksState
} from "../../../../features/comicsBook/actions";
import {setPostEditMode, setTempPostState} from "../../../../features/post/actions";
import POST_ from "../../../../utils/index/post";
import {setEditorDisplayUnregUserInfo, setRouteOnEditorAutoSaveFail} from "../../../../features/editor/actions";
import {COMIC_BOOK_EDIT_MODE} from "../../../../utils/index/comics/book";


export default function ComicBookEdit({comicBookData, comicBookPostData}) {
    return <ComicsWrite/>
}




export const getServerSideProps = wrapper.getServerSideProps(async (context) => {
    const {dispatch} = context.store;

    const {id: comicBookDataId} = context.query;


    const {status, comicsData, comicBookData, comicBookPostData, isServerDown} = await requestComicBookEditPageData(comicBookDataId, context);


    switch (status) {
        case 200:
        case 203:
            dispatch(setComicsBookEditMode(COMIC_BOOK_EDIT_MODE.EDIT));
            dispatch(setComicsState(comicsData));
            dispatch(setComicBookState(comicBookData));

            dispatch(setTempPostState(null, null, comicBookPostData));
            dispatch(setPostEditMode(POST_.EDIT_MODE.EDIT));
            dispatch(setRouteOnEditorAutoSaveFail(false));
            dispatch(setEditorDisplayUnregUserInfo(false));
            break;
    }


    return {
        props: {
            status: status,
            isServerDown: isServerDown,
            comicBookData: comicBookData,
            comicBookPostData: comicBookPostData
        }
    }
})