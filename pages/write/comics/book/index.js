import React, {useRef} from "react";
import {wrapper} from "../../../../features/store";
import LeftSidebar from "../../../../components/sideBar/left-sidebar";
import RightSidebar from "../../../../components/sideBar/right-sidebar";
import style from "./comics_write.module.scss"
import ComicsEditionTitleEditPanel
    from "../../../../components/comics/comicsEditor/modules/Panel.editComicsEditionTitle";
import ComicsEpisodeTypeEditPanel from "../../../../components/comics/comicsEditor/modules/Panel.editComicsEpisodeType";
import ComicsEpisodesEditPanel from "../../../../components/comics/comicsEditor/modules/Panel.editComicsEpisodes";
import ComicsContentTypeSelectPanel from "../../../../components/comics/comicsEditor/modules/Panel.selectContentType";
import ComicsBookImageContentsManagePanel from "../../../../components/comics/comicsEditor/modules/Panel.manageComicsBookPages";
import {requestComicBookWritePageData} from "../../../../features/pageData/actions";
import {
    setComicBookState,
    requestPublishComicBookData,
    setUserPublishedComicBooksState, setComicsBookEditMode, requestUpdateComicBookData
} from "../../../../features/comicsBook/actions";
import {setComicsState} from "../../../../features/comics/actions";
import {setRouteOnEditorAutoSaveFail} from "../../../../features/editor/actions";
import {useSelector} from "react-redux";
import DocHead from "../../../../components/meta/head";
import {COMIC_BOOK_CONTENT_TYPE, COMIC_BOOK_EDIT_MODE} from "../../../../utils/index/comics/book";
import ComicBookLinkManagePanel from "../../../../components/comics/comicsEditor/modules/Panel.manageComicBookLink";
import {setPostEditMode, setTempPostState} from "../../../../features/post/actions";
import POST_ from "../../../../utils/index/post";
import PostWriter from "../../../../components/post/writer";
import SmallButton, {SmallButtonType} from "../../../../components/button/small";
import router, {useRouter} from "next/router";
import {StretchOpenMessageType, StretchOpenBoardType, StretchOpenMessageBoard} from "../../../../components/panel";



/**
 * COMICS VOLUME/ISSUE EDITOR
 * */
const ComicsWrite = ({status, isServerDown}) => {

    const comicsState = useSelector(state => state.comics);
    const comicBookState = useSelector(state => state.comicBook);
    const postState = useSelector(state => state.post);

    const {title: comicsTitle, comicsId, comicsNumber} = comicsState;
    const {contentType: comicBookContentType, isShortStory, comicBookDataId, isVolume, volume, issue, shortStoryNumber, publishedBooks, editMode, owner: comicBookOwner} = comicBookState;
    const {content: postContent, phrase: postPhrase} = postState;


    const isEditMode = editMode===COMIC_BOOK_EDIT_MODE.EDIT;


    const notRegistableMessageRef = useRef(null);



    const cancelButtonClick = async () => {
        await router.push(`/comics/book/${comicsNumber}/${comicsId}`)
    }

    const registerComicBookButtonClick = async () => {
        const longStoryBooks = publishedBooks.filter(book => !book.index.isShortStory);
        const booksWithVol = longStoryBooks.filter(book => book.index.isVolume);
        const booksWithOnlyIssue = longStoryBooks.filter(book => !book.index.isVolume);


        if (isShortStory) {
            // if (publishedShortStory) {
            //     notRegistableMessageRef.current.open(StretchOpenMessageType.COMIC_BOOK.REGISTER.SHORT_STORY_NOT_AVAILABLE);
            //     return;
            // }
        } else {
            if (!isVolume) {
                if (booksWithOnlyIssue.find(book => book.index.issue===issue)) {
                    notRegistableMessageRef.current.open(StretchOpenMessageType.COMIC_BOOK.REGISTER.ISSUE_NOT_AVAILABLE);
                    return ;
                }
            } else {
                if (booksWithVol.find(book => book.index.volume===volume && book.index.issue===issue)) {
                    notRegistableMessageRef.current.open(StretchOpenMessageType.COMIC_BOOK.REGISTER.VOLUME_ISSUE_NOT_AVAILABLE);
                    return;
                }
            }
        }



        const {status, data} = await requestPublishComicBookData(comicBookDataId, postContent, postPhrase);

        switch (status) {
            case 200:
                const {comicsId: _comicsId, comicsNumber: _comicsNumber, isShortStory, isVolume, volume, issue, shortStoryNumber, translator} = data;

                const query = {translator: translator};

                if (isShortStory) query["short-story"] = shortStoryNumber;
                if (!isShortStory) query.issue = issue;
                if (!isShortStory && isVolume) query.vol = volume;

                await router.push({
                    pathname: `/comics/book/${comicsNumber}/${comicsId}`,
                    query: query
                })
                break;
            case 403:
                alert("로그인 되어 있지 않거나, 작성 중이던 글이 이미 게시되었습니다.");

                await router.push({
                    pathname: `/comics/book/${comicsNumber}/${comicsId}`
                })
                break;
            case 404:
                alert("유효하지 않은 코믹스 타이틀/책 데이터입니다.");

                await router.push({
                    pathname: `/comics/book/${comicsNumber}/${comicsId}`
                })
                break;
        }
    }


    const confirmUpdateButtonClick = async () => {
        const {status} = await requestUpdateComicBookData(comicBookDataId, postContent, postPhrase);

        switch (status) {
            case 200:
                const query = {translator: comicBookOwner.userId};

                if (isShortStory) query["short-story"] = shortStoryNumber;
                if (!isShortStory) query.issue = issue;
                if (!isShortStory && isVolume) query.vol = volume;

                await router.push({
                    pathname: `/comics/book/${comicsNumber}/${comicsId}`,
                    query: query
                })
                break;
            case 403:
                alert("로그인 되어 있지 않거나, 작성 중이던 글이 이미 게시되었습니다.");

                await router.push({
                    pathname: `/comics/book/${comicsNumber}/${comicsId}`
                })
                break;
            case 404:
                alert("유효하지 않은 코믹스 타이틀/책 데이터입니다.");

                await router.push({
                    pathname: `/comics/book/${comicsNumber}/${comicsId}`
                })
                break;
        }
    }



    return (
        <>
            {isEditMode? (
                <DocHead title="번역 코믹스 수정 | 미만웹"/>
            ): (
                <DocHead title="번역 코믹스 등록 | 미만웹"/>
            )}


            <LeftSidebar/>

            <main className={style["container"]}>
                
                {/** COMIC BOOK INFO */}
                <section className={style["book_info"]}>
                    
                    {/** PAGE TITLE */}
                    <section className={style["page_title"]}>
                        {isEditMode? (
                            <p>번역 코믹스 수정</p>
                        ): (
                            <p>번역 코믹스 등록</p>
                        )}

                    </section>


                    {/** COMICS TITLE */}
                    <section className={style["comics_title"]}>
                        <p>{comicsTitle.korean}</p>
                        <p>{comicsTitle.origin}</p>
                    </section>


                    {/** EDITION TITLE */}
                    <section className={style["edition_title"]}>
                        <ComicsEditionTitleEditPanel/>
                    </section>


                    {/** EPISODE TYPE */}
                    <section className={style["episode_type"]}>
                        <ComicsEpisodeTypeEditPanel/>
                    </section>


                    {/** EPISODE SECTION */}
                    {!isShortStory && (
                        <section className={style["episodes"]}>
                            <ComicsEpisodesEditPanel/>
                        </section>
                    )}


                    {/** CONTENT TYPE SELECTION */}
                    <section className={style["content_type"]}>
                        <ComicsContentTypeSelectPanel/>
                    </section>


                    {/** BOOK PAGE CONTENT SECTION */}
                    <section className={style["content"]}>
                        {comicBookContentType===COMIC_BOOK_CONTENT_TYPE.IMAGE && (
                            <ComicsBookImageContentsManagePanel/>
                        )}
                        {comicBookContentType===COMIC_BOOK_CONTENT_TYPE.LINK && (
                            <ComicBookLinkManagePanel/>
                        )}

                    </section>

                </section>


                {/** COMIC BOOK TRANSLATOR REVIEW POST */}
                <section className={style["translator_review"]}>
                    <section className={style["title"]}>
                        <div>번역자 후기</div>
                    </section>

                    <section className={style["editor"]}>
                        <PostWriter hideTitle={true} hideSubmitButtons={true}/>
                    </section>

                </section>


                {/** SUBMIT BUTTONS */}
                <section className={style["submit_buttons"]}>
                    <section>
                        <SmallButton buttonType={SmallButtonType.COMIC_BOOK.CANCEL_WRITING} onClick={cancelButtonClick}/>
                        {isEditMode? (
                            <SmallButton buttonType={SmallButtonType.COMIC_BOOK.REGISTER_EDIT} onClick={confirmUpdateButtonClick}/>
                        ): (
                            <SmallButton buttonType={SmallButtonType.COMIC_BOOK.REGISTER} onClick={registerComicBookButtonClick}/>
                        )}
                    </section>

                    <section>
                        <StretchOpenMessageBoard boardType={StretchOpenBoardType.COMIC_BOOK.REGISTER.NOT_AVAILABLE} ref={notRegistableMessageRef}/>
                    </section>
                </section>


            </main>

            <RightSidebar/>
        </>
    )
}



export default ComicsWrite;





export const getServerSideProps = wrapper.getServerSideProps(async (context) => {
    const {dispatch} = context.store;

    const {id: comicsDataId} = context.query;

    const {status, comicsData, comicBookData, comicBookPostData, publishedComicBooks, isServerDown} = await requestComicBookWritePageData(comicsDataId, context);

    switch (status) {
        case 200:
        case 203:
            dispatch(setComicsBookEditMode(COMIC_BOOK_EDIT_MODE.WRITE));
            dispatch(setComicsState(comicsData));
            dispatch(setComicBookState(comicBookData));
            dispatch(setUserPublishedComicBooksState(publishedComicBooks));

            dispatch(setTempPostState(null, null, comicBookPostData));
            dispatch(setPostEditMode(POST_.EDIT_MODE.WRITING));
            dispatch(setRouteOnEditorAutoSaveFail(false));
            break;
    }


    return {
        props: {
            status: status,
            isServerDown: isServerDown,
            comicBookData: comicBookData,
            comicBookPostData: comicBookPostData
        }
    }
});