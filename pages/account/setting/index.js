import {wrapper} from "../../../features/store";
import React, {useState, useEffect} from "react";
import {useSelector} from "react-redux";
import {useRouter} from "next/router";
import ErrorServerDown from "../../500.ServerDown";
import router from "next/router";


// Style
import style from "./setting.module.scss"


// Actions
import {requestLoginVerify} from "../../../features/user/actions";
import {requestUpdateUserInfo} from "../../../features/account/actions";
import LeftSidebar from "../../../components/sideBar/left-sidebar";
import RightSidebar from "../../../components/sideBar/right-sidebar";
import XSmallButton, {XSmallButtonType} from "../../../components/button/x-small";


const Form = () => {
    const router = useRouter();
    const user = useSelector(state => state.user);


    const [password, setPassword] = useState("");
    const [newPassword, setNewPassword] = useState("");
    const [nickname, setNickname] = useState(user.nickname);


    const nicknameUpdatedTime = new Date(user.nicknameUpdatedTime)
    const today = new Date();
    const thisMonday = new Date();
    thisMonday.setDate(today.getDate() - (7 - today.getDay()) + 1) // +1 day because default week starts from sunday(0)
    thisMonday.setHours(0);
    thisMonday.setMinutes(0);
    thisMonday.setSeconds(0);

    const nicknameChangeable = thisMonday > nicknameUpdatedTime


    const submit = async (e) => {
        e.preventDefault();
        if (!password) return alert("현재 비밀번호를 입력해 주세요.");
        if (!nickname) return alert("닉네임을 입력해 주세요.");


        const result = await requestUpdateUserInfo(password, newPassword, nickname);
        if (!result.success) return alert(result.msg);

        alert("계정 정보를 수정하였습니다.");
        await router.push("/");
    }


    return (
        <form onSubmit={submit}>
            <h3>계정 정보 수정</h3>
            <br/>

            <blockquote>
                <p>"아이디(ID)와 이메일(email)은 계정 고유 정보로서, 변경될 수 없습니다."</p>
                <p>"닉네임은 주 1회 변경할 수 있습니다."</p>
                <p>"비밀번호 변경시 모든 기기에서 로그아웃됩니다."</p>
            </blockquote>
            <br/>

            <div>
                <p>아이디</p>
                <div/>
                <input className={style["id"]} type="text" value={user.id} readOnly={true}/>
            </div>
            <div>
                <p>현재 비밀번호</p>
                <div/>
                <input type="password" autoComplete="new-password" onChange={(e) => setPassword(e.target.value)}/>
            </div>
            <div>
                <p>새 비밀번호</p>
                <div/>
                <input type="password" onChange={(e) => setNewPassword(e.target.value)}/>
            </div>
            <div>
                <p>닉네임</p>
                <div/>
                <input
                    type="text"
                    className={(!nicknameChangeable? style["readonly-input"]: null)}
                    value={nickname}
                    onChange={(e) => setNickname(e.target.value)}
                    readOnly={!nicknameChangeable}
                />
                {!nicknameChangeable && (
                    <small>&nbsp;&nbsp;다음 주에 변경해 주세요.</small>
                )}
            </div>
            <div>
                <p>이메일</p>
                <div/>
                <input className={style["email"]} type="text" value={user.email} readOnly={true}/>
            </div>

            <input type="submit" value="수정하기"/>
        </form>
    )
};


export default function Setting({isServerDown}) {
    if (isServerDown) return <ErrorServerDown/>;


    const user = useSelector(state => state.user);


    return (
        <>
            <LeftSidebar/>

            <div className={style["container"]}>
                <h2>계정 설정</h2>
                <br/>

                {user.loggedIn? (
                    <>
                        <Form/>

                        <section className={style["user_unregister"]}>
                            <XSmallButton buttonType={XSmallButtonType.USER.UNREGISTER} onClick={()=>router.push("/account/unregister")}/>
                        </section>
                    </>
                ): (
                    <blockquote>
                        <p>"로그인 되어 있지 않습니다."</p>
                    </blockquote>
                )}
            </div>

            <RightSidebar/>
        </>
    )
}


export const getServerSideProps = wrapper.getServerSideProps(async (context) => {
    const {isServerDown} = await requestLoginVerify(context);

    return {
        props: {
            isServerDown: isServerDown
        }
    }
})