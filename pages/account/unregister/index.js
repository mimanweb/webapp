import {wrapper} from "../../../features/store";
import React, {useState, useEffect} from "react";
import {useSelector} from "react-redux";
import {useRouter} from "next/router";
import ErrorServerDown from "../../500.ServerDown";


// Style
import style from "./unregister.module.scss"


// Actions
import {requestLoginVerify} from "../../../features/user/actions";
import {requestUpdateUserInfo, requestUserUnregister} from "../../../features/account/actions";
import LeftSidebar from "../../../components/sideBar/left-sidebar";
import RightSidebar from "../../../components/sideBar/right-sidebar";


const Form = () => {
    const router = useRouter();
    const user = useSelector(state => state.user);


    const [password, setPassword] = useState("");





    const submit = async (e) => {
        e.preventDefault();
        if (!password) return alert("계정 비밀번호를 입력해 주세요.");


        const {status} = await requestUserUnregister(password);


        switch (status) {
            case 400:
                alert("비밀번호가 입력되지 않았습니다.");
                break;
            case 403:
                alert("비밀번호가 틀렸거나 로그인 되어 있지 않습니다.");
                router.reload();
                break;
            case 200:
                alert("성공적으로 회원탈퇴 되었습니다.");
                await router.push("/");
                break;
        }
    }


    return (
        <form onSubmit={submit}>
            {/*<h3>회원탈퇴</h3>*/}
            {/*<br/>*/}

            <blockquote>
                <p>"회원탈퇴 이후 탈퇴된 계정으로 등록된 이메일(email)은 다시 사용될 수 없습니다."</p>
            </blockquote>
            <br/>

            <div>
                <p>아이디</p>
                <div/>
                <input className={style["id"]} type="text" value={user.id} readOnly={true}/>
            </div>
            <div>
                <p>비밀번호</p>
                <div/>
                <input type="password" autoComplete="new-password" onChange={(e) => setPassword(e.target.value)}/>
            </div>
            <div>
                <p>이메일</p>
                <div/>
                <input className={style["email"]} type="text" value={user.email} readOnly={true}/>
            </div>


            <input type="submit" value="회원탈퇴하기"/>
        </form>
    )
};


export default function UserUnregisterPage({isServerDown}) {
    if (isServerDown) return <ErrorServerDown/>;


    const user = useSelector(state => state.user);


    return (
        <>
            <LeftSidebar/>

            <div className={style["wrapper"]}>
                <h2>회원탈퇴</h2>
                <br/>

                {user.loggedIn? (
                    <Form/>
                ): (
                    <blockquote>
                        <p>"로그인 되어 있지 않습니다."</p>
                    </blockquote>
                )}
            </div>

            <RightSidebar/>
        </>
    )
}


export const getServerSideProps = wrapper.getServerSideProps(async (context) => {
    const {isServerDown} = await requestLoginVerify(context);

    return {
        props: {
            isServerDown: isServerDown
        }
    }
})