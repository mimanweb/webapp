import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux'
import styles from './login.module.scss';
import {loginRequest} from '../../../features/user/actions';
import {wrapper} from "../../../features/store";
import {END} from "redux-saga";


// Components
import Meta from '../../../components/meta/head';
import LeftSidebar from "../../../components/sideBar/left-sidebar";
import RightSidebar from "../../../components/sideBar/right-sidebar";


export default function Login() {
    const dispatch = useDispatch();
    const [userId, setUserId] = useState("");
    const [userPassword, setUserPassword] = useState("");

    // useEffect(() => {
    //     console.log(userid, userPassword)
    // });

    return (
        <>
            <Meta title='MIMAN login'/>
            <LeftSidebar/>

            <main className={styles["wrapper"]}>
                <div>
                    <span>id: </span>
                    <input className="id" type="text" onChange={(e) => setUserId(e.target.value)}/>
                </div>
                <div>
                    <span>password: </span>
                    <input className="password" type="text" onChange={(e) => setUserPassword(e.target.value)}/>
                </div>
                <div>
                    <button onClick={() => dispatch(loginRequest(userId, userPassword))}>login</button>
                </div>
            </main>

            <RightSidebar/>
        </>
    )
};

// export const getServerSideProps = wrapper.getServerSideProps(async ({ store }) => {
//
// })