import http from "../../utils/http";
import {API} from "../../utils/api";


// Action types
export const actionType = {
    SET_POST_STATE: "post/SET_POST_STATE",
    SET_POST_EDIT_MODE: "post/SET_POST_EDIT_MODE",
    SET_TEMP_POST_STATE: "post/SET_TEMP_POST_STATE",
    SET_POST_EDIT_INIT_STATE: "post/SET_POST_EDIT_INIT_STATE",
    SET_POST_OWNER_INFO: "post/SET_POST_OWNER_INFO",
    UPDATE_POST_STATE: "post/UPDATE_POST_STATE",
    UPDATE_UPVOTE_STATE: "post/UPDATE_UPVOTE_STATE",
    RESET_POST_TITLE_AND_CONTENT: "post/RESET_POST_TITLE_AND_CONTENT"
};


// Initial state
const initialState = {
    editMode: null,
    number: null,
    isMyPost: null,
    userId: null,
    nickname: null,
    password: null,
    ipAddress: null,
    category: null,
    board: null,
    postId: null,
    title: null,
    content: null,
    textContent: null,
    phrase: null,
    createdTime: null,
    updatedTime: null,
    updateTimeDiffInMinutes: null,
    record: {
        viewCount: null,
        upvoteCount: null
    },
    isUpvoted: null,
    temp: {
        content: null
    },
    type: null
};


// Reducer
export default function reducer(state=initialState, action) {
    switch (action.type) {
        case actionType.SET_TEMP_POST_STATE:
            return {
                ...state,
                ...action.payload,
                ...{
                    temp: {
                        content: action.payload.content
                    }
                }
            }
        case actionType.SET_POST_STATE:
        case actionType.SET_POST_OWNER_INFO:
        case actionType.UPDATE_POST_STATE:
        case actionType.RESET_POST_TITLE_AND_CONTENT:
        case actionType.SET_POST_EDIT_MODE:
            return {
                ...state,
                ...action.payload
            }
        case actionType.UPDATE_UPVOTE_STATE:
        case actionType.SET_POST_EDIT_INIT_STATE:
            return {
                ...state,
                ...action.payload,
                record: {
                    ...state.record,
                    ...action.payload.record
                }
            }
        default:
            return state;
    }
};




export function setPostEditMode(mode) {
    return {
        type: actionType.SET_POST_EDIT_MODE,
        payload: {
            editMode: mode
        }
    }
}



export function setPostState(postData, category, board) {
    const {number, isMyPost, owner, title, content, phrase, record, time, isUpvotedPost, type, textContent} = postData;
    const {userId, nickname, ipAddress} = owner;
    const {published: publishedTime, updated: updatedTime} = time;
    const {viewCount, upvoteCount} = record;

    return {
        type: actionType.SET_POST_STATE,
        payload: {
            number: number,
            isMyPost: isMyPost || null,
            userId: userId || null,
            nickname: nickname,
            category: category,
            ipAddress: ipAddress,
            board: board,
            title: title,
            content: content,
            textContent: textContent,
            phrase: phrase,
            createdTime: publishedTime,
            updatedTime: updatedTime,
            record: {
                viewCount: viewCount,
                upvoteCount: upvoteCount
            },
            isUpvoted: isUpvotedPost || null,
            type: type
        }
    }
}


export function setTempPostState(category, board, postData) {
    const {postId, title, content, phrase, createdTime, updatedTime, time, updateTimeDiffInMinutes, _id, index} = postData;

    return {
        type: actionType.SET_TEMP_POST_STATE,
        payload: {
            category: category || index.category,
            board: board || index.board,
            postId: postId || _id,
            title: title,
            content: content,
            phrase: phrase,
            createdTime: createdTime || time.created,
            updatedTime: (time && time.updated)? time.updated: updatedTime? updatedTime: null,
            updateTimeDiffInMinutes: updateTimeDiffInMinutes || null
        }
    }
}


export function setEditPostInitState(postData) {

    const {number: postNumber, postId, category, board, title, content, phrase, owner, time, record, index, _id} = postData;
    const {userId, nickname, ipAddress} = owner;
    const {updated: updatedTime} = time;
    const {upvoteCount} = record;

    return {
        type: actionType.SET_POST_EDIT_INIT_STATE,
        payload: {
            number: postNumber || null,
            postId: postId || _id,
            userId: userId || null,
            category: category || index.category,
            board: board || index.board,
            nickname: nickname || null,
            ipAddress: ipAddress,
            title: title,
            content: content,
            phrase: phrase,
            record: {
                upvoteCount: upvoteCount
            },
            updatedTime: updatedTime || null
        }
    }
}


export function setPostOwnerInfo(nickname, password) {
    return {
        type: actionType.SET_POST_OWNER_INFO,
        payload: {
            nickname: nickname,
            password: password
        }
    }
}


export function updatePostState(target, value) {
    const payload = {};

    switch (target) {
        case "title":
            payload.title = value;
            break;
        case "content":
            payload.content = value;
            break;
        case "phrase":
            payload.phrase = value;
            break;
    }

    return {
        type: actionType.UPDATE_POST_STATE,
        payload
    }
}


export function updatePostUpvoteState(isUpvoted, upvoteCount) {

    return {
        type: actionType.UPDATE_UPVOTE_STATE,
        payload : {
            isUpvoted: isUpvoted,
            record: {
                upvoteCount: upvoteCount
            }
        }
    }
}


export function resetPostTitleAndContentState() {
    return {
        type: actionType.UPDATE_UPVOTE_STATE,
        payload: {
            title: "",
            content: ""
        }
    }
}


export async function requestPreviouslySavedTempPost(postId, category, board, context) {
    const res = await http.get(API.Post.findTempPost.url, {category: category, board: board}, undefined, context);

    if (res.status===404) return null;
    else if (res.status===200) return res.data;
}


export async function requestUpdateTempPost(postId, category, board, title, content, phrase) {
    return await http.put(API.Post.updateTempPost.url, {postId: postId, category: category, board: board, title: title, content: content, phrase: phrase});
}


export async function requestPostOwnershipCheck(postNumber, category, board, password) {
    return await http.get(API.Post.checkPostOwnership.url, {postNumber: postNumber, category: category, board: board, password: password});
}


export async function requestDeletePost(postNumber, category, board, password) {
    return await http.delete(API.Post.deletePost.url, {postNumber, category, board, password});
}


export async function requestCreateTempPost(category, board) {
    const res = await http.post(API.Post.createTempPost.url, {category: category, board: board});

    if (res.status===500) return false;
    else if (res.status===201) return res.data;
}


export async function requestUploadPostImage(file, width, height, postId, postCategory, postBoard) {
    const formData = new FormData();

    formData.append('image', file);
    formData.append('width', width);
    formData.append('height', height);
    formData.append("postId", postId);
    formData.append("category", postCategory);
    formData.append("board", postBoard);

    return await http.post(API.Image.uploadPostImage.url, formData);
}


export async function requestPublishPost(postId, category, board, nickname, password, title, content, phrase, type) {
    return await http.post(API.Post.publishPost.url, {postId, category, board, nickname, password, title, content, phrase, type});
}


export async function requestUpdatePostUpvoteCount(postNumber, category, board) {
    return await http.post(API.Post.updateUpvote.url, {postNumber, category, board});
}


export async function requestUpdatePost(postId, password, category, board, title, content, phrase) {
    return await http.put(API.Post.updatePost.url, {postId, password, category, board, title, content, phrase});
}