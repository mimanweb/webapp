export const actionTypes = {
    // Login
    LOGIN_REQUEST: 'user/LOGIN_REQUEST',
    LOGIN_SUCCESS: 'user/LOGIN_SUCCESS',
    LOGIN_FAILURE: 'user/LOGIN_FAILURE',

    // Logout
    LOGOUT_REQUEST: "user/LOGOUT_REQUEST",
    LOGOUT_SUCCESS: "user/LOGOUT_SUCCESS",
    LOGOUT_FAILURE: "user/LOGOUT_FAILURE",

    SET_NOTIFICATION_STATE: "user/SET_NOTIFICATION_STATE",
    SET_NOTIFICATION_LIST_STATE: "user/SET_NOTIFICATION_LIST_STATE"
};


const initialState = {
    loggedIn: false,
    id: null,
    nickname: null,
    createdTime: null,
    updatedTime: null,
    nicknameUpdatedTime: null,
    type: null,
    hasUnreadNotification: null,
    notificationList: [],
    notificationPage: null,
    notificationTotalPageCount: null,
};

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case actionTypes.LOGIN_REQUEST:
            return {
                ...state
            }
        case actionTypes.LOGIN_SUCCESS:
            const user = action.payload;

            return {
                ...state,
                ...{
                    id: user.id,
                    nickname: user.nickname,
                    email: user.email,
                    createdTime: user.time.registered,
                    updatedTime: user.time.updated,
                    nicknameUpdatedTime: user.time.nicknameUpdated,
                    type: user.type || null,
                    loggedIn: true,
                    hasUnreadNotification: user.record.hasUnreadNotification || null
                }
            }
        case actionTypes.LOGIN_FAILURE:
            return {
                ...state,
                ...initialState
            }
        case actionTypes.LOGOUT_REQUEST:
            return {
                ...state
            }
        case actionTypes.LOGOUT_SUCCESS:
            return {
                ...state,
                ...initialState
            }
        case actionTypes.LOGOUT_FAILURE:
            return {
                ...state
            }
        case actionTypes.SET_NOTIFICATION_STATE:
        case actionTypes.SET_NOTIFICATION_LIST_STATE:
            return {
                ...state,
                ...action.payload
            }
        default:
            return state
    }
};