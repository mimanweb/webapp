import {combineReducers} from "redux";
import {HYDRATE} from "next-redux-wrapper";

// Reducers
import user from "./user/reducer";
import register from "./registeration/actions";
import post from "./post/actions";
import editor from "./editor/actions";
import comment from "./comment/actions";
import board from "./pageData/boardPage/actions";
import comics from "./comics/actions";
import comicBook from "./comicsBook/actions";
import homepage from "./homepage/actions";


// Combined reducer
const combinedReducer = combineReducers({
    user,
    register,
    post,
    editor,
    comment,
    board,
    comics,
    comicBook,
    homepage
});

export default function reducer (state, action) {
    if (action.type === HYDRATE) {
        const nextState = {
            ...state,
            ...action.payload
        }


        nextState.comics.recentReadComics = state.comics.recentReadComics;
        return nextState;
    } else {
        return combinedReducer(state, action);
    }
};