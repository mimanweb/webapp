import http from "../../utils/http";
import {API} from "../../utils/api";


// Actions
export async function findIdRequest(email) {
    const res = await http.get(API.User.findId.url, {email: email});

    return !res.error? res.data: null;
}


export async function requestSendIdEmail(email) {
    const res = await http.post(API.Mail.sendUserId.url, {email: email});

    let msg = "";
    if (res.error) {
        if (res.status === 404) msg = "가입되지 않은 이메일입니다."
        else msg = "이메일을 다시 보내주세요."
    }

    return {result: !res.error, msg: msg};
}


export async function requestPasswordResetEmail(id, email) {
    const res = await http.post(API.Mail.sendPasswordResetLink.url, {id: id, email: email});

    let msg = "";
    if (res.error) {
        if (res.status === 404) msg = "가입되지 않은 아이디/이메일입니다."
        else msg = "이메일을 다시 보내주세요."
    }

    return {result: !res.error, msg: msg};
}