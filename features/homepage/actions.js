import http from "../../utils/http";
import {API} from "../../utils/api";
import _ from "underscore";



export const actionType = {
    SET_HOMAPAGE_STATE: "homepage/SET_HOMAPAGE_STATE",
}



const initialState = {
    noticePost: null,
    bestHumorPostList: [],
    bestCombinedBoardPostList: [],
    communityFreeboardRecentPostList: [],
    comicsFreeboardRecentPostList: [],
    recentComicBookList: []
}


export default function reducer(state=initialState, action) {
    switch (action.type) {
        case actionType.SET_HOMAPAGE_STATE:
            return {
                ...state,
                ...action.payload,
            }
        default:
            return state;
    }
};





/**
 * Set homepage state
 * */
export function setHomepageState(homepageData) {
    const {noticePost, bestHumorPostList, bestCombinedBoardPostList, communityFreeboardRecentPostList, comicsFreeboardRecentPostList, recentComicBookList} = homepageData;

    return {
        type: actionType.SET_HOMAPAGE_STATE,
        payload: {
            noticePost: noticePost || null,
            bestHumorPostList: bestHumorPostList || [],
            bestCombinedBoardPostList: bestCombinedBoardPostList || [],
            communityFreeboardRecentPostList: communityFreeboardRecentPostList || [],
            comicsFreeboardRecentPostList: comicsFreeboardRecentPostList || [],
            recentComicBookList: recentComicBookList || []
        }
    }
}
