import {all, fork, call, delay, put, take, takeLatest} from 'redux-saga/effects'
import {actionType} from "./actions";
import {API} from "../../utils/api";
import axios from 'axios';
import router from 'next/router'
import {errBehave} from "../../utils/http";



function* watchRegister({id, password, nickname, email}) {
    try {
        yield call(() => {
            return axios.post(API.User.register.url,
                {
                    id: id,
                    password: password,
                    nickname: nickname,
                    email: email
                })
        })
        yield alert("회원가입 되었습니다.");
        router.push("/");
    } catch (e) {
        yield errBehave(e, 400, "000", ()=>{alert("회원가입 항목을 전부 입력해 주십시오.")});
        yield errBehave(e, 400, "001", ()=>{alert("이미 사용 중인 아이디입니다.")});
        yield errBehave(e, 400, "002", ()=>{alert("잘 못 입력된 항목이 존재합니다.")});
        yield errBehave(e, 500, "000", ()=>{alert("서버 오류가 발생하였습니다.")});
    }
}


export default function* rootSaga() {
    yield all([
        takeLatest(actionType.REGISTER_REQUEST, watchRegister)
    ])
};