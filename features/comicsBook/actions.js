import http from "../../utils/http";
import {API} from "../../utils/api";
import {COMIC_BOOK_IMAGE_STATUS, COMIC_BOOK_PAGE_TYPE} from "../../utils/index/comics/book";
import _ from "underscore";



export const actionType = {
    SET_IS_COMICS_BOOK_TITLE: "comicsBook/SET_IS_COMICS_BOOK_TITLE",
    SET_IS_COMICS_BOOK_VOLUME: "comicsBook/SET_IS_COMICS_BOOK_VOLUME",
    INCREASE_COMICS_VOLUME: "comicsBook/INCREASE_COMICS_VOLUME",
    INCREASE_COMICS_ISSUE: "comicsBook/INCREASE_COMICS_ISSUE",
    DECREASE_COMICS_VOLUME: "comicsBook/DECREASE_COMICS_VOLUME",
    DECREASE_COMICS_ISSUE: "comicsBook/DECREASE_COMICS_ISSUE",
    SET_IS_SHORT_STORY: "comicsBook/SET_IS_SHORT_STORY",
    SET_COMICS_EDITION_TITLE: "comicsBook/SET_COMICS_EDITION_TITLE",
    SET_COMICS_CONTENT_TYPE: "comicsBook/SET_COMICS_CONTENT_TYPE",
    SET_COMIC_BOOK_STATE: "comicsBook/SET_COMIC_BOOK_STATE",
    SET_COMIC_BOOK_PAGE_ON_DRAG: "comicsBook/SET_COMIC_BOOK_PAGE_ON_DRAG",
    SET_COMIC_BOOK_PAGE_DRAG_OVER: "comicsBook/SET_COMIC_BOOK_PAGE_DRAG_OVER",
    SET_SELECTED_COMIC_BOOK_PAGE: "comicsBook/SET_SELECTED_COMIC_BOOK_PAGE",
    SET_USER_PUBLISHED_BOOKS: "comicsBook/SET_USER_PUBLISHED_BOOKS",
    SET_EDIT_MODE: "comicsBook/SET_EDIT_MODE",
}



const initialState = {
    editMode: null,
    comicBookDataId: null,
    isEditionTitle: null,
    isVolume: null,
    isShortStory: false,
    editionTitle: "",
    volume: 0,
    issue: 0,
    shortStoryNumber: 0,
    contentType: null,
    pageList: [],
    link: "",
    linkThumbnail: "",
    tempLinkThumbnail: "",
    selectedPageNumber: null,
    pageNumberOnDrag: null,
    pageNumberOnDragOver: null,
    publishedBooks: [],
    owner: {},
}


export default function reducer(state=initialState, action) {
    switch (action.type) {
        case actionType.SET_IS_COMICS_BOOK_TITLE:
        case actionType.SET_IS_COMICS_BOOK_VOLUME:
        case actionType.SET_IS_SHORT_STORY:
        case actionType.SET_COMICS_EDITION_TITLE:
        case actionType.SET_COMICS_CONTENT_TYPE:
        case actionType.SET_COMIC_BOOK_STATE:
        case actionType.SET_COMIC_BOOK_PAGE_ON_DRAG:
        case actionType.SET_COMIC_BOOK_PAGE_DRAG_OVER:
        case actionType.SET_SELECTED_COMIC_BOOK_PAGE:
        case actionType.SET_USER_PUBLISHED_BOOKS:
        case actionType.SET_EDIT_MODE:
            return {
                ...state,
                ...action.payload,
            }
        case actionType.INCREASE_COMICS_VOLUME:
            return {
                ...state,
                volume: state.volume+1,
            }
        case actionType.DECREASE_COMICS_VOLUME:
            return {
                ...state,
                volume: state.volume<=0? 0: state.volume-1,
            }
        case actionType.INCREASE_COMICS_ISSUE:
            return {
                ...state,
                issue: state.issue+1,
            }
        case actionType.DECREASE_COMICS_ISSUE:
            return {
                ...state,
                issue: state.issue<=0? 0: state.issue-1,
            }
        default:
            return state;
    }
};





/**
 * Set comic book state
 * */
export function setComicBookState(comicBookData) {
    const {_id: comicBookDataId, title, index: comicBookInfo, owner: comicBookOwner} = comicBookData;
    const {volume, issue, shortStoryNumber, isTitle, isVolume, isShortStory, contentType} = comicBookInfo;
    let {pages: pageList, linkThumbnails, link} = comicBookData;

    /**
     * Format comic book pages
     * */
    const singlePageList = pageList.filter(page => [COMIC_BOOK_PAGE_TYPE.SINGLE, COMIC_BOOK_PAGE_TYPE.WIDE].includes(page.pageType));
    const doublePageList = pageList.filter(page => [COMIC_BOOK_PAGE_TYPE.PAIRED].includes(page.pageType));
    const doublePageListGroupByPairNumber = _.groupBy(doublePageList, "pairNumber");
    const groupedDoublePageList = Object.values(doublePageListGroupByPairNumber).map(pairedPage => {
        const firstPage = pairedPage[0];
        const secondPage = pairedPage[1];

        return {
            firstPage: firstPage,
            secondPage: secondPage,
            status: firstPage.status,
            pageNumber: firstPage.pageNumber,
            pageType: firstPage.pageType
        }
    })

    pageList = singlePageList.concat(groupedDoublePageList);
    pageList = _.sortBy(pageList, "pageNumber");


    /**
     * Format link-thumbnail
     * */
    const linkThumbnail = linkThumbnails.find(thumbnail => thumbnail.status===COMIC_BOOK_IMAGE_STATUS.PUBLISH);
    const tempLinkThumbnail = linkThumbnails.find(thumbnail => thumbnail.status===COMIC_BOOK_IMAGE_STATUS.WRITING);

    return {
        type: actionType.SET_COMIC_BOOK_STATE,
        payload: {
            comicBookDataId: comicBookDataId,
            isEditionTitle: isTitle,
            isVolume: isVolume,
            isShortStory: isShortStory,
            editionTitle: title,
            volume: volume,
            issue: issue,
            shortStoryNumber: shortStoryNumber || 0,
            contentType: contentType,
            pageList: pageList,
            link: link || null,
            linkThumbnail: linkThumbnail? linkThumbnail.imgId.large: null,
            tempLinkThumbnail: tempLinkThumbnail? tempLinkThumbnail.imgId.large: null,
            owner: comicBookOwner
        }
    }
}


/**
 * Set user published comic books state
 * */
export function setUserPublishedComicBooksState(list) {
    return {
        type: actionType.SET_USER_PUBLISHED_BOOKS,
        payload: {
            publishedBooks: list
        }
    }
}


/**
 * Set comics edition title status
 * */
export function setIsComicsEditionTitleState(isComicsEditionTitle) {
    return {
        type: actionType.SET_IS_COMICS_BOOK_TITLE,
        payload: {
            isEditionTitle: isComicsEditionTitle
        }
    }
}


/**
 * Set comics volume status
 * */
export function setIsComicsVolumeState(isComicsVolume) {
    return {
        type: actionType.SET_IS_COMICS_BOOK_VOLUME,
        payload: {
            isVolume: isComicsVolume
        }
    }
}


/**
 * Set comics episode type
 * */
export function setIsComicsShortStory(isShortStory) {
    return {
        type: actionType.SET_IS_SHORT_STORY,
        payload: {
            isShortStory: isShortStory
        }
    }
}


/**
 * Set comics edition title
 * */
export function setComicsEditionTitleState(text) {
    return {
        type: actionType.SET_COMICS_EDITION_TITLE,
        payload: {
            editionTitle: text
        }
    }
}


/**
 * Set comics book content type
 * */
export function setComicsBookContentType(type) {
    return {
        type: actionType.SET_COMICS_CONTENT_TYPE,
        payload: {
            contentType: type
        }
    }
}


/**
 * Set comics book edit mode
 * */
export function setComicsBookEditMode(mode) {
    return {
        type: actionType.SET_EDIT_MODE,
        payload: {
            editMode: mode
        }
    }
}


/**
 * Increase comics volume
 * */
export function increaseComicsVolumeState() {
    return {
        type: actionType.INCREASE_COMICS_VOLUME,
    }
}

/**
 * Decrease comics volume
 * */
export function decreaseComicsVolumeState() {
    return {
        type: actionType.DECREASE_COMICS_VOLUME,
    }
}


/**
 * Increase comics volume
 * */
export function increaseComicsIssueState() {
    return {
        type: actionType.INCREASE_COMICS_ISSUE,
    }
}

/**
 * Decrease comics volume
 * */
export function decreaseComicsIssueState() {
    return {
        type: actionType.DECREASE_COMICS_ISSUE,
    }
}


/**
 * Set comic book page number on drag
 * */
export function setComicBookPageOnDrag(pageNumber) {
    return {
        type: actionType.SET_COMIC_BOOK_PAGE_ON_DRAG,
        payload: {
            pageNumberOnDrag: pageNumber
        }
    }
}

/**
 * Set comic book page number on drag over
 * */
export function setComicBookPageOnDragOver(pageNumber) {
    return {
        type: actionType.SET_COMIC_BOOK_PAGE_DRAG_OVER,
        payload: {
            pageNumberOnDragOver: pageNumber
        }
    }
}


/**
 * Set comic book page number on select
 * */
export function setSelectedComicBookPage(pageNumber) {
    return {
        type: actionType.SET_COMIC_BOOK_PAGE_DRAG_OVER,
        payload: {
            selectedPageNumber: pageNumber
        }
    }
}



/**
 * Upload temp(writing) comic book page image
 * */
export async function requestUploadComicBookPageImage(comicBookDataId, imgFile, isLinkThumbnail) {
    const formData = new FormData();

    formData.append('image', imgFile);
    formData.append('comicBookDataId', comicBookDataId);
    formData.append('isLinkThumbnail', isLinkThumbnail);

    return await http.post(API.ComicBook.addPageImage.url, formData);
}


/**
 * Update comic book single page type
 * */
export async function requestUpdateComicBookSinglePageType(comicBookDataId, pageNumber, pageType) {
    return http.put(API.ComicBook.updateSinglePageType.url, {comicBookDataId, pageNumber, pageType});
}


/**
 * Update comic book double page type
 * */
export async function requestUpdateComicBookDoublePageType(comicBookDataId, firstPageNumber, secondPageNumber, pageType) {
    return http.put(API.ComicBook.updateDoublePageType.url, {comicBookDataId, firstPageNumber, secondPageNumber, pageType});
}


/**
 * Delete comic book page
 * */
export async function requestDeleteComicBookPage(comicBookDataId, pageNumber, secondPageNumber) {
    return http.delete(API.ComicBook.deletePage.url, {comicBookDataId, pageNumber, secondPageNumber});
}


/**
 * Update comic book page order
 * */
export async function requestUpdateComicBookPageOrder(comicBookDataId, pageToGo, pageToMove, isPairedPage) {
    return http.put(API.ComicBook.updatePageOrder.url, {comicBookDataId, pageToGo, pageToMove, isPairedPage});
}


/**
 * Update comic book info
 * */
export async function requestUpdateComicBookInfo({comicBookDataId, title, volume, issue, contentType, isTitle, isShortStory, isVolume, link}) {
    return http.put(API.ComicBook.updateInfo.url, {comicBookDataId, title, volume, issue, contentType, isTitle, isShortStory, isVolume, link});
}


/**
 * Publish comic book data
 * */
export async function requestPublishComicBookData(comicBookDataId, postContent, postPhrase) {
    return http.post(API.ComicBook.publishComicBook.url, {comicBookDataId, postContent, postPhrase});
}


/**
 * Update comic book data
 * */
export async function requestUpdateComicBookData(comicBookDataId, postContent, postPhrase) {
    return http.put(API.ComicBook.updateComicBookData.url, {comicBookDataId, postContent, postPhrase});
}



/**
 * Delete comic book data
 * */
export async function requestDeleteComicBookData(comicBookDataId) {
    return http.delete(API.ComicBook.deleteComicBook.url, {comicBookDataId});
}