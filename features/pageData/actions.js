import http from "../../utils/http";
import {API} from "../../utils/api";


/**
 * Homepage data
 * */
export async function requestHomepageData(context) {
    const {status, data, isServerDown} = await http.get(API.PageData.homepage.url, undefined, undefined, context);

    return {
        status: status,
        isServerDown: isServerDown,
        homepageData: data? data.homepage: null,
    }
}


/**
 * Post page data
 * */
export async function requestPostPageData(postNumber, listType, category, board, commentToSearch, context) {
    const {status, data, isServerDown} = await http.get(API.PageData.postPage.url, {postNumber, listType, category, board, commentToSearch}, undefined, context);

    return {
        status: status,
        isServerDown: isServerDown,
        postData: data? data.post: null,
        commentData: data? data.comment: null,
        boardData: data? data.board: null,
    }

}


/**
 * Post Edit page data
 * */
export async function requestPostEditPageData(postNumber, category, board, postPassword, context) {
    const {status, data, isServerDown} = await http.get(API.PageData.postEditPage.url, {postNumber, category, board, postPassword}, undefined, context);

    return {
        status: status,
        isServerDown: isServerDown,
        postData: data? data.post: null
    }
}


/**
 * Comics page data
 * */
export async function requestComicsPageData(comicsNumber, comicsId, context) {
    const {status, data, isServerDown} = await http.get(API.PageData.comicsPage.url, {comicsNumber, comicsId}, undefined, context);

    return {
        status: status,
        isServerDown: isServerDown,
        comicsData: data? data.comics: null
    }
}


/**
 * Comic book write page data
 * */
export async function requestComicBookWritePageData(comicsDataId, context) {
    const {status, data, isServerDown} = await http.get(API.PageData.comicBookWritePage.url, {comicsDataId}, undefined, context);

    return {
        status: status,
        isServerDown: isServerDown,
        comicsData: data? data.comics: null,
        comicBookData: data? data.comicBook: null,
        comicBookPostData: data? data.post: null,
        publishedComicBooks: data? data.publishedComicBooks: []
    }
}


/**
 * Comic book edit page data
 * */
export async function requestComicBookEditPageData(comicBookDataId, context) {
    const {status, data, isServerDown} = await http.get(API.PageData.comicBookEditPage.url, {comicBookDataId}, undefined, context);

    return {
        status: status,
        isServerDown: isServerDown,
        comicsData: data? data.comics: null,
        comicBookData: data? data.comicBook: null,
        comicBookPostData: data? data.post: null,
    }
}


/**
 * Comic book page data
 * */
export async function requestComicBookPageData(comicsNumber, comicsId, translator, issue, volume, shortStoryNumber, context) {
    const {status, data, isServerDown} = await http.get(API.PageData.comicBookPage.url, {comicsNumber, comicsId, translator, issue, volume, shortStoryNumber}, undefined, context);

    return {
        status: status,
        isServerDown: isServerDown,
        comicsData: data? data.comics: null,
        comicBookData: data? data.comicBook: null,
        comicBookPostData: data? data.post: null,
        comicBookCommentData: data? data.comment: null,
    }
}


/**
 * Comics translate page data
 * */
export async function requestComicsTranslatePageData(context) {
    const {status, data, isServerDown} = await http.get(API.PageData.comicsTranslatePage.url, {}, undefined, context);


    return {
        status: status,
        isServerDown: isServerDown,
        totalCount: data? data.totalCount: null,
        comicsList: data? data.comicsList: [],
        itemsPerPage: data? data.itemsPerPage: null,
        pageToSearch: data? data.pageToSearch: null,
    }
}