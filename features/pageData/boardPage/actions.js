import {BOARD_LIST_TYPE} from "../../../utils/index/board";





// Action types
export const actionType = {
    SET_POST_LIST_AND_PAGE_INFO_STATE: "board/SET_POST_LIST_AND_PAGE_INFO_STATE",
    SET_INDEX: "board/SET_INDEX",
};


// Initial state
const initialState = {
    noticePostList: [],
    postList: [],
    currPage: null,
    totalPageCount: null,
    postPerPage: null,
    category: null,
    board: null,
    listType: null,
    hotPostList_24h: [],
    hotPostList_7d: [],
};


// Reducer
export default function reducer(state=initialState, action) {
    switch (action.type) {
        case actionType.SET_POST_LIST_AND_PAGE_INFO_STATE:
        case actionType.SET_INDEX:
            return {
                ...state,
                ...action.payload
            }
        default:
            return state;
    }
};




export function setBoardIndex (category, board, listType=BOARD_LIST_TYPE.ALL) {
    return {
        type: actionType.SET_INDEX,
        payload: {
            category: category,
            board: board,
            listType: listType
        }
    }
}



export function setBoardPostListAndPageInfoState (postList, noticePostList, currPage, totalPageCount, postPerPage, hotPostListInfo) {



    return {
        type: actionType.SET_POST_LIST_AND_PAGE_INFO_STATE,
        payload: {
            postList: postList,
            noticePostList: noticePostList,
            currPage: currPage,
            totalPageCount: totalPageCount,
            postPerPage: postPerPage,
            hotPostList_24h: hotPostListInfo? hotPostListInfo["24h"]: [],
            hotPostList_7d: hotPostListInfo? hotPostListInfo["7d"]: [],
        }
    }
}