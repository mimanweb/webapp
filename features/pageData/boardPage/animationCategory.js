import {BOARD_LIST_TYPE} from "../../../utils/index/board";
import http from "../../../utils/http";
import {API} from "../../../utils/api";

/**
 * Animation Freeboard page data
 * */
export async function requestAnimationFreeboardPageData(pageNumber, listType=BOARD_LIST_TYPE.ALL, context) {
    const {status, data, isServerDown} = await http.get(API.PageData.board.animation.freeboard.url, {pageNumber, listType}, undefined, context);

    return {
        status: status,
        isServerDown: isServerDown,
        board: data? data.board: null,
    }
}