import http from "../../../utils/http";
import {API} from "../../../utils/api";
import {BOARD_LIST_TYPE} from "../../../utils/index/board";



/**
 * Comics Freeboard page data
 * */
export async function requestComicsFreeboardPageData(pageNumber, listType=BOARD_LIST_TYPE.ALL, context) {
    const {status, data, isServerDown} = await http.get(API.PageData.board.comics.freeboard.url, {pageNumber, listType}, undefined, context);

    return {
        status: status,
        isServerDown: isServerDown,
        board: data? data.board: null,
    }
}
