import http from "../../utils/http";
import {API} from "../../utils/api";




// Action types
export const actionType = {
    SET_EDITOR_WRITING_BOARD_REF: "editor/SET_EDITOR_WRITING_BOARD_REF",
    SET_ROUTE_ON_AUTO_SAVE_FAIL: "editor/SET_ROUTE_ON_AUTO_SAVE_FAIL",
    SET_DISPLAY_UNREG_USER_INFO: "editor/SET_DISPLAY_UNREG_USER_INFO",
};


// Initial state
const initialState = {
    editorWritingBoard: null,
    routeOnAutoSaveFail: true,
    displayUnregUserInfo: true
};


// Reducer
export default function reducer(state=initialState, action) {
    switch (action.type) {
        case actionType.SET_EDITOR_WRITING_BOARD_REF:
        case actionType.SET_ROUTE_ON_AUTO_SAVE_FAIL:
        case actionType.SET_DISPLAY_UNREG_USER_INFO:
            return {
                ...state,
                ...action.payload
            }
        default:
            return state;
    }
};



export const setEditorWritingBoard = (ref) => {
    return {
        type: actionType.SET_EDITOR_WRITING_BOARD_REF,
        payload: {
            editorWritingBoard: ref.current
        }
    }
}


/**
 * Set route on auto save fail
 * */
export function setRouteOnEditorAutoSaveFail(enabled) {
    return {
        type: actionType.SET_ROUTE_ON_AUTO_SAVE_FAIL,
        payload: {
            routeOnAutoSaveFail: enabled
        }
    }
}


/**
 * Set display unreg-user info in editor
 * */
export function setEditorDisplayUnregUserInfo(display) {
    return {
        type: actionType.SET_ROUTE_ON_AUTO_SAVE_FAIL,
        payload: {
            displayUnregUserInfo: display
        }
    }
}



export const requestLinkTitle = async (url) => {
    return await http.post(API.Editor.linkTitle.url, {url});
}