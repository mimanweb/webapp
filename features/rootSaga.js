import {all, fork, call, delay, put, take, takeLatest} from 'redux-saga/effects';


// Sagas
import userSaga from "./user/sagas";
import registerSaga from "./registeration/sagas";
import postSaga from "./post/sagas";
import comicBookEditorSaga from "./comicsBook/sagas"


export default function* rootSaga() {
    yield all([
        fork(userSaga),
        fork(registerSaga),
        fork(postSaga),
        fork(comicBookEditorSaga)
    ])
};