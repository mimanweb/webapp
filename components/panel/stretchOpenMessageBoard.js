import React, {useState, forwardRef, useImperativeHandle} from "react";
import style from "./stretchOpenMessageBoard.module.scss";


import PostImgUploadResultMessage from "./modules/postImgUploadResultMessage";
import PostYoutubeLinkInput from "./modules/postYoutubeLinkInput";
import PostTempSaveResultMessage from "./modules/postTempSaveResultMessage";
import PostParamCheckMessage from "./modules/Message.postUpload";
import VerifyOwnerBeforeEditPost from "./modules/Panel.verify_owner_before_edit_post"
import VerifyOwnerBeforeDeletePost from "./modules/Panel.verify_owner_before_delete_post"
import VerifyOwnerBeforeDeleteComment from "./modules/Panel.verify_owner_before_delete_comment";
import PostLinkInput from "./modules/Post.linkInput";
import ComicsImageInputMessage from "./modules/Comics.imageInputMessage";
import NotRegistableComicBookMessage from "./modules/Message.comicBookNotRegistable";
import PostUrlCopiedMessage from "./modules/Message.postUrlCopied";
import {StretchOpenBoardType} from "./index"



const StretchOpenMessageBoard = forwardRef(({boardType, editorRef, updatePostContentFunc}, ref) => {
    const [enabled, setEnabled] = useState(false);
    const [isOpened, setIsOpened] = useState(false);
    const [isClosing, setIsClosing] = useState(false);
    const [additionalBoardType, setAdditionalBoardType] = useState(null);
    const [contentType, setContentType] = useState(null);



    const theme = () => {
        const list = [];

        // ROWS
        switch (boardType) {
            case StretchOpenBoardType.POST_IMG_UPLOAD_RESULT:
            case StretchOpenBoardType.POST_TEMP_SAVE_RESULT:
            case StretchOpenBoardType.POST_PARAM_CHECK:
            case StretchOpenBoardType.COMICS.COVER_IMAGE_INPUT_MESSAGE:
            case StretchOpenBoardType.COMIC_BOOK.REGISTER.NOT_AVAILABLE:
            case StretchOpenBoardType.POST.COPY_CURRENT_URL_TO_CLIPBOARD:
                list.push(style["row1"]);
                break;
            case StretchOpenBoardType.POST_EDITOR_MENUBAR.LINK_INPUT:
                list.push(style["row6"]);
                break;
            case StretchOpenBoardType.VERIFY_OWNER_BEFORE_EDIT_POST:
            case StretchOpenBoardType.VERIFY_OWNER_BEFORE_DELETE_POST:
            case StretchOpenBoardType.COMMENT.VERIFY_OWNER_BEFORE_DELETE_COMMENT:
                list.push(style["row2"]);
                break;
            case StretchOpenBoardType.POST_YOUTUBE_LINK_INPUT:
                list.push(style["row12"]);
                break;
        }

        // ADDITIONAL HEIGHT
        switch (additionalBoardType) {
            case StretchOpenBoardType.POST_YOUTUBE_LINK_INPUT_WITH_ERROR_MESSAGE:
                list.push(style["row13"]);
                break;
        }

        return list.join(" ");
    }


    const onTransitionEnd = () => {
        if (enabled) {
            setIsOpened(true);
        }
        else {
            setIsOpened(false);
            setIsClosing(false);
        }
    }


    const open = () => {
        if (isOpened) {
            close();
            setTimeout(() => setEnabled(true), 500);
        } else {
            setEnabled(true);
        }
    }

    const close = () => {
        setEnabled(false);
        setIsClosing(true);
    }



    useImperativeHandle(ref, () => ({
        open: (contentType) => {
            if (contentType) setContentType(contentType);
            open();
        },
        close: () => {
            close();
        },
        openOrClose: () => {
            if (isOpened) close();
            else open();
        },
        checkIsOpened: () => {
            return enabled;
        }
    }))


    return (
        <div className={`${style["container"]} ${theme()} ${!enabled? style["hide"]: ""}`} onTransitionEnd={onTransitionEnd}>
            {boardType===StretchOpenBoardType.COMMENT.VERIFY_OWNER_BEFORE_DELETE_COMMENT && (<VerifyOwnerBeforeDeleteComment isBoardOpened={isOpened} closeBoardFunc={close}/>)}
            {boardType===StretchOpenBoardType.VERIFY_OWNER_BEFORE_EDIT_POST && (<VerifyOwnerBeforeEditPost isBoardOpened={isOpened} closeBoardFunc={close}/>)}
            {boardType===StretchOpenBoardType.VERIFY_OWNER_BEFORE_DELETE_POST && (<VerifyOwnerBeforeDeletePost isBoardOpened={isOpened} closeBoardFunc={close}/>)}
            {boardType===StretchOpenBoardType.POST_PARAM_CHECK && (<PostParamCheckMessage messageType={contentType} isBoardOpened={isOpened} closeBoardFunc={close}/>)}
            {boardType===StretchOpenBoardType.POST_IMG_UPLOAD_RESULT && (<PostImgUploadResultMessage messageType={contentType} isBoardOpened={isOpened} closeBoardFunc={close}/>)}
            {boardType===StretchOpenBoardType.POST_TEMP_SAVE_RESULT && (<PostTempSaveResultMessage messageType={contentType} isBoardOpened={isOpened} closeBoardFunc={close}/>)}
            {boardType===StretchOpenBoardType.POST_YOUTUBE_LINK_INPUT && (<PostYoutubeLinkInput setAdditionalBoardTypeFunc={setAdditionalBoardType} isBoardOpened={isOpened} editorRef={editorRef} updatePostContentFunc={updatePostContentFunc} closeBoardFunc={close}/>)}
            {boardType===StretchOpenBoardType.POST_EDITOR_MENUBAR.LINK_INPUT && (<PostLinkInput setAdditionalBoardTypeFunc={setAdditionalBoardType} isBoardOpened={isOpened} editorRef={editorRef} updatePostContentFunc={updatePostContentFunc} closeBoardFunc={close}/>)}
            {boardType===StretchOpenBoardType.COMICS.COVER_IMAGE_INPUT_MESSAGE && (<ComicsImageInputMessage isBoardOpened={isOpened} closeBoardFunc={close} messageType={contentType}/>)}
            {boardType===StretchOpenBoardType.COMIC_BOOK.REGISTER.NOT_AVAILABLE && (<NotRegistableComicBookMessage isBoardOpened={isOpened} closeBoardFunc={close} messageType={contentType}/>)}
            {boardType===StretchOpenBoardType.POST.COPY_CURRENT_URL_TO_CLIPBOARD && (<PostUrlCopiedMessage isBoardOpened={isOpened} closeBoardFunc={close} messageType={contentType}/>)}
        </div>
    )
})


export default StretchOpenMessageBoard;