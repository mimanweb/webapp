import style from "./styles/Panel.verify_owner_before_edit_post.module.scss";
import MessageWriter from "./messageWriter";
import React, {useState, useRef, useEffect} from "react";
import {requestPostOwnershipCheck} from "../../../features/post/actions";
import {useSelector} from "react-redux";
import {useRouter} from "next/router";
import Keyboard from "../../../utils/editor/keyboard";




const VerifyOwnerBeforeEditPost = ({isBoardOpened, closeBoardFunc}) => {
    const router = useRouter();

    const postState = useSelector(state => state.post);
    const userState = useSelector(state => state.user);

    const [password, setPassword] = useState(null);


    const {number: postNumber, category, board, isMyPost} = postState;
    const isRegUser = userState.loggedIn;


    const messageWriterRef = useRef(null);
    const confirmButtonRef = useRef(null);



    const onKeyDown = (e) => {
        if (e.keyCode===Keyboard.KEY.ENTER) {
            confirmButtonRef.current.click();
        }
    }


    const onConfirmButtonClick = async () => {
        if (!password) {
            messageWriterRef.current.write("비밀번호가 입력되지 않았습니다. 글 작성시 등록한 비밀번호를 입력해 주세요.");
            return;
        }

        const {status} = await requestPostOwnershipCheck(postNumber, category, board, password);

        switch (status) {
            case 404:
                alert("이미 삭제되거나 수정 권한이 없는 게시글입니다.");
                await router.push(`/board/${category}/${board}`);
                break;
            case 403:
                messageWriterRef.current.write("비밀번호가 틀립니다.");
                break;
            case 200:
                await router.push({
                    pathname: `/post/write/${category}/${board}/${postNumber}`,
                    query: {password: password}
                });
                break;
        }
    }



    useEffect(() => {
        if (isBoardOpened) messageWriterRef.current.write("안녕하세요, 게시글을 수정하러 가시겠습니까? ^-^");

        return () => {
            if (messageWriterRef.current) messageWriterRef.current.clear();
            setPassword(null);
        }
    }, [isBoardOpened])


    return (
        <div className={style["container"]}>
            <section>
                <MessageWriter printLeftToRight={false} ref={messageWriterRef}/>
            </section>
            <section>
                <input type="password" placeholder="비밀번호" autoComplete="new-password" onKeyDown={onKeyDown} onChange={e => setPassword(e.target.value)}/>
                <button className={style["confirm"]} onClick={onConfirmButtonClick} ref={confirmButtonRef}>--&gt; 네 &lt;--</button>
            </section>
        </div>
    )
}


export default VerifyOwnerBeforeEditPost;