import React, {useRef, useEffect} from "react";
import style from "./styles/postImgUploadResultMessage.module.scss";
import MessageWriter from "./messageWriter";
import {StretchOpenMessageType} from "../index";


const message = (type) => {
    switch (type) {
        case StretchOpenMessageType.IMG_LOAD_FAIL:
            return "이미지를 불러오는 중 오류가 발생하였습니다.";
        case StretchOpenMessageType.IMG_UPLOAD_FAIL:
            return "이미지 업로드 중 오류가 발생하였습니다.";
        case StretchOpenMessageType.IMG_FILE_NOT_SUPPORTED:
            return "지원되지 않는 이미지 파일입니다. ( jpg, jpeg, png, gif 지원 )";
        case StretchOpenMessageType.GIF_SIZE_EXCEEDED:
            return "gif 파일은 20MB까지 업로드 가능합니다.";
        case StretchOpenMessageType.IMG_SIZE_EXCEEDED:
            return "이미지 파일은 50MB까지 업로드 가능합니다.";
    }

}


const PostImgUploadResultMessage = ({messageType, isBoardOpened, closeBoardFunc}) => {
    const messageWriterRef = useRef(null);



    useEffect(() => {
        if (isBoardOpened) messageWriterRef.current.write(message(messageType));

        return () => {
            if (messageWriterRef.current) messageWriterRef.current.clear();
        }
    }, [isBoardOpened])


    return (
        <div className={style["container"]}>
            <MessageWriter printLeftToRight={true} ref={messageWriterRef}/>
            <button className={style["confirm"]} onClick={closeBoardFunc}>--&gt; 확인 &lt;--</button>
        </div>
    )
}


export default PostImgUploadResultMessage;