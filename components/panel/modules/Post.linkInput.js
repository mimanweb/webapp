import React, {useState, useRef, useEffect} from "react";
import style from "./styles/Post.linkInput.module.scss";
import {StretchOpenBoardType} from "../index";
import MessageWriter from "./messageWriter";
import {findLineContainer, selectEditorFirstLine, checkEditorHasFocus} from "../../../utils/editor/commonUtils";
import {requestLinkTitle} from "../../../features/editor/actions";


const IFRAME_WIDTH = "420";
const IFRAME_HEIGHT = "248";



const PostLinkInput = ({editorRef, isBoardOpened, closeBoardFunc, updatePostContentFunc, setAdditionalBoardTypeFunc}) => {
    const linkInputRef = useRef(null);
    const linkTitleRef = useRef(null);


    const clearContent = () => {
        linkInputRef.current.value = "";
        linkTitleRef.current.value = "";
    }


    const onInput = async () => {

        linkTitleRef.current.innerHTML = "";

        const text = linkInputRef.current.value;
        const {status, data} = await requestLinkTitle(text);

        let linkTitle;

        switch (status) {
            case 200:
                linkTitle = data.title;
                break;
            default:
                linkTitle = text;
                break;
        }


        linkTitleRef.current.value = linkTitle;
    }



    const insertUrlLinkToEditor = () => {
        const isEditorFocused = checkEditorHasFocus(editorRef.current);


        const link = linkInputRef.current.value;
        const title = linkTitleRef.current.value;


        if (!link || !title) return;

        const p = document.createElement("p");
        const a = document.createElement("a");

        a.href = link;
        a.target = "_blank"
        a.textContent = title
        p.appendChild(a);


        /**
         * Editor not focused
         * */
        if (!isEditorFocused) {
            selectEditorFirstLine(editorRef);

            const selection = document.getSelection();
            const range = selection.getRangeAt(0);

            const line = findLineContainer(range.endContainer);

            range.setStartBefore(line);
            range.setEndBefore(line);
            range.insertNode(p);

            selection.setPosition(line, 0);
        }
        /**
         * Editor has focus
         * */
        else if (isEditorFocused) {
            const selection = document.getSelection();
            const range = selection.getRangeAt(0);
            const line = findLineContainer(range.endContainer);

            range.setStartAfter(line);
            range.setEndAfter(line);
            range.insertNode(p);

            selection.setPosition(line, 0);
        }


        clearContent();
        closeBoardFunc();

        updatePostContentFunc();
    }


    useEffect(() => {
        if (!isBoardOpened) {
            clearContent();
        }

    }, [isBoardOpened])



    return (
        <div className={style["container"]}>
            <textarea placeholder="URL 링크 입력" rows={3} spellCheck={false} defaultValue="" onInput={onInput} ref={linkInputRef}/>

            <section className={style["input"]}>
                <textarea placeholder="본문에 표시될 텍스트" rows={2} spellCheck={false} defaultValue="" ref={linkTitleRef}/>
                <button onClick={insertUrlLinkToEditor}>본문삽입</button>
            </section>
        </div>
    )
}


export default PostLinkInput;