import React, {useRef, useEffect} from "react";
import style from "./styles/postImgUploadResultMessage.module.scss";
import MessageWriter from "./messageWriter";
import {StretchOpenMessageType} from "../index";


const message = (type) => {
    switch (type) {
        case StretchOpenMessageType.POST_TEMP_SAVE_RESULT.TEMP_SAVE_FAILED:
            return "임시저장 실패, 허용되지 않은 HTML 태그로 인해 작성글이 저장/등록되지 않을 수 있습니다.";
        case StretchOpenMessageType.POST_TEMP_SAVE_RESULT.TEMP_SAVE_SUCCESS:
            return "임시저장 되었습니다.";
    }

}


const PostTempSaveResultMessage = ({messageType, isBoardOpened, closeBoardFunc}) => {
    const messageWriterRef = useRef(null);



    useEffect(() => {
        if (isBoardOpened) messageWriterRef.current.write(message(messageType));

        return () => {
            if (messageWriterRef.current) messageWriterRef.current.clear();
        }
    }, [isBoardOpened])


    return (
        <div className={style["container"]}>
            <MessageWriter printLeftToRight={true} ref={messageWriterRef}/>
            <button className={style["confirm"]} onClick={closeBoardFunc}>--&gt; 확인 &lt;--</button>
        </div>
    )
}


export default PostTempSaveResultMessage;