import React, {useRef, useEffect} from "react";
import style from "./styles/Message.postUpload.module.scss";
import MessageWriter from "./messageWriter";
import {StretchOpenMessageType} from "../index";
import {useSelector} from "react-redux";


const message = (type, volume, issue) => {


    switch (type) {
        case StretchOpenMessageType.COMIC_BOOK.REGISTER.ISSUE_NOT_AVAILABLE:
            return `이슈${issue}로 이미 등록하셨습니다.`;
        case StretchOpenMessageType.COMIC_BOOK.REGISTER.VOLUME_ISSUE_NOT_AVAILABLE:
            return `볼륨${volume} 이슈${issue}로 이미 등록하셨습니다.`;
        case StretchOpenMessageType.COMIC_BOOK.REGISTER.SHORT_STORY_NOT_AVAILABLE:
            return "이미 단편으로 등록하셨습니다.";
    }

}


const NotRegistableComicBookMessage = ({messageType, isBoardOpened, closeBoardFunc}) => {
    const comicBookEditorState = useSelector(state => state.comicBook);

    const {volume, issue} = comicBookEditorState;


    const messageWriterRef = useRef(null);



    useEffect(() => {
        if (isBoardOpened) messageWriterRef.current.write(message(messageType, volume, issue));

        return () => {
            if (messageWriterRef.current) messageWriterRef.current.clear();
        }
    }, [isBoardOpened])


    return (
        <div className={style["container"]}>
            <MessageWriter printLeftToRight={false} ref={messageWriterRef}/>
            <button className={style["confirm"]} onClick={closeBoardFunc}>--&gt; 확인 &lt;--</button>
        </div>
    )
}


export default NotRegistableComicBookMessage;