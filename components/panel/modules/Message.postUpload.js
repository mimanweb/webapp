import React, {useRef, useEffect} from "react";
import style from "./styles/Message.postUpload.module.scss";
import MessageWriter from "./messageWriter";
import {StretchOpenMessageType} from "../index";


const message = (type) => {
    switch (type) {
        case StretchOpenMessageType.POST.INVALID_PARAM.NO_NICKNAME:
            return "닉네임이 입력되지 않았습니다.";
        case StretchOpenMessageType.POST.INVALID_PARAM.NO_PASSWORD:
            return "비밀번호가 입력되지 않았습니다.";
        case StretchOpenMessageType.POST.INVALID_PARAM.TOO_SHORT_PASSWORD:
            return "비밀번호가 너무 짧습니다. (4자 이상)";
        case StretchOpenMessageType.POST.INVALID_PARAM.EMPTY_CONTENT:
            return "글 내용이 입력되지 않았습니다.";
        case StretchOpenMessageType.POST.INVALID_PARAM.CONTENT_INVALID_EL_INCLUDED:
            return "유효하지 않은 HTML 요소 혹은 외부 소스 사용으로 인해 작성글이 저장/등록될 수 없습니다.";
        case StretchOpenMessageType.POST.INVALID_PARAM.NO_TITLE:
            return "글 제목이 입력되지 않았습니다.";
    }

}


const PostParamCheckMessage = ({messageType, isBoardOpened, closeBoardFunc}) => {
    const messageWriterRef = useRef(null);



    useEffect(() => {
        if (isBoardOpened) messageWriterRef.current.write(message(messageType));

        return () => {
            if (messageWriterRef.current) messageWriterRef.current.clear();
        }
    }, [isBoardOpened])


    return (
        <div className={style["container"]}>
            <MessageWriter printLeftToRight={true} ref={messageWriterRef}/>
            <button className={style["confirm"]} onClick={closeBoardFunc}>--&gt; 확인 &lt;--</button>
        </div>
    )
}


export default PostParamCheckMessage;