import React, {useState, useEffect, forwardRef, useImperativeHandle} from "react";
import {useSelector} from "react-redux";
import style from "./right-sidebar.module.scss";



import LoginPanel from "./modules/login-panel";
import UserInfoPanel from "./modules/userinfo-panel";
import Panel_Subscriptions from "./modules/Panel.subscriptions";
import Panel_Ad from "./modules/Panel.ad";



/**
 * User user panel
 * Subscription list panel
 * Ad panel
 * */
const RightSidebar = forwardRef(({}, ref) => {
    const [stickyAd, setStickyAd] = useState(true);


    const isLoggedIn = useSelector(state => {
        return !!state.user.id
    });



    useImperativeHandle(ref, () => ({
        setStickyOnAd: () => {
            setStickyAd(true);
        },
        removeStickyFromAd: () => {
            setStickyAd(false);
        }
    }))




    return (
        <aside className={style['container']}>

            {/** SECTION TOP */}
            <section className={style["top"]}>
                {(!isLoggedIn)? (<LoginPanel/>): (<UserInfoPanel/>)}

                <Panel_Subscriptions/>
            </section>

            {/** SECTION MIDDLE */}
            <section className={`${style["middle"]} ${!stickyAd? style["no_sticky"]: ""}`}>
                <Panel_Ad/>
            </section>
        </aside>
    )
});


export default RightSidebar;