import React, {useRef, useEffect} from "react";
import Link from "next/link";
import style from "./Panel.subscription.module.scss";
import {useSelector} from "react-redux";
import {parseImgId} from "../../../utils/image/parse";
import _ from "underscore";
import time from "../../../utils/time";
import {parseComicBookEpText} from "../../../utils/format";




const Card = ({type, title, originTitle, desc, number, imageUrl, owner, publishedTime, episodeIndex, comicsNumber, comicsId}) => {
    const cardRef = useRef(null);


    const stopPropagation = (e) => {
        e.preventDefault();
        e.stopPropagation();
    }

    const onDeleteButtonClick = (e) => {
        stopPropagation(e);

        const parent = cardRef.current.parentNode;

        parent.removeChild(cardRef.current)
    }


    const episodeType = episodeIndex.isShortStory? "단편": `#${episodeIndex.issue}`;
    const episodeTypeLong = parseComicBookEpText(episodeIndex);


    const comicBookLinkPath = {
        pathname: `/comics/book/${comicsNumber}/${comicsId}`,
        query: {translator: owner.userId}
    }

    if (episodeIndex.isShortStory) {
        comicBookLinkPath.query["short-story"] = episodeIndex.shortStoryNumber;
    }
    else if (episodeIndex.isVolume) {
        comicBookLinkPath.query.vol = episodeIndex.volume;
        comicBookLinkPath.query.issue = episodeIndex.issue;
    }
    else {
        comicBookLinkPath.query.issue = episodeIndex.issue;
    }


    return (
        <div className={style["card"]}>

            {/** DEFAULT CONTENTS */}
            <div className={style["default-content"]}>
                <span className={style["title"]}>{title}</span>
                {/*<span className={style["dotdot"]} ref={dotdotSpanRef}>..</span>*/}
                <div className={style["number"]}>{episodeType}</div>
            </div>

            {/** OPEN CONTENT */}
            <Link href={comicBookLinkPath}>
                <a className={style["open-content"]}>
                    {imageUrl? (
                        <div className={style["thumbnail"]}>
                            <img src={imageUrl}/>
                        </div>
                    ): (
                        <div className={style["no_thumbnail"]}>
                            <p>No Thumbnail</p>
                        </div>
                    )}


                    <div className={style["divider"]}>
                        <div/>
                    </div>

                    <div className={style["desc"]}>
                        <div className={style["title"]}>
                            <p className={style["number"]}>{episodeTypeLong}</p>
                            <p className={style["text"]}>{title}</p>
                            <p className={style["origin"]}>{originTitle}</p>
                        </div>
                        {desc && (
                            <div className={style["summary"]}>
                                <p className={style["content"]}>{desc}</p>
                            </div>
                        )}
                    </div>
                </a>
            </Link>

            {/*<div className={style["delete"]} title="구독목록에서 삭제" onClick={onDeleteButtonClick}/>*/}
            <div className={style["bottom"]}>
                <div className={style["translator"]} onClick={stopPropagation} title={`번역자 - ${owner.nickname}(${owner.userId})`}>번역 {owner.nickname}</div>
                <div className={style["up-time"]} onClick={stopPropagation} title={`${time.fullFormatText(publishedTime)} 업로드`}>{time.shortFormatText(publishedTime)}</div>
            </div>
        </div>
    )
}


const Subscriptions = () => {
    const comicsState = useSelector(state => state.comics);

    const {recentReadComics} = comicsState;



    return (
        <div className={style["container"]}>
            <section className={style["top"]}>
                <p className={style["subscription_page"]}>
                    최근 본 작품
                    {/*<div className={style["small"]}>더보기></div>*/}
                </p>
            </section>

            <section className={style["middle"]}>
                {recentReadComics.map((comics, key) => {
                    if (key<=7) {

                        const {title, latestComicBook, number: comicsNumber, id: comicsId} = comics;

                        const {linkThumbnail, owner, time, index: episodeIndex} = latestComicBook;
                        const imgUrl = linkThumbnail? parseImgId(linkThumbnail.imgId.large): null;


                        return (
                            <Card key={key} type="COMICS" title={title.korean} originTitle={title.origin} imageUrl={imgUrl} owner={owner} publishedTime={time.publish} episodeIndex={episodeIndex} comicsNumber={comicsNumber} comicsId={comicsId}/>
                        )
                    }
                })}


                {/*<div className={style["more-dots"]} title="구독목록 27개 더보기"/>*/}
            </section>
        </div>
    )
};


export default Subscriptions;