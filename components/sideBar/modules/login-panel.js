import React, {useState, useEffect} from "react";
import Link from "next/link";
import {useDispatch, useSelector, shallowEqual} from "react-redux";
import {loginRequest} from "../../../features/user/actions";


// Styles
import style from "./login-panel.module.scss";


// Components
import XSmallButton, {XSmallButtonType} from "../../button/x-small";



export default function LoginPanel() {
    const dispatch = useDispatch();
    const [userId, setUserId] = useState("");
    const [userPassword, setUserPassword] = useState("");
    const [stayLoggedIn, setStayLoggedIn] = useState(false);
    const [isStayLoggedInStored, setIsStayLoggedInStored] = useState(false);


    // Set stay logged in checkbox as local stored value
    useEffect(() => {
        const stored = localStorage.getItem("STAY_LOGGED_IN")==="true"

        if (!isStayLoggedInStored) {
            setStayLoggedIn(stored)
            setIsStayLoggedInStored(true);
        }

        localStorage.setItem("STAY_LOGGED_IN", stayLoggedIn.toString())
    }, [stayLoggedIn])



    const onSubmit = (e) => {
        e.preventDefault();

        if (!userId) return;
        if (!userPassword) return;

        dispatch(loginRequest(userId, userPassword, stayLoggedIn));
    }


    return (
        <div className={style["container"]}>

            {/** INPUT ID/PW FORM */}
            <form onSubmit={e=>e.preventDefault()}>
                <section className={style["data"]}>
                    <input type='text' placeholder='아이디' onChange={(e) => setUserId(e.target.value)} spellCheck={false}/>
                    <input type='password' placeholder='비밀번호' onChange={(e) => setUserPassword(e.target.value)} spellCheck={false}/>
                </section>

                <section className={style["menu"]}>
                    <div className={style["stay-logged-in"]} onClick={() => setStayLoggedIn(!stayLoggedIn)}>
                        <div className={stayLoggedIn? style["checkbox"]: style["checkbox_blank"]}/>
                        <label>로그인유지</label>
                    </div>
                    
                    <XSmallButton buttonType={XSmallButtonType.LOGIN} submit={true} onClick={onSubmit}/>
                </section>
            </form>

            {/** ACCOUNT SECTION */}
            <section className={style["account"]}>
                <Link href={"/account/find"}>
                    <label className={style["find-account"]}>ID/PW 찾기</label>
                </Link>

                <Link href={"/account/registration"}>
                    <label className={style["register"]}>
                        <div className={style["icon"]}/>
                        <label>회원가입</label>
                    </label>
                </Link>
            </section>
        </div>
    )
};