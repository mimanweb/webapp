import React from "react";
import style from "./Panel.ad.module.scss";
import {useRouter} from "next/router";


import {AD_300X600, AD_160X600} from "../../ad/index";


const Ad = () => {
    const router = useRouter();
    const wide = [
        // "/post/write/[...index]",
        //
        // "/board/community/notice",
        // "/post/community/notice/[post]",
        // "/board/community/freeboard",
        // "/post/community/freeboard/[post]",
        // "/board/community/humor",
        // "/post/community/humor/[post]",
        //
        // "/board/comics/freeboard",
        // "/post/comics/freeboard/[post]",
    ].includes(router.route);


    return (
        <div className={style["container"]}>
            <section className={wide? style["wide"]: null}>
                {wide? <AD_300X600/>: <AD_160X600/>}
            </section>
        </div>
    )
};


export default Ad;