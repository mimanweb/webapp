import React, {useState} from "react";
import Link from "next/link";
import {useDispatch, useSelector, shallowEqual} from "react-redux";
import style from "./userinfo-panel.module.scss";


// Components
import XSmallButton, {XSmallButtonType} from "../../../components/button/x-small";


// Actions
import {
    logoutRequest,
    requestUserNotificationList, setUserNotificationListState,
    setUserUnreadNotificationState
} from "../../../features/user/actions";
import RightSidebarUserNotificationPanel from "./userInfoPanelAssets/notificationPanel";
import router from "next/router";
import {NOTIFICATION_ITEMS_PER_PAGE} from "../../../utils/index/user";




const MSG_BOARD_TYPE = {
    NOTIFICATION: "NOTIFICATION"
}



export default function UserInfoPanel() {
    const dispatch = useDispatch();
    const userState = useSelector(state => state.user);

    const {id: userId, nickname, hasUnreadNotification} = userState;


    const [msgBoardType, setMsgBoardType] = useState(null);



    const logout = () => {
        dispatch(logoutRequest());
    }



    const menuClick = async (menuType) => {
        if (msgBoardType===menuType) setMsgBoardType(null);
        else {

            switch (menuType) {
                case MSG_BOARD_TYPE.NOTIFICATION:
                    const {status, data} = await requestUserNotificationList(1, NOTIFICATION_ITEMS_PER_PAGE)

                    switch (status) {
                        case 200:
                            dispatch(setUserUnreadNotificationState(false));
                            dispatch(setUserNotificationListState(data.notificationList, data.totalPageCount, 1));
                            break;
                        case 403:
                            router.reload();
                            break;
                    }
                    break;
            }

            setMsgBoardType(menuType);
        }
    }


    const closeMenuPanelFunc = () => {
        setMsgBoardType(null);
    }



    return (
        <div className={style["container"]}>

            {/** SECTION TOP */}
            <section className={style["user_info"]}>
                <label className={style["nickname"]} title={userId}>{nickname}</label>
                <Link href={"/account/setting"}>
                    <label className={style["setting"]} title="내 정보 설정">
                        <div className={style["icon"]}/>
                        <label>내 정보</label>
                    </label>
                </Link>
            </section>


            {/** SECTION MIDDLE 1 */}
            <section className={style["noti_mail_mypost"]}>

                <div className={style["notification"]}>
                    <div className={style["icon"]} title="알림보기" onClick={()=>menuClick(MSG_BOARD_TYPE.NOTIFICATION)}/>

                    {/** New notification indicator*/}
                    {hasUnreadNotification && (
                        <div className={style["new_item_indicator"]}/>
                    )}
                </div>


                <div className={style["mail"]}>
                    <div className={style["icon"]} title="쪽지보기(준비중)" style={{cursor: "default"}}/>
                </div>


                <div className={style["post"]}>
                    <div className={style["icon"]} title="내글보기(준비중)" style={{cursor: "default"}}/>
                </div>
            </section>



            <section className={`${style["noti_mail_mypost_message"]} ${msgBoardType? style["enabled"]: ""}`}>
                <RightSidebarUserNotificationPanel closeMenuPanelFunc={closeMenuPanelFunc}/>
            </section>



            {/** SECTION MIDDLE 2 */}
            <section className={style["bookmark_logout"]}>
                <Link href="/">
                    <div className={style["bookmarks"]} title="북마크목록">
                        <div className={style["icon"]}/>
                        <label>북마크</label>
                    </div>
                </Link>
                <XSmallButton buttonType={XSmallButtonType.LOGOUT} onClick={() => logout()}/>
            </section>

        </div>
    )
};