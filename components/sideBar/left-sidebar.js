import style from "./left-sidebar.module.scss";
import React from "react";
import Link from "next/link";
import {SiteIndex} from "../../utils/index/siteIndex";


const COMMUNITY_BOARD_ITEMS = Object.values(SiteIndex.Community.boards);

const COMICS_BOARD_ITEMS = Object.values(SiteIndex.Comics.boards);

const ANIMATION_BOARD_ITEMS = Object.values(SiteIndex.Animation.boards);




const BoardLink = ({path, name, isNotReady}) => {

    if (isNotReady) {
        return (
            <span className={style['board']} title="공사중">{name}</span>
        )
    } else {
        return (
            <Link href={path}><a className={style['board']}>{name}</a></Link>
        )
    }
}

const BoardSection = ({name, type, boardList}) => {
    return (
        <section className={style["board-index"]}>
            <div className={style["category"]}>
                <label>{name}</label>
            </div>
            {boardList.filter(board => board.showOnSidebar).map((board, idx) => <BoardLink key={idx} name={board.name} path={board.path} isNotReady={board.isNotReady}/>)}
        </section>
    )
}

export default function LeftSidebar() {
    return (
        <aside className={style["container"]}>
            <section className={style["site-index"]}>
                <BoardSection type={SiteIndex.Community.code} name={SiteIndex.Community.name} boardList={COMMUNITY_BOARD_ITEMS}/>
                <BoardSection type={SiteIndex.Comics.code} name={SiteIndex.Comics.name} boardList={COMICS_BOARD_ITEMS}/>
                <BoardSection type={SiteIndex.Animation.code} name={SiteIndex.Animation.name} boardList={ANIMATION_BOARD_ITEMS}/>
            </section>
        </aside>
    )
}