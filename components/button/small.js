import style from "./small.module.scss"
import React from "react";


const option = {
    POST_UPLOAD: {
        text: "등록",
        style: "post-upload"
    },
    POST_CANCEL: {
        text: "취소",
        style: "post-cancel"
    },
    POST_TEMP_SAVE: {
        text: "임시저장",
        style: "post-temp-save"
    },
    TO_HOME: {
        text: "홈으로",
        icon: true,
        style: "to_home"
    },
    GO_BACK: {
        text: "뒤로가기",
        style: "go_back"
    },
    WRITE_POST: {
        text: "글쓰기",
        icon: true,
        style: "write_post"
    },
    UPLOAD_NOTICE_POST: {
        text: "공지글로 등록",
        style: "upload_notice_post"
    },
    COMICS: {
        SEARCH: {
            text: "COMICS 검색",
            style: "comics_series_search"
        },
        REGISTER: {
            text: "새로운 COMICS 등록",
            style: "comics_series_register",
        }
    },
    COMIC_BOOK: {
        REGISTER: {
            text: "등록",
            style: "comic_book_register_book",
        },
        REGISTER_EDIT: {
            text: "완료",
            style: "comic_book_register_editing_book",
        },
        CANCEL_WRITING: {
            text: "취소",
            style: "comic_book_cancel_writing",
        }
    }
}

export const SmallButtonType = option;


const IconArea = () => {
    return (
        <>
            <div className={style["icon"]}/>
            <i/>
        </>
    )
}


export default function SmallButton({buttonType, submit, onClick}) {
    return (
        <button className={`${style["button"]} ${style[buttonType.style]}`} type={submit? "submit": "button"} onClick={onClick}>
            {buttonType.icon && <IconArea/>}

            <div className={style["text"]}>
                <label>{buttonType.text}</label>
            </div>
        </button>
    )
}