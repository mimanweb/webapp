import style from "./x-small.module.scss"
import React from "react";


const option = {
    USER: {
        UNREGISTER: {
            text: "회원탈퇴",
            style: "user_unregister"
        }
    },
    EDIT_POST: {
        text: "글수정",
        icon: true,
        style: "edit-post"
    },
    DELETE_POST: {
        text: "글삭제",
        icon: true,
        style: "delete-post"
    },
    LOGIN: {
        text: "로그인",
        style: "login"
    },
    LOGOUT: {
        text: "로그아웃",
        style: "logout"
    },
    TO_HOME: {
        text: "홈으로",
        icon: true,
        style: "to_home"
    },
    COMIC_BOOK: {
        EDIT: {
            text: "수정",
            style: "comic_book_edit"
        },
        DELETE: {
            text: "삭제",
            style: "comic_book_delete"
        }
    }
}


export const XSmallButtonType = option;


const IconArea = () => {
    return (
        <>
            <div className={style["icon"]}/>
            <i/>
        </>
    )
}


export default function XSmallButton({buttonType, submit, onClick}) {
    return (
        <button className={`${style["button"]} ${style[buttonType.style]}`} type={submit? "submit": "button"} onClick={onClick}>
            {buttonType.icon && <IconArea/>}

            <div className={style["text"]}>
                <label>{buttonType.text}</label>
            </div>
        </button>
    )
}