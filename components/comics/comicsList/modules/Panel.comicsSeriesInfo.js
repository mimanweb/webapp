// import React from "react";
// import style from "./Panel.comicsSeriesInfo.module.scss";
//
//
//
//
//
// const ComicsItem = () => {
//     return (
//         <li className={style["comics_series_item"]}>
//             <section className={style["info"]}>
//                 <div className={style["title"]}>
//                     <p className={style["kor"]}>어메이징 스파이더맨</p>
//                     <p className={style["origin"]}>The Amazing Spider-Man</p>
//                 </div>
//             </section>
//
//             <section className={style["list"]}>
//
//             </section>
//         </li>
//     )
// }
//
//
//
// const ComicsSeriesInfoPanel = () => {
//     return (
//         <div className={style["container"]}>
//
//             {/** COMICS SERIES INFO */}
//             <section className={style["series_info"]}>
//                 <section className={style["doc_title"]}>
//                     <div>
//                         <p className={style["com_logo"]}>MIMANWEB</p>
//                     </div>
//                     <div>
//                         <p className={style["form_title"]}>코믹스 시리즈</p>
//                         <p className={style["form_sub_title"]}>COMICS SERIES</p>
//                     </div>
//                 </section>
//
//                 <section className={style["comics_series_title"]}>
//                     <p className={style["kor_title"]}>어메이징 스파이더맨</p>
//                     <p className={style["origin_title"]}>The Amazing Spider-Man</p>
//                 </section>
//             </section>
//
//
//             {/** DIVIDER */}
//             <section className={style["divider"]}>
//                 <div className={style["poly"]}/>
//             </section>
//
//
//             {/** COMICS SERIES EDITIONS */}
//             <section className={style["related_item"]}>
//                 <section className={style["title"]}>
//                     <p className={style["form_title"]}>시리즈 작품 목록</p>
//                     <p className={style["form_sub_title"]}>SERIES COLLECTED EDITIONS</p>
//                 </section>
//
//                 <section className={style["items"]}>
//                     <section className={style["list"]}>
//                         <ul>
//                             <ComicsItem/>
//                             <ComicsItem/>
//                         </ul>
//                     </section>
//
//                     <section className={style["add_new_item"]}>
//                         <div className={style["button"]}>
//                             <div className={style["icon"]}/>
//                             <p>코믹스 작품 등록</p>
//                         </div>
//                     </section>
//                 </section>
//
//             </section>
//         </div>
//     )
// }
//
//
//
// export default ComicsSeriesInfoPanel;