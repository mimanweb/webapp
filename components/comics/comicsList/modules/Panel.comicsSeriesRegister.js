import React, {useRef, useState, useImperativeHandle, forwardRef} from "react";
import style from "./Panel.comicsSeriesRegister.module.scss";
import Keyboard from "../../../../utils/editor/keyboard";
import {requestCreateComics} from "../../../../features/comics/actions";
import {useSelector} from "react-redux";


const COMICS_REG_STATUS = {
    SUCCESS: "SUCCESS",
    FAILED: "FAILED",
    WRITING: "WRITING"
}



const TextArea = forwardRef(({}, ref) => {
    const textareaRef = useRef(null);



    const onKeyDown = (e) => {
        if (e.keyCode===Keyboard.KEY.ENTER) e.preventDefault();
    }


    const autoResize = () => {
        const textarea = textareaRef.current;
        textarea.style.height = "auto";

        const scrollHeight = textarea.scrollHeight;

        textarea.style.height = `${scrollHeight}px`;
    }


    useImperativeHandle(ref, () => ({
        getText: () => {
            return textareaRef.current.value;
        },
        clearText: () => {
            textareaRef.current.value = "";
        }
    }))


    return (
        <textarea spellCheck={false} onInput={autoResize} onKeyDown={onKeyDown} ref={textareaRef}/>
    )
})



const ComicsSeriesRegisterPanel = ({comicsListRef}) => {
    const [comicsRegStatus, setComicsRegStatus] = useState(COMICS_REG_STATUS.WRITING);
    const [comicsKoreanTitle, setComicsKoreanTitle] = useState("");
    const [comicsOriginTitle, setComicsOriginTitle] = useState("");

    const userState = useSelector(state => state.user);
    const isRegUser = userState.loggedIn;


    const korTitleRef = useRef(null);
    const originTitleRef = useRef(null);



    const onRegisterButtonClick = async () => {
        const korTitle = korTitleRef.current.getText();
        const originTitle = originTitleRef.current.getText();

        if (
            !korTitle ||
            !/[\S]/g.test(korTitle) ||
            !originTitle ||
            !/[\S]/g.test(originTitle)
        ) return;


        const {status, data} = await requestCreateComics({korean: korTitle, origin: originTitle});



        switch (status) {
            case 203:
                setComicsKoreanTitle(korTitle);
                setComicsOriginTitle(originTitle);
                setComicsRegStatus(COMICS_REG_STATUS.SUCCESS);

                comicsListRef.current.search(korTitle, data.comicsDataOid);
                break;
        }
    }
    
    
    const onResetButtonClick = () => {
        setComicsRegStatus(COMICS_REG_STATUS.WRITING);
    }





    return (
        <div className={style["container"]}>

            <div className={style["main"]}>
                <section className={style["doc_title"]}>
                    <p className={style["com_logo"]}>MIMANWEB</p>
                    <p className={style["form_title"]}>
                        {
                            comicsRegStatus===COMICS_REG_STATUS.WRITING? "코믹스 등록":
                            comicsRegStatus===COMICS_REG_STATUS.SUCCESS? "등록 완료": ""
                        }
                    </p>
                    <p className={style["form_sub_title"]}>
                        {
                            comicsRegStatus===COMICS_REG_STATUS.WRITING? "COMICS REGISTRATION":
                            comicsRegStatus===COMICS_REG_STATUS.SUCCESS? "COMICS REGISTRATION COMPLETE": ""
                        }
                    </p>
                </section>


                {comicsRegStatus===COMICS_REG_STATUS.WRITING && (
                    <>
                        <section className={style["reg_comics_note"]}>
                            <p>구글 드라이브 PDF, 개인 블로그 등의 링크 연결은 즉시 사용 가능합니다.</p>
                            <p>파일 업로드는 해당 코믹스의 적합 여부 심사 이후 사용 가능합니다.</p>
                        </section>

                        <section className={style["reg_comics_info"]}>
                            <div className={style["item_desc"]}>
                                코믹스 이름(한글)
                            </div>
                            <div className={style["item_desc_example"]}>
                                예시)&nbsp;&nbsp;워킹데드
                            </div>
                            <section className={style["input"]}>
                                <TextArea ref={korTitleRef}/>
                            </section>

                            <div className={style["item_desc"]}>
                                코믹스 이름(영문)
                            </div>
                            <div className={style["item_desc_example"]}>
                                예시)&nbsp;&nbsp;The Walking Dead
                            </div>
                            <section className={style["input"]}>
                                <TextArea ref={originTitleRef}/>
                            </section>

                        </section>
                    </>
                )}

                {comicsRegStatus===COMICS_REG_STATUS.SUCCESS && (
                    <section className={style["reg_success_msg"]}>
                        <p className={style["comics_kor_title"]}>{comicsKoreanTitle}</p>
                        <p className={style["comics_origin_title"]}>{comicsOriginTitle}</p>

                        <p className={style["guide"]}>아래 코믹스 목록에서 번역 볼륨/이슈를 등록해 주십시오.</p>
                    </section>
                )}

                
                <section className={style["buttons"]}>
                    {!isRegUser && (
                        <div className={style["register_button"]}>등록하기(회원가입 필요)</div>
                    )}
                    {
                        (isRegUser && comicsRegStatus===COMICS_REG_STATUS.WRITING)? (
                            <div className={style["register_button"]} onClick={onRegisterButtonClick}>등록하기</div>
                        ): (isRegUser && comicsRegStatus===COMICS_REG_STATUS.SUCCESS)? (
                            <div className={style["register_button"]} onClick={onResetButtonClick}>또 하나 등록하기</div>
                        ): null
                    }

                </section>
            </div>
        </div>
    )
}



export default ComicsSeriesRegisterPanel;