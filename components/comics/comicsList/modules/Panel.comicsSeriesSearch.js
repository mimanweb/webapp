import React, {useRef, useEffect} from "react";
import style from "./Panel.comicsSeriesSearch.module.scss";
import MessageWriter from "../../../panel/modules/messageWriter";


const ComicsSeriesSearchPanel = () => {

    const messageBoardRef = useRef(null);



    useEffect(() => {
        messageBoardRef.current.write("번역할 코믹스를 검색해 주십시오.");
    }, [])


    return (
        <div className={style["box"]}>
            <div className={style["container"]}>
                <div className={style["list"]}>
                    <ul>
                        <li>어메이징 스파이더맨 더 그레이트 올 마이티(The Amazing Spider-Man The Great All Mighty)</li>
                        <li>캡틴 아메리카 스노우</li>
                        <li>캡틴 아메리카 스노우</li>
                    </ul>
                </div>

                <div className={style["message"]}>
                    <MessageWriter printLeftToRight={true} ref={messageBoardRef}/>
                </div>

                <div className={style["menu"]}>
                    <p className={style["site_logo"]}>MIMANWEB</p>
                    <div>
                        <input type="text" spellCheck={false}/>
                        <p>검색하기</p>
                    </div>
                </div>
            </div>
        </div>
    )
}



export default ComicsSeriesSearchPanel;