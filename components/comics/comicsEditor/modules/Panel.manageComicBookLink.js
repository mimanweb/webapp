import React, {useRef} from "react";
import style from "./Panel.manageComicBookLink.module.scss"
import {autoResizeTextarea} from "../../../../utils/editor/textarea";
import {dataURLtoFile} from "../../../../utils/image";
import {
    requestUpdateComicBookInfo,
    requestUploadComicBookPageImage,
    setComicBookState
} from "../../../../features/comicsBook/actions";
import {useDispatch, useSelector} from "react-redux";
import {parseImgId} from "../../../../utils/image/parse";


const ComicBookLinkManagePanel = () => {
    const dispatch = useDispatch();

    const comicsBookEditorState = useSelector(state => state.comicBook);

    const {comicBookDataId, tempLinkThumbnail, link} = comicsBookEditorState;

    const textareaRef = useRef(null);
    const imgInputRef = useRef(null);



    const onThumbnailUploadButtonClick = () => {
        imgInputRef.current.click();
    }


    const onImgInput = async () => {
        const imgInput = imgInputRef.current;

        if (imgInput.files.length<1) return;


        const readAllFiles = [...Array(imgInput.files.length).keys()].map(i => {
            return new Promise((resolve, reject) => {
                const file = imgInput.files[i];
                const fileExt = file.type.split("/")[1];


                /**
                 * Check file type
                 * */
                if (![
                    "image/jpg",
                    "image/jpeg",
                    "image/png"
                ].includes(file.type)) {
                    //TODO: 파일 타입 에러 메시지
                    return;
                }

                /**
                 * Check file size
                 * */
                if (file.size > 1.5e+7) {
                    //TODO: 파일 사이즈 에러 메시지
                    return;
                }


                const reader = new FileReader();


                /**
                 * File reader on-load
                 * */
                reader.onload = (e) => {
                    const dataUrl = e.target.result;
                    const loadedFile = dataURLtoFile(dataUrl, `cover.${fileExt}`)

                    resolve(loadedFile);
                }

                /**
                 * File reader on-error
                 * */
                reader.onerror = (e) => {
                    reject();
                }

                /**
                 * Read image file
                 * */
                reader.readAsDataURL(file);
            })
        })


        const fileList = await Promise.all(readAllFiles).then((files)=>{
            return files
        })



        for (const file of fileList) {

            const {status, data} = await requestUploadComicBookPageImage(comicBookDataId, file, true);

            switch (status) {
                case 200:
                    const {comicBookData} = data;

                    dispatch(setComicBookState(comicBookData));
                    break;
            }
        }


        /**
         * Clear input
         * */
        imgInput.value = "";
    }


    const onLinkInput = async () => {
        autoResizeTextarea(textareaRef);

        const text = textareaRef.current.value;

        await requestUpdateComicBookInfo({comicBookDataId: comicBookDataId, link: text});
    }




    return (
        <div className={style["container"]}>
            <section className={style["link_input"]}>
                <textarea placeholder="연결할 URL 입력" rows={2} spellCheck={false} defaultValue={link} onInput={onLinkInput} ref={textareaRef}/>
            </section>

            <section className={style["info_display"]}>
                <div className={style["book_cover_upload"]}>
                    <div className={style["button"]} onClick={onThumbnailUploadButtonClick}>
                        썸네일(커버) 이미지 업로드
                        <input type="file" accept=".jpg,.jpeg,.png" multiple={false} ref={imgInputRef} onChange={onImgInput}/>
                    </div>
                </div>

                <div className={style["book_cover"]}>
                    {tempLinkThumbnail? (
                        <img src={parseImgId(tempLinkThumbnail)}/>
                    ): (
                        <div className={style["no_thumbnail"]}>
                            <span>썸네일 없음</span>
                        </div>
                    )}
                </div>
            </section>
        </div>
    )
}



export default ComicBookLinkManagePanel;