import React, {useRef, useState, useEffect} from "react";
import style from "./Panel.manageComicsBookPages.module.scss";
import Dots from "../../../animation/loading/dots";
import {StretchOpenMessageType} from "../../../panel";
import {dataURLtoFile} from "../../../../utils/image";
import {requestUpdateComicsCoverImage} from "../../../../features/comics/actions";
import {
    requestUploadComicBookPageImage,
    setComicBookState,
    setComicBookPageOnDrag,
    setComicBookPageOnDragOver,
    setSelectedComicBookPage,
    requestUpdateComicBookSinglePageType,
    requestUpdateComicBookDoublePageType,
    requestDeleteComicBookPage,
    requestUpdateComicBookPageOrder
} from "../../../../features/comicsBook/actions";
import {useSelector, useDispatch} from "react-redux";
import _ from "underscore";
import {parseImgId} from "../../../../utils/image/parse";
import {COMIC_BOOK_PAGE_TYPE} from "../../../../utils/index/comics/book";



const PAGE_SIDE = {
    LEFT: "left",
    RIGHT: "right"
}





const PageImg = ({pageParam}) => {
    const dispatch = useDispatch();

    const comicsBookEditorState = useSelector(state => state.comicBook);

    const {comicBookDataId, pageNumberOnDrag, pageNumberOnDragOver, selectedPageNumber, pageList} = comicsBookEditorState;

    const [isDragPadEnabled, setIsDragPadEnabled] = useState(false);
    const [pageSideOnDrag, setPageSideOnDrag] = useState(null);
    const [isLeftOfSelected, setIsLeftOfSelected] = useState(null);
    const [isRightOfSelected, setIsRightOfSelected] = useState(null);
    const [isMouseOverPage, setIsMouseOverPage] = useState(false);


    const {pageNumber, pageType, imgId, firstPage, secondPage} = pageParam;


    const isCover = pageNumber===1;
    const isSelectedPage = selectedPageNumber===pageNumber;
    const isOnDragPage = pageNumber===pageNumberOnDrag;
    const isDragOverPage = pageNumber===pageNumberOnDragOver;
    const isWidePage = pageType===COMIC_BOOK_PAGE_TYPE.WIDE;
    const isPairedPage = pageType===COMIC_BOOK_PAGE_TYPE.PAIRED;
    const isSinglePage = pageType===COMIC_BOOK_PAGE_TYPE.SINGLE;

    const leftPage = pageList.find(page => page.pageNumber===pageNumber-1);
    const rightPage = pageList.find(page => page.pageNumber===pageNumber+1);
    const isLeftPagePairedPage = leftPage && leftPage.pageType===COMIC_BOOK_PAGE_TYPE.PAIRED;
    const isRightPagePairedPage = rightPage && rightPage.pageType===COMIC_BOOK_PAGE_TYPE.PAIRED;



    const onDragStart = (e) => {
        e.dataTransfer.effectAllowed = "move";

        dispatch(setComicBookPageOnDrag(pageNumber));
        dispatch(setSelectedComicBookPage(null));
    }

    const onDrop = async (side) => {
        const pageOnDrag = pageList.find(page => page.pageNumber===pageNumberOnDrag);
        const isPageOnDragPairedPage = pageOnDrag.pageType===COMIC_BOOK_PAGE_TYPE.PAIRED;

        if (
            !isOnDragPage
        ) {
            let targetPageNumber;

            switch (side) {
                case PAGE_SIDE.LEFT:
                    targetPageNumber = isPairedPage? firstPage.pageNumber: pageNumber;
                    break;
                case PAGE_SIDE.RIGHT:
                    targetPageNumber = isPairedPage? secondPage.pageNumber+1: pageNumber+1;
                    break;
            }


            const {status, data} = await requestUpdateComicBookPageOrder(comicBookDataId, targetPageNumber, pageNumberOnDrag, isPageOnDragPairedPage);

            switch (status) {
                case 200:
                    dispatch(setComicBookState(data.comicBookData));
                    break;
            }
        }



        dispatch(setComicBookPageOnDrag(null));
        dispatch(setComicBookPageOnDragOver(null));
        setIsDragPadEnabled(false);
        setPageSideOnDrag(null);
    }

    const onDragOver = (e) => {
        e.preventDefault();
    }

    const onDragEnter = (e) => {
        dispatch(setComicBookPageOnDragOver(pageNumber));
    }

    const onDragLeave = (e) => {
        e.preventDefault();

        if (!isDragOverPage) {
            setIsDragPadEnabled(false);
            setPageSideOnDrag(null);
        }
    }

    const onMouseEnter = (e) => {
        setIsMouseOverPage(true);
    }

    const onMouseLeave = (e) => {
        setIsMouseOverPage(false);

        hidePageBindButton();
        setIsDragPadEnabled(false);
        setPageSideOnDrag(null);
    }

    const onPageClick = () => {
        dispatch(setSelectedComicBookPage(isSelectedPage? null: pageNumber))
    }

    const showPageBindButton = () => {
        if (!isSelectedPage) {
            /**
             * Left page of selected
             * */
            if (pageNumber+1===selectedPageNumber) {
                setIsLeftOfSelected(true);
            }
            /**
             * Right page of selected
             * */
            else if (pageNumber-1===selectedPageNumber) {
                setIsRightOfSelected(true);
            }
        }
    }

    const hidePageBindButton = () => {
        setIsLeftOfSelected(null);
        setIsRightOfSelected(null);
    }


    /**
     * Drag on page half
     * */
    const onDragEnterOnPageHalf = (e, side) => {
        setPageSideOnDrag(side)
    }

    const onDragOverOnPageHalf = (e, side) => {
        e.preventDefault();
    }

    const onDragLeaveOnPageHalf = (e, side) => {
        e.preventDefault();
    }


    useEffect(() => {
        if (isDragOverPage) setIsDragPadEnabled(true);
    }, [pageNumberOnDragOver])



    const changeSinglePageType = async (pageType) => {
        const {status, data} = await requestUpdateComicBookSinglePageType(comicBookDataId, pageNumber, pageType);

        switch (status) {
            case 200:
                dispatch(setComicBookState(data.comicBookData));
                break;
        }
    }

    const changeDoublePageType = async (pageType, firstPageNumber, secondPageNumber) => {
        const {status, data} = await requestUpdateComicBookDoublePageType(comicBookDataId, firstPageNumber, secondPageNumber, pageType);

        switch (status) {
            case 200:
                dispatch(setComicBookState(data.comicBookData));
                dispatch(setSelectedComicBookPage(firstPageNumber));
                break;
        }
    }

    const deletePage = async () => {
        let deleteRes;

        if (isPairedPage) {
            deleteRes = await requestDeleteComicBookPage(comicBookDataId, firstPage.pageNumber, secondPage.pageNumber);
        } else {
            deleteRes = await requestDeleteComicBookPage(comicBookDataId, pageNumber);
        }

        const {status, data} = deleteRes;

        switch (status) {
            case 200:
                dispatch(setComicBookState(data.comicBookData));

                if (isSelectedPage) dispatch(setSelectedComicBookPage(null));
                break;
        }
    }



    return (
        <div className={`${style["page"]} ${isWidePage? style["wide"]: isPairedPage? style["double"]: ""}`} onDragOver={onDragOver} onDragEnter={onDragEnter} onDragLeave={onDragLeave} onMouseEnter={onMouseEnter} onMouseLeave={onMouseLeave}>

            {/** PAGE TYPE(SINGLE/WIDE) TOGGLE BUTTON */}
            {isSelectedPage && (
                <section className={style["page_type_toggle"]}>
                    {isSinglePage && (<kbd onClick={()=>changeSinglePageType(COMIC_BOOK_PAGE_TYPE.WIDE)}>넓은 페이지로</kbd>)}
                    {isWidePage && (<kbd onClick={()=>changeSinglePageType(COMIC_BOOK_PAGE_TYPE.SINGLE)}>보통 페이지로</kbd>)}
                    {isPairedPage && (<kbd onClick={()=>changeDoublePageType(COMIC_BOOK_PAGE_TYPE.SINGLE, firstPage.pageNumber, secondPage.pageNumber)}>페이지 나누기</kbd>)}
                </section>
            )}

            {/** PAGE BIND(PAIR) TOGGLE BUTTON */}
            {(
                !isSelectedPage &&
                (selectedPageNumber===pageNumber-1 || selectedPageNumber===pageNumber+1) &&
                (isLeftOfSelected || isRightOfSelected) &&
                !isPairedPage &&
                !(isRightOfSelected && isLeftPagePairedPage) &&
                !(isLeftOfSelected && isRightPagePairedPage)
            )
            && (
                <kbd
                    className={`${style["page_bind"]} ${isLeftOfSelected? style["left_page"]: style["right_page"]}`}
                    onMouseLeave={hidePageBindButton}
                    onClick={()=>changeDoublePageType(COMIC_BOOK_PAGE_TYPE.PAIRED, isLeftOfSelected? pageNumber: selectedPageNumber, isRightOfSelected? pageNumber: selectedPageNumber)}>페이지 묶기</kbd>
            )}

            {/** DELETE BUTTON */}
            <div className={`${style["delete_button"]} ${isMouseOverPage? style["visible"]: ""}`} title="페이지 삭제" onClick={deletePage}/>


            {/** PAGE HALF DRAG DROP PADS */}
            <div className={`${style["drop_pad"]} ${style["left"]} ${isDragPadEnabled? style["visible"]: ""} ${pageSideOnDrag===PAGE_SIDE.LEFT? style["focused"]: ""}`}
                 onDrop={()=>onDrop(PAGE_SIDE.LEFT)}
                 onDragOver={(e)=>onDragOverOnPageHalf(e, PAGE_SIDE.LEFT)}
                 onDragLeave={(e)=>onDragLeaveOnPageHalf(e, PAGE_SIDE.LEFT)}
                 onDragEnter={(e)=>onDragEnterOnPageHalf(e, PAGE_SIDE.LEFT)}/>
            <div className={`${style["drop_pad"]} ${style["right"]} ${isDragPadEnabled? style["visible"]: ""} ${pageSideOnDrag===PAGE_SIDE.RIGHT? style["focused"]: ""}`}
                 onDrop={()=>onDrop(PAGE_SIDE.RIGHT)}
                 onDragOver={(e)=>onDragOverOnPageHalf(e, PAGE_SIDE.RIGHT)}
                 onDragLeave={(e)=>onDragLeaveOnPageHalf(e, PAGE_SIDE.RIGHT)}
                 onDragEnter={(e)=>onDragEnterOnPageHalf(e, PAGE_SIDE.RIGHT)}/>


            {/** PAGE CONTENT */}
            <div className={style["content"]} onClick={onPageClick} onDragStart={onDragStart} onMouseEnter={showPageBindButton} draggable={true}>
                {/** PAGE TYPE ICON */}
                {
                    (isCover || isWidePage || isPairedPage)
                && (
                    <section className={style["page_type"]}>
                        {isCover && (<div className={style["icon"]} title="커버 페이지">COVER</div>)}
                        {isWidePage && (<div className={style["icon"]} title="넓은 페이지">WIDE PAGE</div>)}
                        {isPairedPage && (<div className={style["icon"]} title="2면 페이지">TWO PAGE</div>)}
                    </section>
                )}

                {/** PAGE IMAGE */}
                <section className={`${style["image"]} ${isSelectedPage? style["outlined"]: ""}`}>
                    {!isPairedPage? (
                        <img src={parseImgId(imgId.medium)}/>
                    ): (
                        <>
                            <img src={parseImgId(firstPage.imgId.medium)}/>
                            <img src={parseImgId(secondPage.imgId.medium)}/>
                        </>
                    )}

                </section>

                {/** PAGE NUMBER ICON */}
                <section className={style["page_number"]}>
                    {!isPairedPage? (
                        <div className={style["icon"]} title={`${pageNumber} 페이지`}>{pageNumber}</div>
                    ): (
                        <>
                            <div className={style["icon"]} title={`${firstPage.pageNumber} 페이지`}>{firstPage.pageNumber}</div>
                            <div className={style["icon"]} title={`${secondPage.pageNumber} 페이지`}>{secondPage.pageNumber}</div>
                        </>
                    )}
                </section>
            </div>
        </div>
    )
}



const ComicsBookImageContentsManagePanel = () => {
    const dispatch = useDispatch();

    const comicsBookEditorState = useSelector(state => state.comicBook);

    const {pageList, comicBookDataId} = comicsBookEditorState;

    const imgInputRef = useRef(null);




    const onImgInput = async () => {
        const imgInput = imgInputRef.current;

        if (imgInput.files.length<1) return;


        const readAllFiles = [...Array(imgInput.files.length).keys()].map(i => {
            return new Promise((resolve, reject) => {
                const file = imgInput.files[i];
                const fileExt = file.type.split("/")[1];


                /**
                 * Check file type
                 * */
                if (![
                    "image/jpg",
                    "image/jpeg",
                    "image/png"
                ].includes(file.type)) {
                    //TODO: 파일 타입 에러 메시지
                    return;
                }

                /**
                 * Check file size
                 * */
                if (file.size > 1.5e+7) {
                    //TODO: 파일 사이즈 에러 메시지
                    return;
                }


                const reader = new FileReader();


                /**
                 * File reader on-load
                 * */
                reader.onload = (e) => {
                    const dataUrl = e.target.result;
                    const loadedFile = dataURLtoFile(dataUrl, `cover.${fileExt}`)

                    resolve(loadedFile);
                }

                /**
                 * File reader on-error
                 * */
                reader.onerror = (e) => {
                    reject();
                }

                /**
                 * Read image file
                 * */
                reader.readAsDataURL(file);
            })
        })


        const fileList = await Promise.all(readAllFiles).then((files)=>{
            return files
        })



        for (const file of fileList) {

            const {status, data} = await requestUploadComicBookPageImage(comicBookDataId, file);

            switch (status) {
                case 200:
                    const {comicBookData} = data;

                    dispatch(setComicBookState(comicBookData));
                    break;
            }
        }






        /**
         * Clear input
         * */
        imgInput.value = "";
    }


    return (
        <div className={style["container"]}>
            <section className={style["menu"]}>
                <div className={style["upload_button"]} onClick={()=>imgInputRef.current.click()}>
                    페이지 이미지 업로드
                    <input type="file" accept=".jpg,.jpeg,.png" multiple={true} ref={imgInputRef} onChange={onImgInput}/>
                </div>
            </section>
            
            <section className={style["images_display"]}>
                {/*<div className={style["loading_image"]}>*/}
                {/*    <Dots/>*/}
                {/*</div>*/}
                {pageList.map(page => {
                    return (
                        <PageImg key={page.pageNumber} pageParam={page}/>
                    )
                })}
            </section>
        </div>
    )
}



export default ComicsBookImageContentsManagePanel;