import React from "react";
import style from "./Panel.editComicsEpisodes.module.scss";
import OptionToggleTable from "../optionToggleTable";
import {
    decreaseComicsIssueState,
    decreaseComicsVolumeState,
    increaseComicsIssueState,
    increaseComicsVolumeState, requestUpdateComicBookInfo, setIsComicsVolumeState,
} from "../../../../features/comicsBook/actions";
import {useDispatch, useSelector} from "react-redux";
import {COMIC_BOOK_EDIT_MODE} from "../../../../utils/index/comics/book";




const Item = ({name, type, disabled, blocked, count, increaseFunc, decreaseFunc}) => {
    const comicsBookEditorState = useSelector(state => state.comicBook);

    const {isShortStory, isVolume, volume, issue, publishedBooks} = comicsBookEditorState;


    let unavailable = false;
    let unavailableMessage = null;


    const longStoryBooks = publishedBooks.filter(book => !book.index.isShortStory);
    const booksWithVol = longStoryBooks.filter(book => book.index.isVolume);
    const booksWithOnlyIssue = longStoryBooks.filter(book => !book.index.isVolume);


    switch (type) {
        case "volume":
            if (booksWithVol.find(book => book.index.volume===count && book.index.issue===issue)) {
                unavailable = true;
                unavailableMessage = `볼륨${volume} 이슈${count}는 이미 등록 하셨습니다.`;
            }
            break;
        case "issue":
            if (!isVolume) {
                if (booksWithOnlyIssue.map(book => book.index.issue).includes(count)) {
                    unavailable = true;
                    unavailableMessage = `이슈${count}는 이미 등록 하셨습니다.`
                }
            } else {
                if (booksWithVol.find(book => book.index.volume===volume && book.index.issue===count)) {
                    unavailable = true;
                    unavailableMessage = `볼륨${volume} 이슈${count}는 이미 등록 하셨습니다.`;
                }
            }
            break;
    }




    return (
        <div className={`${style["option"]} ${disabled? style["disabled"]: ""} ${blocked? style["blocked"]: ""}`}>
            <div className={style["name"]}>
                {name}
            </div>

            <div className={style["buttons"]}>
                <div className={style["sub_button"]} onClick={isShortStory? null: decreaseFunc}><div className={style["icon"]}/></div>
                <div className={`${style["number"]} ${unavailable? style["unavailable"]: ""}`} title={unavailableMessage}>
                    {unavailable? (
                        <del>{count}</del>
                    ): (
                        <span>{count}</span>
                    )}
                </div>
                <div className={style["add_button"]} onClick={isShortStory? null: increaseFunc}><div className={style["icon"]}/></div>
            </div>
        </div>
    )
}



const ComicsEpisodesEditPanel = () => {
    const dispatch = useDispatch();

    const comicsBookState = useSelector(state => state.comicBook);

    const {comicBookDataId, isShortStory, isVolume, volume, issue, editMode} = comicsBookState;

    const isEditMode = editMode===COMIC_BOOK_EDIT_MODE.EDIT;


    const options = {
        exist: {
            text: "볼륨 있음",
            callback: (async ()=>{
                dispatch(setIsComicsVolumeState(true));

                await requestUpdateComicBookInfo({comicBookDataId: comicBookDataId, isVolume: true});
            })
        },
        none: {
            text: "볼륨 없음",
            callback: (async ()=>{
                dispatch(setIsComicsVolumeState(false));

                await requestUpdateComicBookInfo({comicBookDataId: comicBookDataId, isVolume: false});
            })
        }
    }



    const increaseVolume = async () => {
        if (isEditMode) return;

        dispatch(increaseComicsVolumeState());

        await requestUpdateComicBookInfo({comicBookDataId: comicBookDataId, volume: volume+1});
    }

    const decreaseVolume = async () => {
        if (isEditMode) return;

        dispatch(decreaseComicsVolumeState());

        await requestUpdateComicBookInfo({comicBookDataId: comicBookDataId, volume: volume<=0? 0: volume-1});
    }



    const increaseIssue = async () => {
        if (isEditMode) return;

        dispatch(increaseComicsIssueState());

        await requestUpdateComicBookInfo({comicBookDataId: comicBookDataId, issue: issue+1});
    }

    const decreaseIssue = async () => {
        if (isEditMode) return;

        dispatch(decreaseComicsIssueState());

        await requestUpdateComicBookInfo({comicBookDataId: comicBookDataId, issue: issue<=0? 0: issue-1});
    }



    return (
        <section className={style["container"]}>
            <OptionToggleTable options={options} defOption={isVolume? options.exist: options.none} disabled={isShortStory} blocked={isEditMode}/>

            {isVolume && (
                <Item name="볼륨 번호(volume)" type="volume" count={volume} increaseFunc={increaseVolume} decreaseFunc={decreaseVolume} disabled={(!isVolume || isShortStory)} blocked={isEditMode}/>
            )}
            <Item name="이슈 번호(issue)" type="issue" count={issue} increaseFunc={increaseIssue} decreaseFunc={decreaseIssue} disabled={isShortStory} blocked={isEditMode}/>
        </section>
    )
}



export default ComicsEpisodesEditPanel;