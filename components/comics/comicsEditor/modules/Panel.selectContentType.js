import React from "react";
import style from "./Panel.selectContentType.module.scss";
import OptionToggleTable from "../optionToggleTable";
import {COMIC_BOOK_CONTENT_TYPE} from "../../../../utils/index/comics/book";
import {useDispatch, useSelector} from "react-redux";
import {requestUpdateComicBookInfo, setComicsBookContentType} from "../../../../features/comicsBook/actions";


const ComicsContentTypeSelectPanel = () => {
    const dispatch = useDispatch();

    const comicsBookState = useSelector(state => state.comicBook);

    const {comicBookDataId, contentType, isApproved} = comicsBookState;



    const options = {
        image: {
            text: "이미지 파일로 올리기",
            callback: (async ()=>{
                dispatch(setComicsBookContentType(COMIC_BOOK_CONTENT_TYPE.IMAGE));

                await requestUpdateComicBookInfo({comicBookDataId: comicBookDataId, contentType: COMIC_BOOK_CONTENT_TYPE.IMAGE});
            }),
            isDisabled: !isApproved,
            disabledText: !isApproved? "게시 적합 여부가 심사되지 않았습니다.": null
        },
        url: {
            text: "외부 URL 링크(구글 드라이브, 개인 블로그 등)로 연결하기",
            callback: (async ()=>{
                dispatch(setComicsBookContentType(COMIC_BOOK_CONTENT_TYPE.LINK));

                await requestUpdateComicBookInfo({comicBookDataId: comicBookDataId, contentType: COMIC_BOOK_CONTENT_TYPE.LINK});
            })
        }
    }


    return (
        <div className={style["container"]}>
            <span>본문 내용</span>
            <OptionToggleTable options={options} defOption={contentType===COMIC_BOOK_CONTENT_TYPE.IMAGE? options.image: options.url}/>
        </div>
    )
}


export default ComicsContentTypeSelectPanel;