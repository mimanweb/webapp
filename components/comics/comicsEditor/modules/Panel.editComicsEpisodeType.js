import React from "react";
import style from "./Panel.editComicsEpisodeType.module.scss";
import OptionToggleTable from "../optionToggleTable";
import {useDispatch, useSelector} from "react-redux";
import {requestUpdateComicBookInfo, setIsComicsShortStory} from "../../../../features/comicsBook/actions";
import {COMIC_BOOK_EDIT_MODE} from "../../../../utils/index/comics/book";



const ComicsEpisodeTypeEditPanel = () => {
    const dispatch = useDispatch();

    const comicsBookState = useSelector(state => state.comicBook);

    const {comicBookDataId, isShortStory, publishedBooks, editMode} = comicsBookState;

    const isUserUploadedShortStory = publishedBooks.find(book => book.index.isShortStory);
    const isEditMode = editMode===COMIC_BOOK_EDIT_MODE.EDIT;


    const options = {
        long: {
            text: "장편",
            callback: (async ()=>{
                dispatch(setIsComicsShortStory(false));

                await requestUpdateComicBookInfo({comicBookDataId: comicBookDataId, isShortStory: false});
            })
        },
        short: {
            text: "단편",
            callback: (async ()=>{
                dispatch(setIsComicsShortStory(true));

                await requestUpdateComicBookInfo({comicBookDataId: comicBookDataId, isShortStory: true});
            })
        }
    }

    return (
        <section className={style["container"]}>
            <span>회차 종류</span>
            <OptionToggleTable options={options} defOption={isShortStory? options.short: options.long} blocked={isEditMode}/>
        </section>
    )
}



export default ComicsEpisodeTypeEditPanel;