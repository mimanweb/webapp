import React, {useState, useEffect} from "react";
import style from "./optionToggleTable.module.scss";



const OptionItem = ({idx, selectedIdx, text, callback, selectFunc, isDisabled, isBlocked, isDisabledOption, optionDisabledText=null}) => {

    const isSelected = idx===selectedIdx;


    const onClick = () => {
        if (isBlocked || isDisabled || isDisabledOption) return;

        selectFunc(idx);
        callback();
    }

    return (
        <div className={`${style["option_item"]} ${isSelected? style["selected"]: ""} ${isDisabledOption? style["disabled"]: ""}`} onClick={onClick} title={isDisabledOption? optionDisabledText: null}>
            {isDisabledOption? (
                <del>{text}</del>
            ): (
                <span>{text}</span>
            )}
        </div>
    )

}



const OptionToggleTable = ({options, defOption, disabled, blocked}) => {
    const [selectedOptionIdx, setSelectedOptionIdx] = useState(null);


    const selectFunc = (idx) => {
        setSelectedOptionIdx(idx);
    }


    useEffect(()=>{
        if (defOption) {
            setSelectedOptionIdx(Object.values(options).indexOf(defOption));
        }
    }, [])


    return (
        <div className={`${style["container"]} ${disabled? style["disabled"]: ""} ${blocked? style["blocked"]: ""}`}>
            {Object.values(options).map((opt, idx) => {
                return (
                    <OptionItem key={idx} idx={idx} selectedIdx={selectedOptionIdx} text={opt.text} isDisabledOption={opt.isDisabled} optionDisabledText={opt.disabledText} callback={opt.callback} selectFunc={selectFunc} isDisabled={disabled} isBlocked={blocked}/>
                )
            })}
        </div>
    )
}


export default OptionToggleTable;