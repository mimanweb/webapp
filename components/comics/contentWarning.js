import React from "react";
import style from "./contentWarning.module.scss";




const ComicsContentWarningPanel = () => {
    return (
        <div className={style["container"]}>

            <div className={style["nudity"]}>
                <p className={style["sign"]}>NUDITY</p>
                <p className={style["message"]}>노출 묘사가 있을 수 있음.</p>
            </div>

            <div className={style["sexuality"]}>
                <p className={style["sign"]}>SEXUALITY</p>
                <p className={style["message"]}>성행위 묘사가 있을 수 있음.</p>
            </div>

            <div className={style["extreme_violence"]}>
                <p className={style["sign"]}>EXTREME VIOLENCE</p>
                <p className={style["message"]}>극도로 폭력적인 묘사가 있을 수 있음.</p>
            </div>

            <div className={style["extreme_cruelty"]}>
                <p className={style["sign"]}>EXTREME CRUELTY</p>
                <p className={style["message"]}>극도로 잔인한 신체훼손 묘사가 있을 수 있음.</p>
            </div>

        </div>
    )
}



export default ComicsContentWarningPanel;