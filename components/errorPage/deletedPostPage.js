import React from "react";
import style from "./deletedPostPage.module.scss"
import Link from "next/link";

import SmallButton, {SmallButtonType} from "../button/small";


const DeletedPostPage = () => {

    return (
        <div className={style["container"]}>
            <section>
                <h1>POST<br/>DELETED</h1>

                <p>"사용자에 의해 삭제된 게시글입니다."</p>

                <section className={style["buttons"]}>
                    <Link href="/">
                        <a className={style["to_home_button"]}>
                            <SmallButton buttonType={SmallButtonType.TO_HOME}/>
                        </a>
                    </Link>
                    <SmallButton buttonType={SmallButtonType.GO_BACK} onClick={()=>window.history.back()}/>
                </section>
            </section>
        </div>
    )
}



export default DeletedPostPage;