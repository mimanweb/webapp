import React from "react";
import style from "./dots.module.scss";


export default function Dots() {
    return (
        <div className={style["wrapper"]}>
            <div>
                <div className={style["first-dot"]}/>
                <div className={style["second-dot"]}/>
                <div className={style["third-dot"]}/>
            </div>
        </div>
    )
}