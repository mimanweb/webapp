import React from "react";
import style from "./navbar.module.scss"
import Link from "next/link";
import {useDispatch, useSelector, useStore} from 'react-redux';

export default function NavBar() {
    return (
        <nav className={style["container"]}>
            <section className={style["logo"]}>
                <div className={style["dots"]}>
                    {/*<div className={style["color-dot"]}/>*/}
                </div>
                <div className={style["icon"]}>
                    <Link href={"/"}>
                        <a><span className={style["color-dot"]}>.</span>MIMANWEB</a>
                    </Link>
                </div>
                <div className={style["dots"]}>
                    {/*<div className={style["color-dot"]}/>*/}
                </div>
            </section>
        </nav>
    )
};