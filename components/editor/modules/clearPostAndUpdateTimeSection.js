import style from "./clearPostAndUpdateTimeSection.module.scss";
import React from "react";
import {useSelector, useDispatch} from "react-redux";
import {resetPostTitleAndContentState, requestUpdateTempPost} from "../../../features/post/actions";
import POST_ from "../../../utils/index/post";
import time from "../../../utils/time";



const ClearPostAndUpdateTimeSection = ({clearEditorContentFunc}) => {
    const dispatch = useDispatch();

    const postState = useSelector(state => state.post);

    const {editMode} = postState;
    const isEditMode = editMode===POST_.EDIT_MODE.EDIT;


    const clearPost = async () => {
        await requestUpdateTempPost(postState.postId, postState.category, postState.board, "", "", postState.phrase);

        dispatch(resetPostTitleAndContentState());

        clearEditorContentFunc();
    }


    const getLatestUpdatedTimeMessageText = () => {
        const updatedTime = new Date(postState.updatedTime || postState.createdTime);

        const diff = postState.updateTimeDiffInMinutes || time.now().diff(updatedTime, "minutes");
        const hour = Math.floor(diff/60);
        const minute = diff%60;

        let diffText;

        if (hour <= 8) {
            if (hour<=0 && minute===0) diffText = "방금 전";
            else if (hour<=0 && minute!==0) diffText = minute + "분 전";
            else if (hour>0 && minute===0) diffText = hour + "시간 전";
            else if (hour>0 && minute!==0) diffText = hour + "시간 " + minute + "분 전";
        } else {
            const today = new Date();
            const todayDate = today.getDate();
            const date = updatedTime.getDate();

            let hour = updatedTime.getHours();
            hour = hour>12? hour-12: hour;
            const isMorning = updatedTime.getHours()<=12;
            const ampm = isMorning? "오전": "오후";
            const minute = updatedTime.getMinutes();

            if (todayDate===date) {
                diffText = `오늘`;
            }
            else if (todayDate-date===1) {
                diffText = `어제`;
            }
            else if (todayDate-date>1) {
                diffText = `${todayDate-date}일 전`;
            }

            diffText += ` ${ampm} ${hour}시`;
            if (minute>0) diffText += `${minute}분`
        }



        return "마지막 작성 시각: " + diffText
    }


    const getLatestUpdateTimeMessageTitle = () => {
        const updateTime = postState.updatedTime || postState.createdTime;
        const time = new Date(updateTime);

        const week = ["일", "월", "화", "수", "목", "금", "토"];

        const year = time.getFullYear();
        const month = time.getMonth()+1;
        const date = time.getDate();
        const day = week[time.getDay()];
        const hour = time.getHours();
        const minute = time.getMinutes();
        
        return year + "년 " + month + "월 " + date + "일(" + day + ") " + hour + "시 " + minute + "분"
    }


    return (
        <section className={style["container"]}>
            {!isEditMode && (
                <button className={style["clear"]} title="글 제목과 내용을 지우고 새 글로 작성" onClick={clearPost}>
                    <div className={style["icon"]}/>
                    <label>작성글 초기화</label>
                </button>
            )}

            {(!isEditMode && postState.updatedTime)
            && (
                <p className={style["temp-post-update-time"]} title={getLatestUpdateTimeMessageTitle()}>{getLatestUpdatedTimeMessageText()}</p>
            )}

            {isEditMode && (
                <p className={style["post_last_edit_time"]}>마지막 수정 시각: {getLatestUpdateTimeMessageTitle()}</p>
            )}
        </section>
    )
}


export default ClearPostAndUpdateTimeSection;