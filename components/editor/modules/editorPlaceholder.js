import style from "./editorPlaceholder.module.scss";
import React, {useState, useImperativeHandle, forwardRef} from "react";


const EditorPlaceholder = forwardRef(({}, ref)=>{
    const [visibility, setVisibility] = useState(false);



    useImperativeHandle(ref, () => ({
        show: () => {
            setVisibility(true);
        },
        hide: () => {
            setVisibility(false);
        }
    }))

    return (
        <div className={`${style["container"]} ${visibility? style["show"]: style["hide"]}`}>
            <p>글 내용</p>
        </div>
    )
})


export default EditorPlaceholder;