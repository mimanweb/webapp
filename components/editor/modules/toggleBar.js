import React, {useState, useEffect, useImperativeHandle, forwardRef, useRef} from "react";
import {useSelector, useDispatch} from "react-redux";
import _ from "underscore";
import style from "./toggleBar.module.scss";
import {useRouter} from "next/router";
import {
    findLineContainer,
    findCursorOffsetFromLine,
    findRangeOffsetFromLine,
    checkLineContainsUniqueEl,
    setNewRange,
    mergeDuplicatedStyles,
    organizeNestedStyles,
    findWrapperNode,
    findTextWrapperNode,
    selectEditorFirstLine,
    checkEditorHasFocus
} from "../../../utils/editor/commonUtils";
import {StretchOpenMessageBoard as ErrorMessageBoard} from "../../panel";
import {StretchOpenMessageBoard as YoutubeLinkInputBoard} from "../../panel";
import {StretchOpenBoardType, StretchOpenMessageType} from "../../panel";

import {requestUploadPostImage} from "../../../features/post/actions";
import {dataURLtoFile, optimizeImage, readImgDimensions} from "../../../utils/image/index";


import STYLE_ from "../../../utils/editor/styleOptions";


let WRITING_BOARD_NODE;



const applyStyleToSelectionRange = (targetAttr, newValue, basicValue) => {
    const otherAttrs = STYLE_.ATTRIBUTES.filter(attr => attr !== targetAttr);

    // Below handles applying style attr changes to the current range selection
    const selection = document.getSelection();
    const range = selection.getRangeAt(0);



    /** When selection is CARET */
    if (selection.type === "Caret") {
        // Save current cursor(line-over) offset
        let originCursorOffset = findCursorOffsetFromLine(selection);

        const P = findLineContainer(range.commonAncestorContainer)
        const style = P.style;

        // Set style attribute of special types of line container(P tag)
        // When line contains IMG, IFRAME, BR, or the new font-size is a basic one which does not need to be shown
        style[targetAttr] = newValue === basicValue ? null : newValue;

        if (["IMG"].includes(P.firstChild.nodeName)) {
            originCursorOffset = 0;
            STYLE_.ATTRIBUTES.forEach(attr => style[attr] = null);
        }
        if (["IFRAME"].includes(P.firstChild.nodeName)) {
            originCursorOffset = 0;
            P.removeAttribute("style");
        }
        if (["A"].includes(P.firstChild.nodeName)) {
            originCursorOffset = 0;
            P.removeAttribute("style");
        }
        if (["BR"].includes(P.firstChild.nodeName)) {
            originCursorOffset = 0;
        }
        if (!STYLE_.ATTRIBUTES.some(attr => style[attr])) {
            P.removeAttribute("style");
        }


        // Remove target style attr from inner SPAN in line container(P)
        const list = [];
        P.childNodes.forEach(node => {
            switch (node.nodeName) {
                case "#text":
                    list.push(document.createTextNode(node.textContent));
                    break;
                case "SPAN":
                    const style = node.style;
                    style[targetAttr] = null;

                    if (otherAttrs.some(attr => style[attr])) list.push(node);
                    else list.push(document.createTextNode(node.textContent));
                    break;
                case "BR":
                case "IMG":
                case "IFRAME":
                case "A":
                    list.push(node);
                    break;
            }
        })


        // Delete prev range
        range.selectNodeContents(P);
        range.deleteContents();


        // Insert new content(nodes) in place
        let cursorNode;
        let cursorNodeOffset = 0;
        mergeDuplicatedStyles(list).forEach(node => {
            // Find cursor node
            if (!cursorNode && (cursorNodeOffset + node.textContent.length) >= originCursorOffset) {
                cursorNode = node.nodeName === "SPAN" ? node.firstChild : node;
                cursorNodeOffset = originCursorOffset - cursorNodeOffset;
            }
            if (!cursorNode) cursorNodeOffset += node.textContent.length;

            range.insertNode(node);
            range.setStartAfter(node);
        })


        // Set cursor back
        selection.setPosition(cursorNode, cursorNodeOffset);


    } else if (selection.type === "Range") {
        /** When selection is RANGE */

        /**
         * Move cursor out of A tag
         * */
        if (findLineContainer(range.startContainer) !== findLineContainer(range.endContainer)) {
            const checkLinkTagInNode = (node) => {
                return (node.nodeName==="#text" && node.parentNode.nodeName==="A") ||
                    (node.nodeName==="A") ||
                    (node.nodeName==="P" && [...node.children].some(_node => _node.nodeName==="A"));
            }


            if (checkLinkTagInNode(range.startContainer)) {
                const currLine = findLineContainer(range.startContainer);

                range.setStartBefore(currLine.nextSibling.firstChild);
            }
            else if (checkLinkTagInNode(range.endContainer)) {
                const currLine = findLineContainer(range.endContainer);

                range.setEndAfter(currLine.previousSibling.lastChild);
            }
        }



        const originRangeStartOffset = findRangeOffsetFromLine(range.startContainer, range.startOffset);
        const originRangeEndOffset = findRangeOffsetFromLine(range.endContainer, range.endOffset);

        const content = range.cloneContents();
        const contentNodes = _.clone(content.childNodes);




        /** RANGE within a line */
        if (contentNodes[0].nodeName !== "P") {
            const cursorNode = range.startContainer;
            const cursorNodeOffset = range.startOffset;
            const currLine = findLineContainer(cursorNode);


            /**
             * Range within an A tag,
             * Don't touch it and just pass along
             * */
            if (cursorNode.parentNode.nodeName === "A") {
                setNewRange(selection, range.startContainer, range.startOffset, range.endContainer, range.endOffset);
                return
            } else {
                range.deleteContents();
            }



            // Cursor within a SPAN
            // Escape cursor from the inside of SPAN to prevent nesting
            if (cursorNode.parentNode.nodeName === "SPAN") {
                if (cursorNodeOffset === 0) {
                    // Cursor at the start of SPAN
                    range.setStartBefore(cursorNode.parentNode);

                } else if (0 < cursorNodeOffset < cursorNode.textContent.length) {
                    // Cursor at the end of SPAN
                    const span1 = cursorNode.parentNode.cloneNode(true);
                    span1.textContent = span1.textContent.substr(0, cursorNodeOffset);

                    const span2 = cursorNode.parentNode.cloneNode(true);
                    span2.textContent = span2.textContent.substr(cursorNodeOffset);

                    range.setStartAfter(cursorNode.parentNode);
                    range.insertNode(span2);
                    range.insertNode(span1);
                    cursorNode.parentNode.parentNode.removeChild(cursorNode.parentNode);
                    range.setStartAfter(span1);

                } else {
                    // Cursor at the end of SPAN
                    range.setStartAfter(cursorNode.parentNode);
                }
            }


            // Apply new font-size to the extracted content nodes
            contentNodes.forEach(node => {
                const style = node.style;

                switch (node.nodeName) {
                    case "SPAN":
                        style[targetAttr] = newValue;
                        break;
                    case "#text":
                        const span = document.createElement("span");
                        span.style[targetAttr] = newValue;

                        if (cursorNode.parentNode.nodeName === "SPAN") {
                            otherAttrs.forEach(attr => { span.style[attr] = cursorNode.parentNode.style[attr] });
                        }

                        span.appendChild(node);
                        node = span;
                        break;
                }

                range.insertNode(node);
                range.setStartAfter(node);
            })


            // Replace line(P) with new contents
            const P = findLineContainer(range.commonAncestorContainer);
            const nodeList = ["IMG", "IFRAME", "A"].includes(P.firstChild.nodeName) ? [P.firstChild] : mergeDuplicatedStyles(P.childNodes);


            range.selectNodeContents(P);
            range.deleteContents();


            let cursorStartNode;
            let cursorStartNodeOffset = 0;
            let cursorEndNode;
            let cursorEndNodeOffset = 0;

            nodeList.forEach(node => {
                // Find cursor range
                if (!cursorStartNode && (cursorStartNodeOffset + node.textContent.length) >= originRangeStartOffset) {
                    cursorStartNode = node.nodeName === "SPAN" ? node.firstChild : ["IMG", "IFRAME", "A"].includes(node.nodeName) ? P : node;
                    cursorStartNodeOffset = originRangeStartOffset - cursorStartNodeOffset;
                }
                if (!cursorStartNode) cursorStartNodeOffset += node.textContent.length;

                if (!cursorEndNode && (cursorEndNodeOffset + (["IMG", "IFRAME", "A"].includes(node.nodeName) ? 1 : node.textContent.length)) >= originRangeEndOffset) {
                    cursorEndNode = node.nodeName === "SPAN" ? node.firstChild : ["IMG", "IFRAME", "A"].includes(node.nodeName) ? P : node;
                    cursorEndNodeOffset = originRangeEndOffset - cursorEndNodeOffset;
                }
                if (!cursorEndNode) cursorEndNodeOffset += node.textContent.length;

                range.insertNode(node);
                range.setStartAfter(node);
            });


            // Organize nested styles if whole line is wrapped by one SPAN
            if (!checkLineContainsUniqueEl(currLine)) {
                const trimList = nodeList.filter(node => node.textContent);

                if (trimList.length===1 && trimList[0].nodeName==="SPAN") {
                    const nestingSpan = trimList[0];

                    STYLE_.ATTRIBUTES.forEach(attr => {
                        const spanStyle = nestingSpan.style[attr];
                        const defStyle = STYLE_.DEFAULT_OPTIONS[attr].value;

                        if (spanStyle) currLine.style[attr] = spanStyle===defStyle? null: spanStyle;
                    });

                    // Remove style attr from the line if its none
                    if (STYLE_.ATTRIBUTES.every(attr => !currLine.style[attr])) currLine.removeAttribute("style");

                    // Extract text content from SPAN
                    range.setStartBefore(currLine.firstChild);
                    range.setEndAfter(currLine.lastChild);

                    const clone = nestingSpan.cloneNode(true);

                    range.deleteContents();
                    clone.childNodes.forEach(node => {
                        range.insertNode(node);
                    });

                    range.setStartBefore(currLine.firstChild);
                    range.setEndAfter(currLine.lastChild);
                    return;
                }
            }


            // Set range back
            const newRange = document.createRange();
            newRange.setStart(cursorStartNode, cursorStartNodeOffset);
            newRange.setEnd(cursorEndNode, cursorEndNodeOffset);

            selection.removeAllRanges();
            selection.addRange(newRange);


        } else {
            /** RANGE contains multiple lines */

            // Parse content list of range by lines
            // Apply style to extracted content elements
            const lineContentsList = [...contentNodes.values()].map(p => {
                return [...p.childNodes.values()].map(child => {
                    const style = child.style;
                    switch (child.nodeName) {
                        case "SPAN":
                            style[targetAttr] = newValue;
                            break;
                        case "BR":
                        case "#text":
                            const span = document.createElement("span");
                            span.style[targetAttr] = newValue;
                            span.appendChild(child);
                            child = span;
                            break;
                    }
                    return child;
                })
            });


            // Replacing range
            const rangeClone = range.cloneRange();
            const isRangeStartAtBeforeLine = rangeClone.startContainer.nodeName === "P" && rangeClone.startOffset === 0;
            const isRangeEndAtBeforeLine = rangeClone.endContainer.nodeName === "P" && rangeClone.endOffset === 0;

            range.deleteContents();

            const startContainer = findLineContainer(rangeClone.startContainer);
            const endContainer = findLineContainer(rangeClone.endContainer);

            lineContentsList.forEach((nodeList, index) => {
                if (index === 0) {
                    nodeList.forEach(node => {
                        startContainer.appendChild(node);
                    })
                } else if (0 < index && index < lineContentsList.length-1) {
                    const p = contentNodes[index].cloneNode();

                    nodeList.forEach(node => {
                        p.appendChild(node);
                    })

                    range.setStartBefore(endContainer)
                    range.insertNode(p);
                } else {
                    if (endContainer.hasChildNodes()) {
                        nodeList.reverse();
                        nodeList.forEach(node => {
                            endContainer.insertBefore(node, endContainer.firstChild);
                        })
                    } else {
                        nodeList.forEach(node => {
                            endContainer.appendChild(node);
                        })
                    }
                }
            })


            // Merge and trim line inner contents
            range.setStartBefore(startContainer);
            range.setEndAfter(endContainer);


            // Find cursor positions
            let cursorStartNode;
            let cursorStartNodeOffset = 0;
            let cursorEndNode;
            let cursorEndNodeOffset = 0;
            const extractedContents = [...range.extractContents().childNodes];

            const list = extractedContents.map((p, idx) => {
                const _p = p.cloneNode();
                const nodeList = mergeDuplicatedStyles(p.childNodes);
                const organizedNodeList = organizeNestedStyles(_p, nodeList);

                organizedNodeList.forEach(node => {
                    const nodeLength = node.textContent.length;

                    // Set start cursor
                    const isStartCursorOverOffset = (cursorStartNodeOffset + (["IMG", "IFRAME", "A"].includes(node.nodeName) ? nodeLength + 1 : nodeLength)) >= originRangeStartOffset;
                    if (!cursorStartNode && isStartCursorOverOffset) {
                        cursorStartNode = node.nodeName === "SPAN"? node.firstChild: ["IMG", "IFRAME", "A"].includes(node.nodeName)? _p: node;
                        if (node.nodeName === "SPAN" && node.hasChildNodes() && node.firstChild.nodeName === "BR") cursorStartNode = node;
                        cursorStartNodeOffset = originRangeStartOffset - cursorStartNodeOffset;
                    }
                    if (!cursorStartNode) cursorStartNodeOffset += nodeLength;

                    // Set end cursor
                    const isEndCursorOverOffset = (cursorEndNodeOffset + (["IMG", "IFRAME", "A"].includes(node.nodeName) ? nodeLength + 1 : nodeLength)) >= originRangeEndOffset;
                    if (idx === extractedContents.length-1 && !cursorEndNode && isEndCursorOverOffset) {
                        cursorEndNode = node.nodeName === "SPAN"? node.firstChild: ["IMG", "IFRAME", "A"].includes(node.nodeName)? _p: node;
                        cursorEndNodeOffset = originRangeEndOffset - cursorEndNodeOffset;
                    }
                    if (idx === extractedContents.length-1 && !cursorEndNode) cursorEndNodeOffset += nodeLength;

                    _p.appendChild(node);
                });

                return _p;
            });

            if (isRangeStartAtBeforeLine) {
                cursorStartNode = list[0];
                cursorStartNodeOffset = 0;
            }
            if (isRangeEndAtBeforeLine) {
                cursorEndNode = list.slice(-1)[0];
                cursorEndNodeOffset = 0;
            }

            list.forEach(p => {
                range.insertNode(p);
                range.setStartAfter(p);
            })



            // Set range back
            setNewRange(selection, cursorStartNode, cursorStartNodeOffset, cursorEndNode, cursorEndNodeOffset);
        }
    }
}

const getNextStyle = (styleList, currStyle) => {
    const currIndex = styleList.indexOf(currStyle);
    const newIndex = currIndex+1 > styleList.length-1 ? 0 : currIndex+1;

    return styleList[newIndex];
}



const delay = (ms) => new Promise(resolve => setTimeout(resolve, ms));



const ShortcutCombinationButton = ({type, onClick}) => {
    const KEY_COMBINATIONS = {
        FONT_SIZE: <><kbd>Ctrl</kbd> + <kbd>D</kbd></>,
        FONT_WEIGHT: <><kbd>Ctrl</kbd> + <kbd>B</kbd></>,
        FONT_STYLE: <><kbd>Ctrl</kbd> + <kbd>I</kbd></>,
        COLOR: <><kbd>Ctrl</kbd> + <kbd>E</kbd></>,
    }

    return (
        <button className={style["keyboard-shortcut"]} onClick={onClick}>
            <p className={style["keys"]}>{KEY_COMBINATIONS[type]}</p>
        </button>
    )
}

const FontSizeToggleButton = ({_style, fontSize, currState, onClick}) => {
    if (fontSize.value===currState.value) _style += ` ${style["enabled"]}`;

    return (
        <button className={_style} onClick={onClick}>{fontSize.label}</button>
    )
}

const ColorToggleButton = ({_style, color, currState, onClick}) => {
    if (color.value===currState.value) _style += ` ${style["enabled"]}`;

    return (
        <button className={_style} onClick={onClick}>
            {/*<div className={style["color-block"]}/>*/}
            {color.label}
        </button>
    )
}



/**
 * MAIN
 * */
const ToggleBar = forwardRef(({updatePostContentFunc, writingBoardRef}, ref) => {
    const router = useRouter();

    const postState = useSelector(state => state.post);

    const [fontSize, setFontSize] = useState(STYLE_.DEFAULT_OPTIONS.fontSize);
    const [color, setColor] = useState(STYLE_.DEFAULT_OPTIONS.color);
    const [fontWeight, setFontWeight] = useState(STYLE_.DEFAULT_OPTIONS.fontWeight);
    const [fontStyle, setFontStyle] = useState(STYLE_.DEFAULT_OPTIONS.fontStyle);

    const imgInputRef = useRef(null);
    const errorMessageBoardRef = useRef(null);
    const youtubeLinkInputBoardRef = useRef(null);
    const urlLinkInputBoardRef = useRef(null);



    const applyCursorNodeAndLineStylesToState = (cursorNode) => {
        const currLine = findLineContainer(cursorNode);
        const span = findTextWrapperNode(cursorNode);

        const styleObj = {};

        STYLE_.ATTRIBUTES.forEach(attr => {
            const lineStyle = currLine.style[attr];
            const spanStyle = span && span.style[attr];
            const defStyle = STYLE_.DEFAULT_OPTIONS[attr].value;

            styleObj[attr] = spanStyle ? spanStyle : lineStyle ? lineStyle : defStyle;
        })

        // Apply to the state
        setColor(STYLE_.makeStyleObj("color", styleObj.color));
        setFontSize(STYLE_.makeStyleObj("fontSize", styleObj.fontSize));
        setFontWeight(STYLE_.makeStyleObj("fontWeight", styleObj.fontWeight));
        setFontStyle(STYLE_.makeStyleObj("fontStyle", styleObj.fontStyle));
    }


    const updateState = () => {
        const selection = document.getSelection();
        const range = selection.getRangeAt(0);

        if (selection.type==="Caret") {
            /**
             * Find styles from SPAN which the current cursor in, merge it with line container's style,
             * Ignore SPAN if none at cursor position
             * */
            applyCursorNodeAndLineStylesToState(range.startContainer);
        }
        else if (selection.type==="Range") {
            /**
             * Find style obj if both range start/end within the same text wrapper(SPAN),
             * Show empty options if not
             * */
            applyCursorNodeAndLineStylesToState(range.endContainer);
            // const isBothRangeInSameNode = range.startContainer===range.endContainer;
            //
            // if (isBothRangeInSameNode) {
            //     applyCursorNodeAndLineStylesToState(range.startContainer);
            // } else {
            //     resetState();
            // }
        }
    }



    /**
     * TOGGLE FONT SIZE
     * */
    const toggleFontSize = (styleObj) => {
        if (!checkEditorHasFocus(WRITING_BOARD_NODE)) selectEditorFirstLine(writingBoardRef);

        let newFontSize = styleObj || getNextStyle(STYLE_.FONT_SIZE, fontSize);

        setFontSize(newFontSize);
        applyStyleToSelectionRange("fontSize", newFontSize.value, STYLE_.DEFAULT_OPTIONS.fontSize.value);

        updatePostContentFunc();
    }


    /**
     * TOGGLE COLOR
     * */
    const toggleColor = (styleObj) => {
        if (!checkEditorHasFocus(WRITING_BOARD_NODE)) selectEditorFirstLine(writingBoardRef);

        let newColor = styleObj || getNextStyle(STYLE_.COLOR, color);

        setColor(newColor);
        applyStyleToSelectionRange("color", newColor.value, STYLE_.DEFAULT_OPTIONS.color.value);

        updatePostContentFunc();
    }


    /**
     * TOGGLE FONT WEIGHT
     * */
    const toggleFontWeight = (styleObj) => {
        if (!checkEditorHasFocus(WRITING_BOARD_NODE)) selectEditorFirstLine(writingBoardRef);

        const newFontWeight = styleObj || getNextStyle(STYLE_.FONT_WEIGHT, fontWeight);

        setFontWeight(newFontWeight);
        applyStyleToSelectionRange("fontWeight", newFontWeight.value, STYLE_.DEFAULT_OPTIONS.fontWeight.value);

        updatePostContentFunc();
    }


    /**
     * TOGGLE FONT STYLE
     * */
    const toggleFontStyle = (styleObj) => {
        if (!checkEditorHasFocus(WRITING_BOARD_NODE)) selectEditorFirstLine(writingBoardRef);

        const newFontStyle = styleObj || getNextStyle(STYLE_.FONT_STYLE, fontStyle);

        setFontStyle(newFontStyle);
        applyStyleToSelectionRange("fontStyle", newFontStyle.value, STYLE_.DEFAULT_OPTIONS.fontStyle.value);

        updatePostContentFunc();
    }


    /**
     * TOGGLE IMG UPLOAD
     * */
    const toggleImgInput = () => {
        youtubeLinkInputBoardRef.current.close();
        errorMessageBoardRef.current.close();
        urlLinkInputBoardRef.current.close();
        imgInputRef.current.click();
    }


    const onImgInput = async () => {
        const isEditorFocused = checkEditorHasFocus(WRITING_BOARD_NODE);
        let selection = document.getSelection();

        if (!isEditorFocused) selectEditorFirstLine(writingBoardRef);

        const range = selection.getRangeAt(0);
        const currLine = findLineContainer(range.endContainer);

        if (imgInputRef.current.files.length!==1) return;


        const file = imgInputRef.current.files[0]
        const mimeType = file.type.split("/")[1];


        if (![
            "image/jpg",
            "image/jpeg",
            "image/png",
            "image/gif",
            "image/svg",
            "image/svg+xml",
        ].includes(file.type)) {
            errorMessageBoardRef.current.open(StretchOpenMessageType.IMG_FILE_NOT_SUPPORTED);
            return;
        }

        const reader = new FileReader();

        let blob;
        let imgWidth;
        let imgHeight;

        /**
         * Gif input
         * */
        if (file.type==="image/gif") {
            // File size limit for gif
            if (file.size > 2e+7) {
                errorMessageBoardRef.current.open(StretchOpenMessageType.GIF_SIZE_EXCEEDED);
                return;
            }

            const imgDimensions = await readImgDimensions(file);

            blob = file;
            imgWidth = imgDimensions.width;
            imgHeight = imgDimensions.height;
        }
        /**
         * None-gif input
         * */
        else {
            try {
                // File size limit for jpeg/png
                if (file.size > 5e+7) {
                    errorMessageBoardRef.current.open(StretchOpenMessageType.IMG_SIZE_EXCEEDED);
                    return;
                }


                if (file.size > 1e+6) {
                    const blobImgData = await optimizeImage(file, 1280);

                    blob = blobImgData.blob;
                    imgWidth = blobImgData.width;
                    imgHeight = blobImgData.height;
                }
                else {
                    const imgDimensions = await readImgDimensions(file);

                    blob = file;
                    imgWidth = imgDimensions.width;
                    imgHeight = imgDimensions.height;
                }
            } catch (e) {
                errorMessageBoardRef.current.open(StretchOpenMessageType.IMG_FILE_NOT_SUPPORTED);
            }

        }

        // ON LOAD
        reader.onload = async (e) => {
            const blob = e.target.result;
            const _file = dataURLtoFile(blob, "file." + mimeType);


            const {status, data} = await requestUploadPostImage(_file, imgWidth, imgHeight, postState.postId, postState.category, postState.board);

            switch (status) {
                case 403:
                    /**
                     * Post relay on already has been published or deleted
                     * */
                    alert("작성 중인 게시글이 삭제되었거나 수정할 수 없는 상태입니다.");
                    await router.push(`/board/${postState.category}/${postState.board}`);
                    break;
                case 404:
                    /**
                     * Post data does not exist for some reason
                     * */
                    alert("작성 중인 게시글이 존재하지 않습니다.");
                    await router.push(`/board/${postState.category}/${postState.board}`);
                    break;
                case 500:
                    /**
                     * Image upload failed
                     * */
                    errorMessageBoardRef.current.open(StretchOpenMessageType.IMG_UPLOAD_FAIL);
                    break;
                case 200:
                    /**
                     * Successfully uploaded image
                     * */
                    const {imageId} = data;

                    const p = document.createElement("p");
                    const img = document.createElement("img");

                    img.src = `${process.env.IMG_SERVER_URL}/${imageId}`;
                    img.setAttribute("data-width", imgWidth);
                    img.setAttribute("data-height", imgHeight);

                    const lineWidth = currLine.offsetWidth;
                    const ratio = lineWidth/imgWidth;

                    // img.style.width = imgWidth + "px";
                    // img.style.height = Math.round((ratio>=1)? imgHeight: imgHeight*ratio) + "px";

                    p.appendChild(img);


                    if (isEditorFocused) {
                        range.setStartAfter(currLine);
                        range.setEndAfter(currLine);
                    } else {
                        range.setStartBefore(currLine);
                        range.setEndBefore(currLine);
                    }

                    range.insertNode(p);

                    // Set cursor back
                    setNewRange(selection, p, 0, p, 0);

                    updatePostContentFunc();
                    break;
            }
        }


        // ON ERROR
        reader.onerror = (e) => {
            errorMessageBoardRef.current.open(StretchOpenMessageType.IMG_LOAD_FAIL);
        }


        // EXEC
        reader.readAsDataURL(blob);

        // RESET INPUT EL
        imgInputRef.current.value = "";

        // Set cursor back
        setNewRange(selection, range.startContainer, range.startOffset, range.endContainer, range.endOffset);

        updatePostContentFunc();
    }


    /**
     * TOGGLE YOUTUBE LINK
     * */
    const toggleYoutubeLink = () => {
        if (!checkEditorHasFocus(WRITING_BOARD_NODE)) selectEditorFirstLine(writingBoardRef);
        urlLinkInputBoardRef.current.close();
        youtubeLinkInputBoardRef.current.openOrClose();
    }


    /**
     * TOGGLE URL LINK
     * */
    const toggleUrlLink = () => {
        if (!checkEditorHasFocus(WRITING_BOARD_NODE)) selectEditorFirstLine(writingBoardRef);
        youtubeLinkInputBoardRef.current.close();
        urlLinkInputBoardRef.current.openOrClose();
    }




    useImperativeHandle(ref, () => ({
        setWritingBoardNode: (node) => {
            WRITING_BOARD_NODE = node
        },
        updateState: () => {
            if (!checkEditorHasFocus(WRITING_BOARD_NODE)) return
            updateState();
        },
        resetState: () => {
        },
        toggleFontSize: () => { toggleFontSize(null); },
        toggleFontWeight: () => { toggleFontWeight(null); },
        toggleFontStyle: () => { toggleFontStyle(null); },
        toggleColor: () => { toggleColor(null); },
    }))


    return (
        <div className={style["container"]}>

            {/** TOP SECTION */}
            <section className={style["top"]}>

                {/** FONT SIZE */}
                <section className={style["font-size"]}>
                    <ShortcutCombinationButton type="FONT_SIZE" onClick={()=>toggleFontSize(null)}/>
                    <div className={style["values"]}>
                        <FontSizeToggleButton _style={style["normal"]} fontSize={STYLE_.FONT_SIZE[0]} currState={fontSize} onClick={()=>toggleFontSize(STYLE_.FONT_SIZE[0])}/>
                        <FontSizeToggleButton _style={style["big1"]} fontSize={STYLE_.FONT_SIZE[1]} currState={fontSize} onClick={()=>toggleFontSize(STYLE_.FONT_SIZE[1])}/>
                        <FontSizeToggleButton _style={style["big2"]} fontSize={STYLE_.FONT_SIZE[2]} currState={fontSize} onClick={()=>toggleFontSize(STYLE_.FONT_SIZE[2])}/>
                        <FontSizeToggleButton _style={style["big3"]} fontSize={STYLE_.FONT_SIZE[3]} currState={fontSize} onClick={()=>toggleFontSize(STYLE_.FONT_SIZE[3])}/>
                    </div>
                </section>

                {/** FONT COLOR */}
                <section className={style["color"]}>
                    <ShortcutCombinationButton type="COLOR" onClick={()=>toggleColor(null)}/>
                    <div className={style["values"]}>
                        <ColorToggleButton _style={style["black"]} color={STYLE_.COLOR[0]} currState={color} onClick={()=>toggleColor(STYLE_.COLOR[0])}/>
                        <ColorToggleButton _style={style["red"]} color={STYLE_.COLOR[1]} currState={color} onClick={()=>toggleColor(STYLE_.COLOR[1])}/>
                        <ColorToggleButton _style={style["blue"]} color={STYLE_.COLOR[2]} currState={color} onClick={()=>toggleColor(STYLE_.COLOR[2])}/>
                    </div>
                </section>

                {/** FONT WEIGHT */}
                <section className={style["font-weight"]}>
                    <ShortcutCombinationButton type="FONT_WEIGHT" onClick={()=>toggleFontWeight(null)}/>
                    <div className={style["values"]}>
                        <button className={fontWeight !== STYLE_.DEFAULT_OPTIONS.fontWeight? style["enabled"]: undefined} onClick={()=>toggleFontWeight(null)}>굵게</button>
                    </div>
                </section>

                {/** FONT STYLE */}
                <section className={style["font-style"]}>
                    <ShortcutCombinationButton type="FONT_STYLE" onClick={()=>toggleFontStyle(null)}/>
                    <div className={style["values"]}>
                        <button className={fontStyle !== STYLE_.DEFAULT_OPTIONS.fontStyle? style["enabled"]: undefined} onClick={()=>toggleFontStyle(null)}>기울인</button>
                    </div>
                </section>

                {/** IMAGE FILE */}
                <section className={style["image-file"]} onClick={toggleImgInput}>
                    <div className={style["icon"]}/>
                    <label>이미지 파일</label>
                    <input type="file" accept=".jpg,.jpeg,.png,.gif" multiple={false} onChange={onImgInput} ref={imgInputRef}/>
                </section>

                {/** YOUTUBE LINK */}
                <section className={style["youtube-link"]} onClick={toggleYoutubeLink}>
                    <label className={style["icon"]}>YouTube</label>
                    {/*<label>링크</label>*/}
                </section>

                {/** HYPERLINK */}
                <section className={style["hyperlink"]} onClick={toggleUrlLink}>
                    <div className={style["icon"]}/>
                    <label>링크</label>
                </section>
            </section>


            {/** BOTTOM SECTION */}
            <section className={style["bottom"]}>
                <YoutubeLinkInputBoard boardType={StretchOpenBoardType.POST_YOUTUBE_LINK_INPUT} editorRef={writingBoardRef} updatePostContentFunc={updatePostContentFunc} ref={youtubeLinkInputBoardRef}/>
                <YoutubeLinkInputBoard boardType={StretchOpenBoardType.POST_EDITOR_MENUBAR.LINK_INPUT} editorRef={writingBoardRef} updatePostContentFunc={updatePostContentFunc} ref={urlLinkInputBoardRef}/>
                <ErrorMessageBoard boardType={StretchOpenBoardType.POST_IMG_UPLOAD_RESULT} ref={errorMessageBoardRef}/>
            </section>

        </div>
    )
})


export default ToggleBar;