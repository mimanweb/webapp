import React, {useState, useEffect, useImperativeHandle, forwardRef, useRef} from "react";
import style from "./uniqueElToggleKeys.module.scss"
import {
    findLineContainer,
    findUniqueElFromLine,
    checkEditorHasFocus,
    findLinkElFromLine
} from "../../../utils/editor/commonUtils";



let IS_MOUSE_OVER_KEYPAD = false;



const UniqueElToggleKeys = forwardRef(({editorRef, uniqueElControllerRef}, ref) => {
    const thisRef = useRef(null);

    const [visibility, setVisibility] = useState(false);


    const setKeypadMouseOver = (value) => {
        IS_MOUSE_OVER_KEYPAD = value;
    }


    const toggleKey = () => {
        setVisibility(false);
        uniqueElControllerRef.current.show();
    }

    const show = () => {
        setVisibility(true);
    }

    const hide = () => {
        setVisibility(false);
    }


    useImperativeHandle(ref, ()=>({
        updateState: () => {
            const selection = document.getSelection();
            const range = selection.getRangeAt(0);
            const isUniqueElControllerUp = uniqueElControllerRef.current.checkVisibility()

            if (selection.type!=="Caret" || isUniqueElControllerUp) {
                setVisibility(false);
                return;
            }


            const currLine = findLineContainer(range.endContainer);
            const isUniqueContentLine = currLine && findUniqueElFromLine(currLine);
            const isLinkLine = currLine && findLinkElFromLine(currLine);

            if (isUniqueContentLine || isLinkLine) {
                thisRef.current.style.top = currLine.offsetTop + "px";
                show();
            } else {
                hide();
            }
        },
        hide: () => {
            hide();
        },
        checkMouseOverKeypad: () => {
            return IS_MOUSE_OVER_KEYPAD;
        },
    }))



    useEffect(() => {
        thisRef.current.style.left = -thisRef.current.offsetWidth + "px";
    }, [thisRef, visibility])



    return (
        <div className={`${style["container"]} ${visibility? style["visible"]: ""}`} ref={thisRef}>
            <div
                className={style["keys"]}
                onClick={toggleKey}
                onMouseOver={()=>setKeypadMouseOver(true)}
                onMouseLeave={()=>setKeypadMouseOver(false)}>
                <kbd>Ctrl</kbd> + <kbd className={style["space"]}>Space</kbd>
            </div>
        </div>
    )
})


export default UniqueElToggleKeys;