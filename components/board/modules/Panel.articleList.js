import React, {useEffect, useState} from "react";
import style from "./Panel.articleList.module.scss";
import {parseCommentCount} from "../../../utils/format";
import _ from "underscore";
import {parseImgId} from "../../../utils/image/parse";
import Link from "next/link";



const ArticleList = ({list, maxRow}) => {


    let rowCount = 0;



    return (
        <div className={style["container"]}>
            {_.isEmpty(list) && (
                <span className={style["no_result"]}>추첨된 인기글이 없습니다.</span>
            )}

            {list.map((article, key) => {
                let rowAttempt;
                let item;

                let {title, record, isImagePost, textContent, thumbnail, number, index} = article;

                const commentCount = record.commentCount || 0;
                const img = parseImgId(thumbnail);
                const text = textContent;
                const type = isImagePost? "image": "text";

                const postLink = `/post/${index.category}/${index.board}/${number}`


                switch (type) {
                    case "text":
                        rowAttempt = rowCount+1;

                        item = (
                            <Link href={postLink} key={key}>
                                <a className={style["text_article"]}>
                                    <span title={title}>{title}</span>
                                    <span>{parseCommentCount(commentCount, false, true)}</span>
                                </a>
                            </Link>
                        )
                        break;
                    case "image":
                        rowAttempt = rowCount+3;

                        const no_text = !text;

                        item = (
                            <Link href={postLink} key={key}>
                                <a className={style["image_article"]}>
                                    <div className={style["img"]}>
                                        <img src={img}/>
                                        <p className={style["comment_count"]}>{parseCommentCount(commentCount)}</p>
                                    </div>
                                    <div className={style["text_content"]}>
                                        <span className={`${style["title"]} ${no_text? style["full"]: ""}`}>{title}</span>
                                        {text && (
                                            <span className={style["text"]}>{text}</span>
                                        )}
                                    </div>
                                </a>
                            </Link>
                        )
                        break;
                }


                if (rowAttempt<=maxRow) {
                    rowCount = rowAttempt;

                    return item;
                }
            })}
        </div>
    )
}



export default ArticleList;