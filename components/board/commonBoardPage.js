import React from "react";
import Link from "next/link";
import ErrorServerDown from "../../pages/500.ServerDown";
import style from "./commonBoardPage.module.scss"
import {useSelector} from "react-redux";


import PostBoard from "../../components/board/board";
import {AD_728X90} from "../../components/ad/index";
import LeftSidebar from "../../components/sideBar/left-sidebar";
import RightSidebar from "../../components/sideBar/right-sidebar";
import ArticleList from "../../components/board/modules/Panel.articleList";
import DocHead from "../meta/head";





export default function CommonBoardPage({isServerDown, status, category, board, boardFullname}) {
    if (isServerDown) return <ErrorServerDown/>;

    const boardState = useSelector(state => state.board);

    const {hotPostList_24h, hotPostList_7d} = boardState;


    return (
        <>
            <DocHead title={`미만웹 | ${boardFullname}`}/>

            <LeftSidebar/>

            <main className={style["container"]}>

                {/** TOP SECTION */}
                <section className={style["top"]}>

                    {/** INNER LEFT */}
                    <section className={style["left"]}>
                        <div className={style["title"]}>
                            <Link href={`/board/${category}/${board}`}>
                                <a>
                                    <h1>{boardFullname}</h1>
                                </a>
                            </Link>
                        </div>

                        {/** SECTION CONTAINER */}
                        <section className={style["top_left_bottom_block"]}>

                            <section className={style["hot_post_list_title"]}>
                                <p>오늘의 인기글</p>
                            </section>

                            <section className={style["hot_post_list_content"]}>
                                <section className={style["hot_post_list"]}>
                                    <ArticleList list={hotPostList_24h.slice(0, Math.floor(hotPostList_24h.length/3))} maxRow={5}/>
                                </section>

                                <section className={style["hot_post_list"]}>
                                    <ArticleList list={hotPostList_24h.slice(Math.floor(hotPostList_24h.length*1/3), Math.floor(hotPostList_24h.length*2/3))} maxRow={5}/>
                                </section>

                                <section className={style["hot_post_list"]}>
                                    <ArticleList list={hotPostList_24h.slice(Math.floor(hotPostList_24h.length*2/3))} maxRow={5}/>
                                </section>
                            </section>
                        </section>
                    </section>

                    {/** INNER RIGHT */}
                    <section className={style["right"]}>
                        <section className={style["hot_post_list_title"]}>
                            <p>주간 인기글</p>
                        </section>

                        <section className={style["hot_post_list"]}>
                            <ArticleList list={hotPostList_7d} maxRow={9}/>
                        </section>
                    </section>
                </section>


                {/** MIDDLE SECTION */}
                <section className={style["middle"]}>

                    {/** AD */}
                    <section className={style["first"]}>
                        <AD_728X90/>
                    </section>

                    {/** BOARD, POST LIST */}
                    <section>
                        <PostBoard category={category} board={board}/>
                    </section>

                </section>


                {/** BOTTOM SECTION */}
                <section className={style["bottom"]}>
                    <AD_728X90/>
                </section>

            </main>

            <RightSidebar/>
        </>
    );
}

