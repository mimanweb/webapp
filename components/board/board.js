import React from "react";
import style from "./board.module.scss";
import Link from "next/link";
import _ from "underscore";
import {useSelector, useDispatch} from "react-redux";
import timeUtil from "../../utils/time";
import HorizontalTextPageNavigator from "../pageNavigator/pageNavigator";
import {POST_PAGE_PER_PAGE_GROUP_COUNT, BOARD_LIST_TYPE} from "../../utils/index/board";
import {useRouter} from "next/router";
import {SiteIndex} from "../../utils/index/siteIndex";
import {USER_TYPE} from "../../utils/index/user";
import SmallButton, {SmallButtonType} from "../../components/button/small";
import POST_ from "../../utils/index/post";
import {parseFullUserInfo} from "../../utils/format";





const Tab = ({boardListType}) => {
    const boardState = useSelector(state => state.board);
    const {category, board, listType} = boardState;

    let text;
    let link = {
        pathname: `/board/${category}/${board}`
    };
    let isCurrPostType = boardListType===listType;

    switch (boardListType) {
        case BOARD_LIST_TYPE.ALL:
            text = "전체글";
            break;
        case BOARD_LIST_TYPE.HOT:
            text = "인기글";
            link.query = {"list": BOARD_LIST_TYPE.HOT};
            break;
        case BOARD_LIST_TYPE.NOTICE:
            text = "공지";
            link.query = {"list": BOARD_LIST_TYPE.NOTICE};
            break;
    }

    return (
        <Link href={link}>
            <a className={isCurrPostType? style["fill"]: null}>{text}</a>
        </Link>
    )
}



const PostTypeIcon = ({type}) => {
    switch (type) {
        case POST_.TYPE.NOTICE:
            return (
                <div className={`${style["post_type_icon"]} ${style["notice"]}`}>
                    <span>공지</span>
                </div>
            )
    }

}



/**
 * Post article
 * */
const PostArticle = ({postParams, inNoticeList}) => {

    const postState = useSelector(state => state.post);
    const boardState = useSelector(state => state.board);


    const {number: postPagePostNumber} = postState;
    const {number: postNumber, owner, index: postIndex, title, time, record, images, isMyPost, type: postType} = postParams;
    const {upvoteCount, commentCount, viewCount} = record;
    const {userId, nickname, ipAddress} = owner;

    const isRegUserPost = userId;
    const isPostPagePost = postNumber===postPagePostNumber;
    const isNoticePost = postType===POST_.TYPE.NOTICE;



    const upvote_icon_style = () => {
        const styles = [style["upvote_count_icon"]]

        if (upvoteCount===1) {
            styles.push(style["lv1"]);
        }
        else if (upvoteCount>=2 && upvoteCount<=3) {
            styles.push(style["lv2"]);
        }
        else if (upvoteCount>=4 && upvoteCount<=5) {
            styles.push(style["lv3"]);
        }
        else if (upvoteCount>=6 && upvoteCount<=7) {
            styles.push(style["lv4"]);
        }
        else if (upvoteCount>=8 && upvoteCount<=10) {
            styles.push(style["lv5"]);
        }
        else if (upvoteCount>=11) {
            styles.push(style["lv6"]);
        }


        return styles.join(" ");
    }


    const postLink = {
        pathname: `/post/${postIndex.category}/${postIndex.board}/${postNumber}`,
    }

    if (BOARD_LIST_TYPE.ALL!==boardState.listType) postLink.query = {list: boardState.listType};


    return (
        <article className={`${style["post"]} ${isPostPagePost? style["highlight"]: ""}`}>
            {isPostPagePost && (
                <div className={style["current_post"]}/>
            )}

            {(
                !inNoticeList
            ) && (
                <Link href={postLink}>
                    <a className={upvote_icon_style()}>
                        <span>{upvoteCount<100? upvoteCount: "+99"}</span>
                    </a>
                </Link>
            )}

            {inNoticeList && (
                <div className={style["notice_type_icon"]}>
                    <label>공지</label>
                </div>
            )}

            <div className={style["title"]}>

                {postType && (
                    <PostTypeIcon type={postType}/>
                )}

                <Link href={postLink}>
                    <a className={`${style["title"]} ${inNoticeList? style["fresh"]: ""}`}>
                        <span>{title}</span>
                    </a>
                </Link>

                {
                    !_.isEmpty(images)
                    && (<div className={style["image_icon"]}/>)
                }

                {(
                    !inNoticeList &&
                    commentCount>0
                )
                    && (<label className={style["comment_count"]}>{commentCount}</label>)
                }
            </div>

            <div className={style["post_metadata"]}>
                <div className={style["nickname"]}>
                    <label title={parseFullUserInfo(userId, nickname, ipAddress)}>{nickname}</label>
                </div>
                <div className={style["views"]}>
                    <label title={`조회수: ${viewCount}`}>{viewCount}</label>
                </div>
                <div className={style["time"]}>
                    <label title={timeUtil.fullFormatText(time.published)}>{timeUtil.shortFormatText(time.published)}</label>
                </div>
            </div>
        </article>
    )
}



/**
 * Board
 * */
const Board = ({category, board}) => {
    const router = useRouter();

    const boardState = useSelector(state => state.board);
    const userState = useSelector(state => state.user);

    const {type: userType} = userState;
    const {postList, noticePostList, currPage, totalPageCount, postPerPage, listType} = boardState;

    const currPageGroup = Math.ceil(currPage/POST_PAGE_PER_PAGE_GROUP_COUNT);
    const pageStartOfCurrPageGroup = (currPageGroup-1)*POST_PAGE_PER_PAGE_GROUP_COUNT;
    const pageEndOfCurrPageGroup = (currPageGroup)*POST_PAGE_PER_PAGE_GROUP_COUNT;


    const isAdminUser = userType===USER_TYPE.ADMIN;
    const atCommunityNoticeBoard = category===SiteIndex.Community.code && board===SiteIndex.Community.boards.Notice.code;


    const currBoardLinkPath = `/board/${category}/${board}`
    let boardListQuery = {};


    switch (listType) {
        case BOARD_LIST_TYPE.HOT:
            boardListQuery = {list: BOARD_LIST_TYPE.HOT};
            break;
        case BOARD_LIST_TYPE.NOTICE:
            boardListQuery = {list: BOARD_LIST_TYPE.NOTICE};
            break;
    }


    const isBoardEmpty = _.isEmpty(postList);



    const onPostWriteButtonClick = async () => {
        await router.push(`/post/write/${category}/${board}`);
    }



    return (
        <div className={style["container"]}>


            {/** TAB SECTION */}
            {!atCommunityNoticeBoard && (
                <section className={style["tab"]}>
                    <Tab boardListType={BOARD_LIST_TYPE.ALL}/>
                    <Tab boardListType={BOARD_LIST_TYPE.HOT}/>
                    <Tab boardListType={BOARD_LIST_TYPE.NOTICE}/>
                </section>
            )}


            {/** LIST SECTION */}
            <section className={style["list"]}>

                {!_.isEmpty(noticePostList) && (
                    <>
                        {/** NOTICE LIST */}
                        <section className={style["notice_list"]}>
                            {noticePostList.map((post, key) => {
                                return (
                                    <PostArticle key={key} postParams={post} inNoticeList={true}/>
                                )
                            })}
                        </section>

                        {/** GAP */}
                        <div className={style["gap"]}/>
                    </>
                )}




                {/** POST LIST */}
                <section className={style["post_list"]}>
                    {isBoardEmpty? (
                        <div className={style["empty_board_message"]}>
                            <span>{
                                listType===BOARD_LIST_TYPE.HOT? "아직 인기글이 없습니다.":
                                    listType===BOARD_LIST_TYPE.NOTICE? "공지글이 없습니다.":
                                        "게시글이 없습니다, 첫 게시글을 작성해 주십시오."
                            }</span>
                        </div>
                    ): (
                        postList.map((post, key) => {
                            return (
                                <PostArticle key={key} postParams={post}/>
                            )
                        })
                    )}
                </section>

            </section>


            {/** PAGE NAVIGATION */}
            <section className={style["page_nav_post_write_button"]}>
                <HorizontalTextPageNavigator
                    isLinkMode={true}
                    currPage={currPage}
                    totalPageCount={totalPageCount}
                    pagePerPageGroupCount={POST_PAGE_PER_PAGE_GROUP_COUNT}
                    toFirstPageLinkPath={{
                        pathname: currBoardLinkPath,
                        query: {...{page: 1}}
                    }}
                    toPrevPageLinkPath={{
                        pathname: currBoardLinkPath,
                        query: {...{page: pageStartOfCurrPageGroup}}
                    }}
                    toPageLinkPath={currBoardLinkPath}
                    toNextPageLinkPath={{
                        pathname: currBoardLinkPath,
                        query: {...{page: pageEndOfCurrPageGroup+1}}
                    }}
                    additionalLinkQuery={boardListQuery}/>

                {(
                    !atCommunityNoticeBoard ||
                    (atCommunityNoticeBoard && isAdminUser)
                )
                && (
                    <SmallButton buttonType={SmallButtonType.WRITE_POST} onClick={onPostWriteButtonClick}/>
                )}
            </section>


        </div>
    )
};


export default Board;