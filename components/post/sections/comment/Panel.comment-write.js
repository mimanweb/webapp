import React, {useRef, useState, useImperativeHandle, forwardRef, useEffect} from "react";
import style from "./styles/Panel.comment-write.module.scss";
import {useSelector, useDispatch} from "react-redux";
import optimizeImage from "../../../../utils/image/imageOptimizer";
import Keyboard from "../../../../utils/editor/keyboard";
import LocalStorage_ from "../../../../utils/index/localStorage";
import {requestUploadComment, requestUploadReplyComment, setCommentState, setCommentNumberToWrite} from "../../../../features/comment/actions";


import Panel_YoutubeLinkInput from "./Panel.youtube-link-input";
import {dataURLtoFile} from "../../../../utils/image";



const CommentWrite = forwardRef(({inCommentSection, inReplyCommentSection}, ref)=>{
    const dispatch = useDispatch();

    const userState = useSelector(state => state.user);
    const postState = useSelector(state => state.post);
    const commentState = useSelector(state => state.comment);


    const isRegUser = userState.loggedIn;
    const {id: userId} = userState;
    const {number: postNumber, category, board} = postState;
    const {commentNumberToWrite: repliedCommentNumber} = commentState;


    const [nickname, setNickname] = useState("");
    const [password, setPassword] = useState("");

    const [isValidMedia, setIsValidMedia] = useState(undefined);
    const [mediaMsg, setMediaMsg] = useState("");
    const [isYoutubeInputEnabled, setIsYoutubeInputEnabled] = useState(false);


    const youtubeLinkInputRef = useRef(null);
    const textareaRef = useRef(null);
    const imageInputRef = useRef(null);
    const mediaRef = useRef(null);




    const autoResize = () => {
        const textarea = textareaRef.current;
        textarea.style.height = "auto";

        const scrollHeight = textarea.scrollHeight;

        textarea.style.height = `${scrollHeight}px`;
    }


    const inputYoutubeLink = (link) => {
        setIsValidMedia(undefined);

        if (!link) return;

        const testEl = document.createElement("div");

        testEl.innerHTML = link;

        const children = testEl.children;

        /**
         * When iframe html is given,
         * Validate the link
         * */
        if (!testEl.textContent && children.length===1 && children[0].nodeName==="IFRAME") {
            const iframe = children[0];

            // Attributes and src url validation
            if (Number(iframe.width)===0 || Number(iframe.height)===0 || iframe.src.substr(0, "https://www.youtube.com/".length)!=="https://www.youtube.com/") {
                setIsValidMedia(false);
                setMediaMsg("잘못된 YouTube 링크");
                return;
            }

            mediaRef.current.innerHTML = iframe.outerHTML;

            setIsValidMedia(true);
        }
        /**
         * Any text is given,
         * Validate if it is a youtube link
         * */
        else {
            const stripLink = link.replace(" ", "");

            // Find suffix
            const domainMatch = /https:\/\/www\.youtube\.com\/watch\?v=([\S]+)/g.exec(stripLink) || /https:\/\/youtu\.be\/([\S]+)/g.exec(stripLink);

            let suffix;

            if (domainMatch && domainMatch.length>=2) suffix = domainMatch[1];

            let id;
            let startTime;

            // Find start time
            const startTimeMatch = /(.+)\?t=([\d]+)/.exec(suffix);

            if (startTimeMatch && startTimeMatch.length>=3 && startTimeMatch[2]) {
                id = startTimeMatch[1];
                startTime = startTimeMatch[2];
            } else {
                id = suffix
            }


            if (id) {
                const iframe = document.createElement("iframe");

                iframe.width = "560";
                iframe.height = "315";
                iframe.src = `https://www.youtube.com/embed/${id}${startTime? `?start${startTime}`: ""}`;
                mediaRef.current.innerHTML = iframe.outerHTML;

                setIsValidMedia(true);
                return;
            }


            setIsValidMedia(false);
            setMediaMsg("잘못된 YouTube 링크");
        }
    }



    const onImgInput = async () => {
        if (imageInputRef.current.files.length!==1) return;

        setIsValidMedia(undefined);
        mediaRef.current.innerHTML = "";

        const file = imageInputRef.current.files[0]

        if (![
            "image/jpg",
            "image/jpeg",
            "image/png",
            "image/gif",
            "image/svg",
            "image/svg+xml",
        ].includes(file.type)) {
            setIsValidMedia(false);
            setMediaMsg("지원되는 이미지 파일이 아님");
            return;
        }

        const reader = new FileReader();
        let blob;

        /**
         * Gif input
         * */
        if (file.type==="image/gif") {
            // File size limit for gif
            if (file.size > 2e+7) {
                setIsValidMedia(false);
                setMediaMsg("gif 파일은 20MB까지 업로드 가능");
                return;
            }

            blob = file;
        }
        /**
         * None-gif input
         * */
        else {
            try {
                // File size limit for jpeg/png
                if (file.size > 5e+7) {
                    setIsValidMedia(false);
                    setMediaMsg("이미지 파일은 50MB까지 업로드 가능");
                    return;
                }

                if (file.size > 1e+6) {
                    const blobImgData = await optimizeImage(file, 720, 0.95);
                    blob = blobImgData.blob;
                }
                else {
                    blob = file
                }
            } catch (e) {
                setIsValidMedia(false);
                setMediaMsg("지원되는 이미지 파일이 아님.");
            }

        }


        // ON LOAD
        reader.onload = (e) => {
            const img = document.createElement("img");

            img.src = e.target.result;
            mediaRef.current.appendChild(img);


            if (isYoutubeInputEnabled) {
                youtubeLinkInputRef.current.clearText();
                setIsYoutubeInputEnabled(false);
            }

            setIsValidMedia(true);
        }

        // ON ERROR
        reader.onerror = (e) => {
            setIsValidMedia(false);
            setMediaMsg("이미지 불러오기 실패");
        }


        reader.readAsDataURL(blob);

        imageInputRef.current.value = "";
    }




    const onNicknameKeydown = (e) => {
        switch (e.keyCode) {
            case Keyboard.KEY.ENTER:
                e.preventDefault();
                break;
        }
    }


    const toggleImgInputButton = () => {
        imageInputRef.current.click();
    }


    const toggleYoutubeInputButton = () => {
        if (isYoutubeInputEnabled) {
            setIsValidMedia(undefined);
            youtubeLinkInputRef.current.clearText();
        }

        setIsYoutubeInputEnabled(!isYoutubeInputEnabled);
    }


    const onSubmit = async () => {
        const text = textareaRef.current.value;
        let imgFile;
        let youtubeHTML;

        if (
            (!text && !isValidMedia) ||
            (!isRegUser && (!nickname || !password))
        ) return;



        if (isValidMedia) {
            const mediaEl = mediaRef.current.children[0];

            /**
             * Image input
             * */
            if (mediaEl.nodeName==="IMG") {
                const mimeType = mediaEl.src.split(";")[0].split("data:")[1]
                const fileExt = mimeType.split("/")[1];

                imgFile = dataURLtoFile(mediaEl.src, "file." + fileExt);
            }
            /**
             * Image youtube
             * */
            else if (mediaEl.nodeName==="IFRAME") {
                youtubeHTML = mediaEl.outerHTML;
            }
        }



        /**
         * Upload as a comment
         * */
        let commentData;
        if (!inCommentSection) {
            commentData = await requestUploadComment(postNumber, category, board, imgFile, youtubeHTML, text, nickname, password);
        }
        /**
         * Upload as a reply-comment
         * */
        else {
            commentData = await requestUploadReplyComment(postNumber, repliedCommentNumber, category, board, imgFile, youtubeHTML, text, nickname, password);
        }


        if (mediaRef.current) mediaRef.current.innetHTML = "";
        if (textareaRef.current) textareaRef.current.value = "";
        if (youtubeLinkInputRef.current) youtubeLinkInputRef.current.clearText();
        if (isYoutubeInputEnabled) setIsYoutubeInputEnabled(false);
        if (isValidMedia) setIsValidMedia(false);

        dispatch(setCommentState(commentData.data))

        dispatch(setCommentNumberToWrite(null));

    }




    const onNicknameChange = (e) => {
        const _nickname = e.target.value;

        setNickname(_nickname);

        if (_nickname) {
            localStorage.setItem(LocalStorage_.UnregUser.NICKNAME, _nickname);
        } else {
            localStorage.removeItem(LocalStorage_.UnregUser.NICKNAME);
        }
    }

    const onPasswordChange = (e) => {
        const _password = e.target.value;

        setPassword(_password);

        if (_password) {
            localStorage.setItem(LocalStorage_.UnregUser.PW, _password);
        } else {
            localStorage.removeItem(LocalStorage_.UnregUser.PW);
        }
    }


    useEffect(() => {
        const _nickname = localStorage.getItem(LocalStorage_.UnregUser.NICKNAME);
        const _password = localStorage.getItem(LocalStorage_.UnregUser.PW);

        if (_nickname) setNickname(_nickname);
        if (_password) setPassword(_password);
    }, [isRegUser])



    return (
        <div className={style["container"]}>

            {/** MENUS SECTION */}
            <section className={style["menus"]}>

                {/** MENU BUTTONS */}
                <section className={style["buttons"]}>

                    {!isRegUser && (
                        <div className={style["unregistered-user-info"]}>
                            <input className={style["nickname"]} type="text" placeholder="닉네임" spellCheck={false} autoComplete="off" value={nickname} onKeyDown={onNicknameKeydown} onChange={onNicknameChange}/>
                            <input className={style["password"]} type="password" placeholder="비밀번호" title="댓글 수정/삭제에 사용" spellCheck={false} autoComplete="off" value={password} onChange={onPasswordChange}/>
                        </div>
                    )}

                    <button className={style["image-upload"]} onClick={toggleImgInputButton}>
                        <div className={style["icon"]}/>
                        <span>이미지 파일</span>
                        <input className={style["img"]} type="file" accept=".jpg,.jpeg,.png,.gif" multiple={false} onChange={onImgInput} ref={imageInputRef}/>
                    </button>

                    <button className={style["youtube-link"]} onClick={toggleYoutubeInputButton}>
                        <span>YouTube</span>
                    </button>
                </section>


                {/** YOUTUBE LINK INPUT */}
                <section className={`${style["youtube-link-input"]}${!isYoutubeInputEnabled? ` ${style["hide"]}`: ""}`}>
                    <Panel_YoutubeLinkInput apply={inputYoutubeLink} ref={youtubeLinkInputRef}/>
                </section>

            </section>


            {/** INPUT COMMENT SECTION */}
            <section className={style["editor"]}>
                <div className={`${style["content"]} ${inCommentSection? style["narrow"]: ""}`}>
                    <div className={`${style["media"]} ${!isValidMedia? style["hide"]: ""}`} ref={mediaRef}/>
                    {isValidMedia===false && (<p className={style["error-message"]}>{mediaMsg}</p>)}

                    <textarea rows={4} spellCheck={false} placeholder={inCommentSection? "대댓글 입력": "댓글 입력"} onInput={autoResize} ref={textareaRef}/>
                </div>
                <button className={style["submit"]} title={inCommentSection? "대댓글등록": "댓글등록"} onClick={onSubmit}>
                    {inCommentSection? "대댓글등록": "댓글등록"}
                </button>
            </section>
        </div>
    )
})


export default CommentWrite;