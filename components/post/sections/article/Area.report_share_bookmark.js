import React from "react";
import style from "./styles/Area.report_share_bookmark.module.scss";


const ReportShareBookmark = ({postUrlCopiedMessageFunc}) => {





    const copyUrlToClipboard = () => {
        postUrlCopiedMessageFunc();
    }



    return (
        <div className={style["container"]}>
            <p className={style["report"]}><img src="/icons/svg/small/report.svg"/><label>신고</label></p><i/>
            <p className={style["link-share"]} onClick={copyUrlToClipboard}>
                <label className={style["icon"]}>URL</label>
                <label>링크공유</label>
            </p>

            <i/>
            <p className={style["bookmark"]}><img src="/icons/svg/small/bookmark.svg"/><label>북마크</label></p>
        </div>
    )
}


export default ReportShareBookmark;