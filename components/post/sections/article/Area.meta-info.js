import React from "react";
import {useSelector} from "react-redux";
import style from "./styles/Area.meta-info.module.scss";
import time from "../../../../utils/time";
import {parseFullUserInfo} from "../../../../utils/format";





const MetaInfo = () => {
    const postState = useSelector(state => state.post);
    const commentState = useSelector(state => state.comment);

    const {userId, ipAddress, nickname, record, createdTime, updatedTime} = postState;
    const {viewCount} = record;
    const {totalCommentCount} = commentState;

    const isUnregUser = !userId;


    const isUpdatedBefore = createdTime!==updatedTime;


    const createdTimeFullText = `작성시각: ${time.fullFormatText(createdTime)}`;
    const updatedTimeFullText = `수정시각: ${time.fullFormatText(updatedTime)}`;



    return (
        <div className={style["container"]}>
            <p className={style["nickname"]}><label title={parseFullUserInfo(userId, nickname, ipAddress)}>{nickname}</label></p><i/>
            <div className={style["comment"]} title={`댓글수: ${totalCommentCount}`}><div className={style["icon"]}/>{totalCommentCount}</div><i/>
            <p className={style["views"]} title={`조회수: ${viewCount}`}>{viewCount}</p><i/>
            <p className={style["time"]} title={!isUpdatedBefore? createdTimeFullText: `${createdTimeFullText}\n${updatedTimeFullText}`}>{time.shortFormatText(createdTime)}</p>
        </div>
    )
}


export default MetaInfo;