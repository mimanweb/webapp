import React, {useRef} from "react";
import {useSelector} from "react-redux";
import style from "./styles/Panel.article.module.scss";





const Article = () => {
    const postState = useSelector(state => state.post);
    const {content} = postState;

    const postRef = useRef(null);



    return (
        <div className={style["container"]}>
            <article className={style["post_content"]} dangerouslySetInnerHTML={{__html: content}} ref={postRef}/>
        </div>
    )
}


export default Article;