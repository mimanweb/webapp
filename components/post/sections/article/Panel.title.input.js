import React, {useRef, useEffect} from "react";
import {useSelector, useDispatch} from "react-redux";
import style from "./styles/Panel.titleInput.module.scss"
import {updatePostState} from "../../../../features/post/actions";
import {updatePostAfterDelay} from "../../../../utils/editor/delayExecutor";
import Post_ from "../../../../utils/index/post";
import Keyboard from "../../../../utils/editor/keyboard";



const TitleInput = () => {
    const dispatch = useDispatch();

    const userState = useSelector(state => state.user);
    const postState = useSelector(state => state.post);
    const editorState = useSelector(state => state.editor);

    const {loggedIn} = userState;
    const {editorWritingBoard, routeOnAutoSaveFail} = editorState;
    const {editMode} = postState;

    const isEditMode = editMode===Post_.EDIT_MODE.EDIT;
    const isRegUser = loggedIn;


    const inputRef = useRef(null);



    const onKeyDown = (e) => {
        if (isRegUser && e.keyCode===Keyboard.KEY.TAB) {
            e.preventDefault();
            editorWritingBoard.focus();
        }
    }

    const onInput = async () => {
        /**
         * Update state and the temp post data
         * */
        const title = inputRef.current.value? inputRef.current.value: "";

        dispatch(updatePostState("title", title));

        if (!isEditMode) await updatePostAfterDelay(postState, "title", title, undefined, routeOnAutoSaveFail);
    }



    useEffect(()=>{
        inputRef.current.value = postState.title;
    }, [postState.title])


    return (
        <div className={style["container"]}>
            <input type="text" placeholder="글 제목" spellCheck={false} onKeyDown={onKeyDown} onInput={onInput} ref={inputRef}/>
        </div>
    )
}


export default TitleInput;