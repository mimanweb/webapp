import React, {useRef} from "react";
import {useSelector} from "react-redux";
import style from "./styles/Buttons.edit_delete.module.scss";
import {requestPostOwnershipCheck, requestDeletePost} from "../../../../features/post/actions";
import {useRouter} from "next/router";


/** COMPONENTS */
import XSmallButton, {XSmallButtonType} from "../../../button/x-small";
import {StretchOpenMessageBoard, StretchOpenBoardType} from "../../../panel";


const EditDelete = () => {
    const router = useRouter();

    const userState = useSelector(state => state.user);
    const postState = useSelector(state => state.post);

    const {number: postNumber, category, board} = postState;
    const isRegUser = userState.loggedIn;
    const isRegUserPost = postState.userId;


    const verifyBeforeEditBoardRef = useRef();
    const verifyBeforeDeleteBoardRef = useRef();



    const onEditButtonClick = async () => {
        if (!isRegUserPost) {
            if (verifyBeforeDeleteBoardRef.current.checkIsOpened()) verifyBeforeDeleteBoardRef.current.close();

            verifyBeforeEditBoardRef.current.openOrClose();
        } else {
            const {status} = await requestPostOwnershipCheck(postNumber, category, board);

            switch (status) {
                case 200:
                    await router.push(`/post/write/${category}/${board}/${postNumber}`);
                    break;
                case 403:
                    await router.reload();
                    break;
                case 404:
                    alert("게시글이 존재하지 않습니다.");
                    await router.push(`/board/${category}/${board}`);
                    break;
            }
        }

    }


    const onDeleteButtonClick = async () => {
        if (!isRegUserPost) {
            if (verifyBeforeEditBoardRef.current.checkIsOpened()) verifyBeforeEditBoardRef.current.close();

            verifyBeforeDeleteBoardRef.current.openOrClose();
        } else {
            const {status} = await requestDeletePost(postNumber, category, board);

            switch (status) {
                case 200:
                    await router.push(`/board/${category}/${board}`);
                    break;
                case 403:
                    await router.reload();
                    break;
                case 404:
                    alert("게시글이 존재하지 않습니다.");
                    await router.push(`/board/${category}/${board}`);
                    break;
            }
        }
    }



    return (
        <div className={style["container"]}>
            <section className={style["buttons"]}>
                <XSmallButton buttonType={XSmallButtonType.EDIT_POST} onClick={onEditButtonClick}/>
                <XSmallButton buttonType={XSmallButtonType.DELETE_POST} onClick={onDeleteButtonClick}/>
            </section>

            <section>
                <StretchOpenMessageBoard boardType={StretchOpenBoardType.VERIFY_OWNER_BEFORE_EDIT_POST} ref={verifyBeforeEditBoardRef}/>
                <StretchOpenMessageBoard boardType={StretchOpenBoardType.VERIFY_OWNER_BEFORE_DELETE_POST} ref={verifyBeforeDeleteBoardRef}/>
            </section>
        </div>
    )
}


export default EditDelete;