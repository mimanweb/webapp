import React, {useState, useEffect} from "react";
import {useSelector} from "react-redux";
import style from "./styles/Panel.board-info.module.scss";
import {findBoardMapItem, findCategoryMapItem} from "../../../../utils/index/siteIndex";
import Link from "next/link";



const BoardInfo = () => {
    const postState = useSelector(state => state.post);



    const currCategory = findCategoryMapItem(postState.category);
    const currBoard = currCategory && findBoardMapItem(postState.category, postState.board);

    const isIndexInfoSet = currCategory && currBoard;


    return (
        <div className={style["container"]}>
            <Link href={isIndexInfoSet? `/board/${currCategory.code}/${currBoard.code}`: "/"}>
                <a>{isIndexInfoSet? currBoard.fullName: ""}</a>
            </Link>
        </div>
    )
}


export default BoardInfo;