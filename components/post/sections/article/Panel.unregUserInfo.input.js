import React, {useState, useEffect, useRef} from "react";
import {useDispatch, useSelector} from "react-redux";
import style from "./styles/Panel.unregUserInfo.input.module.scss";
import {setPostOwnerInfo} from "../../../../features/post/actions";
import {checkBlankText} from "../../../../utils/editor/commonUtils";
import Keyboard from "../../../../utils/editor/keyboard";
import POST_ from "../../../../utils/index/post";
import LocalStorage_ from "../../../../utils/index/localStorage";





const UnregUserInfoInput = () => {
    const dispatch = useDispatch();


    const postState = useSelector(state => state.post);
    const editorState = useSelector(state => state.editor);


    const [nickname, setNickname] = useState("");
    const [password, setPassword] = useState("");

    const {editMode, nickname: postNickname} = postState;
    const {editorWritingBoard} = editorState;
    const isEditMode = editMode===POST_.EDIT_MODE.EDIT;


    const nicknameInputRef = useRef(null);
    const pwInputRef = useRef(null);



    const onNicknameKeydown = (e) => {
        /**
         * Prevent input space
         * */
        if ([Keyboard.KEY.SPACE].includes(e.keyCode)) e.preventDefault();
    }


    const onPasswordKeydown = (e) => {
        switch (e.keyCode) {
            case Keyboard.KEY.SPACE:
                e.preventDefault();
                break;
            case Keyboard.KEY.TAB:
                e.preventDefault();
                editorWritingBoard.focus();
                break;
        }

    }


    const onNicknameChange = (e) => {
        const _nickname = e.target.value;

        setNickname(_nickname);

        if (_nickname) {
            localStorage.setItem(LocalStorage_.UnregUser.NICKNAME, _nickname);
            dispatch(setPostOwnerInfo(_nickname, password));
        } else {
            localStorage.removeItem(LocalStorage_.UnregUser.NICKNAME);
            dispatch(setPostOwnerInfo(null, password));
        }
    }

    const onPasswordChange = (e) => {
        const _password = e.target.value;

        setPassword(_password);

        if (_password) {
            localStorage.setItem(LocalStorage_.UnregUser.PW, _password);
            dispatch(setPostOwnerInfo(nickname, _password));
        } else {
            localStorage.removeItem(LocalStorage_.UnregUser.PW);
            dispatch(setPostOwnerInfo(nickname, null));
        }
    }


    useEffect(() => {
        /**
         * Fill input field if previously saved data exists
         * */
        switch (editMode) {
            case POST_.EDIT_MODE.WRITING:
                const _nickname = localStorage.getItem(LocalStorage_.UnregUser.NICKNAME);
                const _password = localStorage.getItem(LocalStorage_.UnregUser.PW);

                if (_nickname) {
                    nicknameInputRef.current.value = _nickname;
                    setNickname(_nickname);
                }
                if (_password) {
                    pwInputRef.current.value = _password;
                    setPassword(_password);
                }

                break;
            case POST_.EDIT_MODE.EDIT:
                setNickname(postNickname);
                break;
        }

    }, [editMode])


    return (
        <div className={style["container"]}>
            <label>비회원 작성</label>

            <input className={isEditMode? style["blank"]: null} type="text" placeholder="닉네임" spellCheck={false} autoComplete="off" disabled={isEditMode} value={nickname} onKeyDown={onNicknameKeydown} onChange={onNicknameChange} ref={nicknameInputRef}/>

            {editMode===POST_.EDIT_MODE.WRITING && (
                <input className={style["password"]} type="password" placeholder="비밀번호" title="글 수정/삭제에 사용" autoComplete="off" value={password} onKeyDown={onPasswordKeydown} onChange={onPasswordChange} ref={pwInputRef}/>
            )}
        </div>
    )
}


export default UnregUserInfoInput;