import React from "react";
import {useSelector} from "react-redux";
import style from "./styles/Panel.title.module.scss";
import Meta from "../../../meta/head";


const Title = () => {
    const postState = useSelector(state => state.post);


    return (
        <div className={style["container"]}>
            <h1 className={style["post_title"]}>{postState.title}</h1>
        </div>
    )
}


export default Title;