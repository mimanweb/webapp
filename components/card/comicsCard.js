import style from "./comicsCard.module.scss";
import React from "react";
import Link from "next/link";
import {COMIC_BOOK_IMAGE_STATUS} from "../../utils/index/comics/book";
import {parseImgId} from "../../utils/image/parse";
import {parseComicBookEpText, parseComicBookLinkPath} from "../../utils/format";


export const COMICS_CARD_TYPE = {
    BIG: "big",
    SMALL: "small"
};


const ComicsCard = ({type, comicBook}) => {

    const {comics, index} = comicBook
    const linkThumbnail = comicBook.linkThumbnails.find(thumbnail => thumbnail.status===COMIC_BOOK_IMAGE_STATUS.PUBLISH);


    return (
        <div className={`${style["container"]} ${style[type]}`}>
            <Link href={parseComicBookLinkPath(comics, comicBook)}>
                <a className={style["card"]}>
                    <div className={style["thumbnail"]}>
                        <img src={parseImgId(linkThumbnail.imgId.large)}/>
                    </div>
                    <div className={style["desc"]}>
                        <span className={style["title"]}>{comics.title.korean}</span>
                        <span className={style["episode"]}>{parseComicBookEpText(index)}</span>
                    </div>
                </a>
            </Link>
        </div>
    )
}




export default ComicsCard;