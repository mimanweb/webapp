import React from "react";
import styles from './footer.module.scss';

export default function Footer() {
    return (
        <footer className={styles['container']}>
            <p>version.alpha</p>
            <p>주인장 문의 mimanweb.master@gmail.com</p>
        </footer>
    )
}