import style from "./pageNavigator.module.scss";
import React from "react";
import _ from "underscore";
import Link from "next/link";




export const PAGE_NAVIGATOR_THEME = {
    DARK: "dark"
}




const ToFirstPage = ({isLinkMode, onClick, linkPath, additionalLinkQuery}) => {
    switch (typeof linkPath) {
        case "string":
            linkPath = {
                pathname: linkPath,
                query: additionalLinkQuery
            }
            break;
        case "object":
            linkPath = {
                pathname: linkPath.pathname,
                query: {...linkPath.query, ...additionalLinkQuery}
            }
            break;
    }


    if (isLinkMode) {
        return (
            <Link href={linkPath}>
                <a className={style["to_firstpage"]}>
                    <div className={style["icon"]}/>
                    <div className={style["icon"]}/>
                    <span>처음</span>
                </a>
            </Link>
        )
    } else {
        return (
            <div className={style["to_firstpage"]} onClick={onClick}>
                <div className={style["icon"]}/>
                <div className={style["icon"]}/>
                <span>처음</span>
            </div>
        )
    }
}


const ToPrevPage = ({isLinkMode, onClick, linkPath, additionalLinkQuery}) => {
    switch (typeof linkPath) {
        case "string":
            linkPath = {
                pathname: linkPath,
                query: additionalLinkQuery
            }
            break;
        case "object":
            linkPath = {
                pathname: linkPath.pathname,
                query: {...linkPath.query, ...additionalLinkQuery}
            }
            break;
    }


    if (isLinkMode) {
        return (
            <Link href={linkPath}>
                <a className={style["to_prevpage"]}>
                    <div className={style["icon"]}/>
                    <span>이전</span>
                </a>
            </Link>
        )
    } else {
        return (
            <div className={style["to_prevpage"]} onClick={onClick}>
                <div className={style["icon"]}/>
                <span>이전</span>
            </div>
        )
    }
}


const ToPage = ({isLinkMode, pageNumber, isCurrPage, onClick, linkPath, additionalLinkQuery}) => {
    if (isLinkMode) {
        return (
            <Link href={{
                pathname: linkPath,
                query: {...{page: pageNumber}, ...additionalLinkQuery}
            }}>
                <a className={isCurrPage? style["selected"]: null}>{pageNumber}</a>
            </Link>
        )
    } else {
        return (
            <p className={isCurrPage? style["selected"]: null} onClick={()=>onClick(pageNumber)}>{pageNumber}</p>
        )
    }
}


const ToNextPage = ({isLinkMode, onClick, linkPath, additionalLinkQuery}) => {
    switch (typeof linkPath) {
        case "string":
            linkPath = {
                pathname: linkPath,
                query: additionalLinkQuery
            }
            break;
        case "object":
            linkPath = {
                pathname: linkPath.pathname,
                query: {...linkPath.query, ...additionalLinkQuery}
            }
            break;
    }


    if (isLinkMode) {
        return (
            <Link href={linkPath}>
                <a className={style["to_nextpage"]}>
                    <span>다음</span>
                    <div className={style["icon"]}/>
                </a>
            </Link>
        )
    } else {
        return (
            <div className={style["to_nextpage"]} onClick={onClick}>
                <span>다음</span>
                <div className={style["icon"]}/>
            </div>
        )
    }
}



const HorizontalTextPageNavigator = ({isLinkMode, toFirstPageLinkPath, toFirstPageFunc, toPrevPageLinkPath, toPrevPageFunc, toPageLinkPath, toPageFunc, toNextPageLinkPath, toNextPageFunc, currPage, totalPageCount, pagePerPageGroupCount, additionalLinkQuery, theme}) => {

    const totalPageGroup = Math.ceil(totalPageCount/pagePerPageGroupCount);
    const currPageGroup = Math.ceil(currPage/pagePerPageGroupCount);

    const pageStartOfCurrPageGroup = (currPageGroup-1)*pagePerPageGroupCount;
    const pageEndOfCurrPageGroup = (currPageGroup)*pagePerPageGroupCount;

    const isEmptyPage = !totalPageCount


    let themeStyle = "";

    switch (theme) {
        case PAGE_NAVIGATOR_THEME.DARK:
            themeStyle = style["dark"];
            break;
    }


    return (
        <div className={`${style["container"]} ${themeStyle}`}>

            {/** TO INIT PAGE INDEX */}
            {(
                !isEmptyPage &&
                currPage!==1
            )
            && (
                <ToFirstPage isLinkMode={isLinkMode} linkPath={toFirstPageLinkPath} onClick={toFirstPageFunc} additionalLinkQuery={additionalLinkQuery}/>
            )}


            {/** TO PREV PAGE INDEX */}
            {(
                !isEmptyPage &&
                currPageGroup!==1
            )
            && (
                <ToPrevPage isLinkMode={isLinkMode} linkPath={toPrevPageLinkPath} onClick={toPrevPageFunc} additionalLinkQuery={additionalLinkQuery}/>
            )}


            {/** COMMENT PAGES */}
            <div className={style["numbers"]}>
                {_.times(totalPageCount, (n) => {
                    const page = n+1;
                    const isPageWithinPageGroupRange = (page>pageStartOfCurrPageGroup) && (page<=pageEndOfCurrPageGroup);
                    const isCurrPage = currPage===page;

                    if (isPageWithinPageGroupRange) {
                        return <ToPage key={page} isLinkMode={isLinkMode} pageNumber={page} isCurrPage={isCurrPage} linkPath={toPageLinkPath} onClick={()=>toPageFunc(page)} additionalLinkQuery={additionalLinkQuery}/>
                    }
                })}
            </div>


            {/** TO NEXT PAGE INDEX */}
            {(
                !isEmptyPage &&
                (currPageGroup!==totalPageGroup) && (totalPageGroup>1)
            )
            && (
                <ToNextPage isLinkMode={isLinkMode} linkPath={toNextPageLinkPath} onClick={toNextPageFunc} additionalLinkQuery={additionalLinkQuery}/>
            )}
        </div>
    )
}


export default HorizontalTextPageNavigator;