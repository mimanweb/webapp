import Head from 'next/head';
import React from "react";


export default function DocHead({title="미만웹", desc}) {

    return (
        <Head>
            <html lang="ko"/>
            <link rel="icon" type="image/x-icon" href="/favicon.svg"/>
            <title>{title}</title>
            <meta name="title" content={title}/>
            <meta property="og:title" content={title}/>
            {desc && (
                <>
                    <meta name="description" content={desc}/>
                    <meta property="og:description" content={desc}/>
                </>
            )}
        </Head>
    )
}