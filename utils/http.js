import axios from "axios";
import {loginSuccess} from "../features/user/actions";
import {END} from "redux-saga";
import _ from "underscore";



let cancelToken;




/**
 * GET
 * */
export async function get(url, params = {}, config = {}, clientContext = null, cancelable = false) {
    let isServerDown;

    const options = {
        params: params,
    }

    if (cancelable) {
        if (cancelToken) cancelToken.cancel("Operation canceled due to a new request");
        cancelToken = axios.CancelToken.source();

        options.cancelToken = cancelToken.token;
    }

    const client = clientContext;
    if (client) {
        const _headers = {};

        const cookieFromClient = client.req.headers.cookie;
        const xForwardedForFromClient = client.req.headers["x-forwarded-for"];

        if (cookieFromClient) _headers.Cookie = cookieFromClient;
        if (xForwardedForFromClient) _headers["X-Forwarded-For"] = xForwardedForFromClient;

        options.headers = _headers;
    }

    config.withCredentials = true;

    const serverRes = await axios.get(url, {...options, ...config})
        .then(serverRes => {

            /**
             * Check user user data and save it
             * */
            const userData = serverRes.data.user;

            if (userData && !_.isEmpty(userData)) {

                client.store.dispatch(loginSuccess(userData));
                client.store.dispatch(END);
                client.store.sagaTask.toPromise();
            }

            return serverRes;
        })
        .catch(err => {
            /** ERROR FORMAT CONFIG */

            /**
             * Server is down
             * */
            if (err.response===undefined) {
                isServerDown = true;
                return;
            }

            const res = err.response;

            res.error = true;
            res.canceled = axios.isCancel(err);

            if (typeof res.data === "object") {
                res.code = res.data.code;
                res.message = res.data.message;
                res.data = res.data.data;
            }

            return res;
        });


    if (isServerDown) {
        return {
            status: 500,
            isServerDown,
            data: null
        };
    } else {
        serverRes.isServerDown = false;
    }


    /** Send COOKIE along the client response */
    if (serverRes.headers['set-cookie']) client.res.setHeader('Set-Cookie', serverRes.headers['set-cookie']);
    return serverRes;
}


/**
 * POST
 * */
export async function post(url, params = {}, config = {}, cancelable = false) {
    if (cancelable) {
        if (cancelToken) cancelToken.cancel("Operation canceled due to a new request");
        cancelToken = axios.CancelToken.source();
        config.cancelToken = cancelToken.token;
    }

    config.withCredentials = true;

    return await axios.post(url, params, config).catch(err => {
        console.error(err)
        /** config error format */

        const res = err.response;
        res.error = true;

        res.canceled = axios.isCancel(err);

        if (typeof res.data === "object") {
            res.code = res.data.code;
            res.message = res.data.message;
            res.data = res.data.data;
        }

        return res;
    });
}


/**
 * PUT
 * */
export async function put(url, params = {}, config = {}, cancelable = false) {
    if (cancelable) {
        if (cancelToken) cancelToken.cancel("Operation canceled due to a new request");
        cancelToken = axios.CancelToken.source();
        config.cancelToken = cancelToken.token;
    }

    config.withCredentials = true;

    return await axios.put(url, params, config).catch(err => {
        /** config error format */

        const res = err.response;
        res.error = true;

        res.canceled = axios.isCancel(err);

        if (typeof res.data === "object") {
            res.code = res.data.code;
            res.message = res.data.message;
            res.data = res.data.data;
        }

        return res;
    });
}


/**
 * DELETE
 * */
export async function del(url, params = {}, config = {}, cancelable = false) {
    if (cancelable) {
        if (cancelToken) cancelToken.cancel("Operation canceled due to a new request");
        cancelToken = axios.CancelToken.source();
        config.cancelToken = cancelToken.token;
    }

    config.withCredentials = true;
    config.data = params

    return await axios.delete(url, config).catch(err => {
        /** config error format */

        const res = err.response;
        res.error = true;

        res.canceled = axios.isCancel(err);

        if (typeof res.data === "object") {
            res.code = res.data.code;
            res.message = res.data.message;
            res.data = res.data.data;
        }

        return res;
    });
}



/**
 * Handling api server response http errors
 * In Saga
 * */
export function* errBehave(err, status = null, code = null, callback) {
    // Handling default error
    const errRes = err.response
    const errStatus = errRes.status;

    if (errStatus !== status) return;

    let errCode = null;
    if (errRes.data && errRes.data.code) errCode = errRes.data.code;

    if (errCode !== code) return;

    return callback();
}


export default {
    get,
    post,
    put,
    delete: del
}