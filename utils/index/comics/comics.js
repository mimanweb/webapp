import {parseRecentComicsListText} from "../../format";
import {requestRecentReadComicsLatestUpdateInfo, setRecentReadComicsListState} from "../../../features/comics/actions";

export const COMICS_STATUS = {
    DELETED: "DELETED",
    APPROVED: "APPROVED",
    AWAITING_APPROVAL: "AWAITING_APPROVAL",
}


export const COMICS_SEARCH_LIST_PAGE_GROUP_COUNT = 10;



export const updateRecentReadComicsList = async (dispatch) => {
    const listTxt = localStorage.getItem("recentReadComics");

    const recentReadList = parseRecentComicsListText(listTxt);


    const {status, data} = await requestRecentReadComicsLatestUpdateInfo(recentReadList);

    if (status===200) {
        dispatch(setRecentReadComicsListState(data.recentReadComicsList))
    }
}