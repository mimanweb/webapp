

export const COMIC_BOOK_CONTENT_TYPE = {
    LINK: "LINK",
    IMAGE: "IMAGE"
}


export const COMIC_BOOK_PAGE_TYPE = {
    SINGLE: "SINGLE",
    WIDE: "WIDE",
    PAIRED: "PAIRED"
}


export const COMIC_BOOK_IMAGE_STATUS = {
    WRITING: "WRITING",
    PUBLISH: "PUBLISH",
    DELETED: "DELETED",
}


export const COMIC_BOOK_STATUS = {
    WRITING: "WRITING",
    PUBLISH: "PUBLISH",
    DELETED: "DELETED",
    BLOCKED: "BLOCKED",
    EDITING: "EDITING",
}


export const COMIC_BOOK_EDIT_MODE = {
    WRITE: "WRITE",
    EDIT: "EDIT",
}