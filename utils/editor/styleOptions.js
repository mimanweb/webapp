const COLOR = [
    {value: "rgb(38, 38, 38)", label: "검정"},
    {value: "rgb(255, 0, 0)", label: "빨강"},
    {value: "rgb(0, 0, 255)", label: "파랑"},
]

const FONT_SIZE = [
    {value: "14px", label: "보통"},
    {value: "20px", label: "큰"},
    {value: "28px", label: "큰큰"},
    {value: "36px", label: "큰큰큰"}
];

const FONT_WEIGHT = [
    {value: "400", label: "보통"},
    {value: "700", label: "굵게"}
];

const FONT_STYLE = [
    {value: "normal", label: "보통"},
    {value: "italic", label: "기울게"},
];

const DEFAULT_OPTIONS = {
    color: COLOR[0],
    fontSize: FONT_SIZE[0],
    fontWeight: FONT_WEIGHT[0],
    fontStyle: FONT_STYLE[0]
}

const ATTRIBUTES = Object.keys(DEFAULT_OPTIONS);

const makeStyleObj = (style, value) => {
    const optionMap = {
        color: COLOR,
        fontSize: FONT_SIZE,
        fontWeight: FONT_WEIGHT,
        fontStyle: FONT_STYLE
    }

    const option = optionMap[style];
    return option.find(obj => obj.value===value);
}


export default {
    COLOR,
    FONT_SIZE,
    FONT_WEIGHT,
    FONT_STYLE,
    DEFAULT_OPTIONS,
    ATTRIBUTES,
    makeStyleObj
}