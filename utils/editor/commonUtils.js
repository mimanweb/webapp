import STYLE_ from "./styleOptions";



export const findLineContainer = (node) => {
    if (node.nodeName==="DIV") return false;
    return node.nodeName==="P"? node: findLineContainer(node.parentNode);
}


export const findWrapperNode = (node) => {
    if (["SPAN", "P"].includes(node.nodeName)) return node;
    else return node.parentNode.nodeName === "P"? node: findWrapperNode(node.parentNode);
}


export const findTextWrapperNode = (node) => {
    if (node.nodeName==="SPAN") return node;
    else if (node.nodeName==="#text" && node.parentNode) return findTextWrapperNode(node.parentNode);
    else if (["P", "DIV"].includes(node.nodeName)) return null;
    else return null;
}


export const findUniqueElFromLine = (line) => {
    return line.hasChildNodes() && [...line.childNodes.values()].find(node => ["IMG", "IFRAME"].includes(node.nodeName));
}


export const findLinkElFromLine = (line) => {
    return line.hasChildNodes() && [...line.childNodes.values()].find(node => ["A"].includes(node.nodeName));
}


export const findLineStartSpan = (line) => {
    const list = [];

    line.childNodes.forEach(node => {
        if (node.textContent || node.nodeName==="SPAN") list.push(node);
    })

    return list.length>0 && list[0].nodeName==="SPAN"? list[0]: null;
}


export const findRightNodesCutFromRangeEnd = (node, offset, cloneIt) => {
    const list = [];

    // Node at the current cursor ends
    // Exclude if the cursor end points at the end of node(which has "" text)
    const nodeAtCursorEnd = node.textContent.length === offset? null: node.cloneNode(true)
    if (nodeAtCursorEnd) {
        if (nodeAtCursorEnd.nodeName === "SPAN") nodeAtCursorEnd.firstChild.textContent = nodeAtCursorEnd.firstChild.textContent.substr(offset);
        else if (nodeAtCursorEnd.nodeName === "#text") nodeAtCursorEnd.textContent = nodeAtCursorEnd.textContent.substr(offset);

        list.push(nodeAtCursorEnd);
    }

    const findNextSlings = (node) => {
        const nextSibling = node.nextSibling;

        if (!nextSibling) return;

        if (nextSibling.nodeName==="BR") list.push(nextSibling);

        if (nextSibling.textContent) list.push(cloneIt? nextSibling.cloneNode(true): nextSibling);

        if (nextSibling.nextSibling) findNextSlings(nextSibling);
    }

    findNextSlings(node);
    return list;
}


export const findCursorOffsetFromLine = (selection) => {
    /**
     * Only use it with Caret selection
     * */
    const focusNode = selection.focusNode;
    const isFocusNodePTag = focusNode.nodeName==="P";


    if (isFocusNodePTag && focusNode.textContent.length===0) return 0;
    else if (isFocusNodePTag && focusNode.textContent.length!==0) return focusNode.textContent.length;

    let offset = selection.focusOffset;

    const calcPrevSibling = (node) => {
        if (node.nodeName === "P") return;
        if (node.parentNode.nodeName === "SPAN") node = node.parentNode;
        if (node.previousSibling) {
            offset += node.previousSibling.textContent.length;
            calcPrevSibling(node.previousSibling);
        }
    }

    calcPrevSibling(focusNode);

    return offset;
}


export const findRangeOffsetFromLine = (rangeContainer, containerOffset) => {
    let offset = containerOffset;

    const calcPrevSibling = (node) => {
        if (!["SPAN", "#text"].includes(node.nodeName)) return;
        if (node.parentNode.nodeName !== "P") return calcPrevSibling(node.parentNode);
        else if (node.previousSibling){
            offset += node.previousSibling.textContent.length;
            calcPrevSibling(node.previousSibling);
        }
    }

    calcPrevSibling(rangeContainer)
    return offset;
}


export const selectEditorFirstLine = (writingBoardRef) => {
    const selection = document.getSelection();

    selection.setPosition(writingBoardRef.current.firstChild, 0);
}


export const checkBlankText = (text) => {
    return !/[\S]/.test(text);
}


export const checkEditorHasFocus = (editorNode) => {
    if (!editorNode) return

    const selection = document.getSelection()

    if (selection.type === "None") return

    const checkParentNode = (node) => {
        if (!node.parentNode) return
        return editorNode === node.parentNode || checkParentNode(node.parentNode)
    }

    return checkParentNode(selection.focusNode)
}


export const checkLineContainsUniqueEl = (line) => {
    return line && line.hasChildNodes() && [...line.childNodes.values()].some(node => ["IMG", "IFRAME"].includes(node.nodeName));
}


export const checkLineContainsLinkEl = (line) => {
    return line && line.hasChildNodes() && [...line.childNodes.values()].some(node => ["A"].includes(node.nodeName));
}


export const checkLineContainsOnlyBr = (line) => {
    const list = [...line.childNodes.values()];

    return line.hasChildNodes() && !checkLineContainsUniqueEl(line) && list.every(node => !node.textContent) && list.some(node => node.nodeName==="BR");
}


export const checkSelectionWithinUniqueElLine = () => {
    const selection = document.getSelection();
    const range = selection.getRangeAt(0);

    const rangeStartLine = findLineContainer(range.startContainer);
    const rangeEndLine = findLineContainer(range.endContainer);
    const isUniqueElLine = checkLineContainsUniqueEl(rangeStartLine);


    return rangeStartLine===rangeEndLine && isUniqueElLine;
}


export const checkSelectionWithinLinkLine = () => {
    const selection = document.getSelection();
    const range = selection.getRangeAt(0);

    const rangeStartLine = findLineContainer(range.startContainer);
    const rangeEndLine = findLineContainer(range.endContainer);
    const isLinkLine = checkLineContainsLinkEl(rangeStartLine);


    return rangeStartLine===rangeEndLine && isLinkLine;
}


export const checkEditorContentElTypes = (nodes) => {
    const allowedEls = [
        "#text",
        "P",
        "SPAN",
        "IMG",
        "IFRAME",
        "BR",
        "B",
        "I",
        "FONT",
        "A"
    ];


    return [...nodes.values()].every(node => {
        const isNotAllowedEl = !allowedEls.includes(node.nodeName);
        const hasChildNodes = node.hasChildNodes();

        if (isNotAllowedEl) {
            return false;
        }
        else if (!isNotAllowedEl && hasChildNodes) {
            return checkEditorContentElTypes(node.childNodes);
        }
        else if (!isNotAllowedEl && !hasChildNodes) {
            return true;
        }
    })
}


export const setNewRange = (selection, rangeStartContainer, rangeStartOffset, rangeEndContainer, rangeEndOffset) => {
    const newRange = document.createRange();

    newRange.setStart(rangeStartContainer, rangeStartOffset);
    newRange.setEnd(rangeEndContainer, rangeEndOffset);

    selection.removeAllRanges();
    selection.addRange(newRange);
}


export const mergeDuplicatedStyles = (nodeList) => {
    const list = [];

    const allStylesMatch = (a, b) => {
        return a.style.color === b.style.color &&
            a.style.fontSize === b.style.fontSize &&
            a.style.fontWeight === b.style.fontWeight &&
            a.style.fontStyle === b.style.fontStyle
    };

    nodeList.forEach(node => {
        const lastItem = list.length>0 && list[list.length-1]

        switch (node.nodeName) {
            case "#text":
                if (lastItem && lastItem.nodeName === "#text") lastItem.textContent = lastItem.textContent + node.textContent;
                else list.push(document.createTextNode(node.textContent));
                break;
            case "SPAN":
                if (node.hasChildNodes() && node.firstChild.nodeName === "BR") {
                    list.push(node);
                    break;
                }
                if (!node.textContent) break;
                if (lastItem.nodeName === "SPAN" && allStylesMatch(lastItem, node)) lastItem.textContent = lastItem.textContent + node.textContent;
                else {
                    const span = document.createElement("span");
                    span.style.color = node.style.color;
                    span.style.fontSize = node.style.fontSize;
                    span.style.fontWeight = node.style.fontWeight;
                    span.style.fontStyle = node.style.fontStyle;
                    span.textContent = node.textContent;
                    list.push(span);
                }
                break;
            default:
                list.push(node)
                break;
        }
    })

    return list;
}

export const organizeNestedStyles = (lineContainer, childNodes) => {
    /**
     * Apply nested styles of child nodes to line container
     * And returns organized child nodes
     * */

    if (childNodes.some(node => ["IMG", "IFRAME"].includes(node.nodeName))) {
        lineContainer.removeAttribute("style");
        return childNodes;
    }


    const lineStyle = {};

    // Set current line container style object
    STYLE_.ATTRIBUTES.forEach(attr => {
        lineStyle[attr] = lineContainer.style[attr] || STYLE_.DEFAULT_OPTIONS[attr].value;
    })

    const allStylesEqual = (node1, node2) => {
        return STYLE_.ATTRIBUTES.every(attr => node1.style[attr] === node2.style[attr]);
    }


    // comparer:: A set of diff style attributes between child nodes and line container
    // in the end null:preserve current, false:diff, value:will be replacing current
    const comparer = {};

    STYLE_.ATTRIBUTES.forEach(attr => comparer[attr] = null);


    // Check duplicated styles along the child nodes
    childNodes.forEach(node => {
        switch (node.nodeName) {
            case "SPAN":
                STYLE_.ATTRIBUTES.forEach(attr => {
                    const style = node.style[attr];
                    const compare = comparer[attr];
                    if (style && compare && style !== compare) comparer[attr] = false;
                    else if (compare === null) comparer[attr] = style || lineStyle[attr];
                })
                break;
            case "#text":
                STYLE_.ATTRIBUTES.forEach(attr => {
                    const style = lineStyle[attr];
                    const compare = comparer[attr];
                    if (compare && style !== compare) comparer[attr] = false;
                    else if (compare === null) comparer[attr] = style;
                })
                break;
        }
    })

    // Sub line container style from comparer styles
    STYLE_.ATTRIBUTES.forEach(attr => {
        if (lineStyle[attr] === comparer[attr]) comparer[attr] = null;
    })


    const list = [];

    childNodes.forEach(node => {
        const _node = node.cloneNode(true);
        const lastItem = list[list.length-1] || null;

        if (_node.nodeName==="SPAN") {
            // Organize styles in SPAN from line containers
            Object.entries(comparer).forEach(entry => {
                const attr = entry[0];
                const diff = entry[1];
                const curr = _node.style[attr];
                const line = lineStyle[attr];

                if (diff && !curr) _node.style[attr] = lineStyle[attr];
                else if ((!diff && curr === line) || (diff && curr === diff)) _node.style[attr] = null;
            })

            const newNode = STYLE_.ATTRIBUTES.some(attr => _node.style[attr])? _node: [..._node.childNodes.values()].every(node => node.nodeName==="#text")? document.createTextNode(_node.textContent): _node.firstChild;

            switch (newNode.nodeName) {
                case "SPAN":
                    if (lastItem && lastItem.nodeName === "SPAN" && allStylesEqual(lastItem, newNode)) {
                        lastItem.textContent = lastItem.textContent + newNode.textContent;
                    } else list.push(newNode)
                    break;
                case "#text":
                    if (lastItem && lastItem.nodeName === "#text") lastItem.textContent = lastItem.textContent + newNode.textContent;
                    else list.push(newNode);
                    break;
                default:
                    list.push(newNode);
                    break;
            }
        } else list.push(_node);
    })


    const newLineStyle = {};
    STYLE_.ATTRIBUTES.forEach(attr => {
        const def = STYLE_.DEFAULT_OPTIONS[attr].value;
        const curr = lineStyle[attr];
        const compare = comparer[attr];

        if (compare && def !== compare) newLineStyle[attr] = compare;
        else if (!compare && def !== curr) newLineStyle[attr] = curr;
    })



    // Empty line container styles
    STYLE_.ATTRIBUTES.forEach(attr => lineContainer.style[attr] = null);

    // Set with new organized styles
    Object.entries(newLineStyle).forEach(entry => {
        lineContainer.style[entry[0]] = entry[1];
    })

    // Remove style attr from the line if its none
    if (STYLE_.ATTRIBUTES.every(attr => !lineContainer.style[attr])) lineContainer.removeAttribute("style");


    /**
     * Organize nested styles if whole line is wrapped by one SPAN
     * */
    const trimList = list.filter(node => node.textContent);

    if (trimList.length===1 && trimList[0].nodeName==="SPAN") {
        const nestingSpan = trimList[0];

        STYLE_.ATTRIBUTES.forEach(attr => {
            const spanStyle = nestingSpan.style[attr];
            const defStyle = STYLE_.DEFAULT_OPTIONS[attr].value;

            if (spanStyle) lineContainer.style[attr] = spanStyle===defStyle? null: spanStyle;
        });

        // Remove style attr from the line if its none
        if (STYLE_.ATTRIBUTES.every(attr => !lineContainer.style[attr])) lineContainer.removeAttribute("style");

        return nestingSpan.childNodes;
    } else {
        return list;
    }
}