import {requestUpdateTempPost} from "../../features/post/actions";
import router from "next/router";


let timeout;
const DELAY = 4000;



const delayExec = (func, delayMs=DELAY) => {
    clearTimeout(timeout);

    timeout = setTimeout(async () => {
        return func();
    }, delayMs);
}


export const cancelUpdatingPostAfterDelay = () => {
    clearTimeout(timeout);
}



export const updatePostAfterDelay = async (postState, updateTarget, updateValue, delay, routeOnAutoSaveFail) => {
    const post = postState;

    switch (updateTarget) {
        case "title":
            post.title = updateValue;
            break;
        case "content":
            post.content = updateValue;
            break;
        case "phrase":
            post.phrase = updateValue;
            break;
    }

    const updateCurrTempPost = async () => {
        const updateRes = await requestUpdateTempPost(post.postId, post.category, post.board, post.title, post.content, post.phrase);

        const {status, data} = updateRes;

        switch (status) {
            case 404:
                if (!routeOnAutoSaveFail) return;

                alert("등록 완료된 임시저장 글입니다.");

                const {postNumber} = data;

                await router.push(`/post/${post.category}/${post.board}/${postNumber}`);
                break
        }
    }

    await delayExec(()=>updateCurrTempPost(), delay);
}