



export const autoResizeTextarea = (textareaRef) => {
    const textarea = textareaRef.current;
    textarea.style.height = "auto";

    const scrollHeight = textarea.scrollHeight;

    textarea.style.height = `${scrollHeight}px`;
}