import convertImgToCanvas from "./convertImgToCanvas";


export const scaleCanvas = (canvas, scale) => {
    const scaledCanvas = document.createElement('canvas');
    scaledCanvas.width = canvas.width * scale;
    scaledCanvas.height = canvas.height * scale;

    scaledCanvas
        .getContext('2d')
        .drawImage(canvas, 0, 0, scaledCanvas.width, scaledCanvas.height);

    return scaledCanvas;
};

const optimizeImage = async (image, maxWidth=640, quality=0.9) => {
    let canvas = await convertImgToCanvas(image);

    if (canvas.width > maxWidth) {
        canvas = scaleCanvas(canvas, maxWidth / canvas.width);
    }

    const blob = await new Promise((resolve) => {canvas.toBlob(resolve, image.type, quality)})

    return {
        blob: blob,
        width: canvas.width,
        height: canvas.height
    }
};

export default optimizeImage;