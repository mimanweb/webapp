const convertImgToCanvas = async (image) => {
    const canvas = document.createElement('canvas');
    const img = document.createElement('img');

    // create img element from File object
    img.src = await new Promise((resolve) => {
        const reader = new FileReader();
        reader.onload = (e) => resolve(e.target.result);
        reader.readAsDataURL(image);
    });
    await new Promise((resolve) => {
        img.onload = resolve;
    });

    // draw image in canvas element
    canvas.width = img.width;
    canvas.height = img.height;
    canvas.getContext('2d').drawImage(img, 0, 0, canvas.width, canvas.height);

    return canvas;
};

export const readImgDimensions = async (image) => {
    const canvas = await convertImgToCanvas(image);

    return {width: canvas.width, height: canvas.height};
}


export default convertImgToCanvas;