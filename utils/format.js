export const checkNumeric = (str) => {
    return !isNaN(parseFloat(str))
}


export const parseFullUserInfo = (userId, nickname, ipAddress) => {
    return userId? `${nickname} (${userId})`: `${nickname} 비회원(${ipAddress})`
}


export const parseCommentCount = (count, displayZero, wrapWithBracket) => {
    let text = "";

    if (displayZero) {
        if (count>=100) text = "99+";
        else if (count>0) text = count;
        else text = "0";
    }
    else {
        if (count>=100) text = "99+";
        else if (count>0) text = count;
        else text = "";
    }

    if (wrapWithBracket && text) {
        text = "[" + text + "]";
    }

    return text;
}


export const parseRecentComicsListText = (listTxt) => {

    return !listTxt? []: listTxt.split(",").filter(txt => {
        const [comicsNumber, comicBookDataId] = txt.split("_");

        return comicsNumber && comicBookDataId && !/[^\d]/g.test(comicsNumber);
    }).map(txt => {
        const [comicsNumber, comicBookDataId] = txt.split("_");

        return {
            comicsNumber: Number(comicsNumber),
            comicBookDataId: comicBookDataId
        }
    });
}


export const parseComicBookLinkPath = (comics, comicBook) => {
    const {owner, index} = comicBook;

    const query = {translator: owner.userId};

    if (index.isShortStory) {
        query["short-story"] = index.shortStoryNumber;
    }
    else if (index.isVolume) {
        query.vol = index.volume;
        query.issue = index.issue;
    }
    else {
        query.issue = index.issue;
    }

    return {
        pathname: `/comics/book/${comics.number}/${comics.id}`,
        query: query
    }
}


export const parseComicBookEpText = (index) => {
    return index.isShortStory? "단편": index.isVolume? `issue ${index.issue} vol ${index.volume}`: `issue ${index.issue}`;
}