/**
 * Read user token info from the client request cookie
 * And write as an Authorization token cookie
 * */
export const readClientLoginToken = (req) => {
    if (!req.headers || !req.headers.cookie) return null;
    const cookie = req.headers.cookie;

    const matches = [...cookie.matchAll(/(ACCESS_TOKEN|REFRESH_TOKEN)=(.*?)(;\s|$)/g)]

    if (matches.length === 0) return null;
    else if (matches.length === 1) {
        return "Bearer " + JSON.stringify({
            [matches[0][1]]: matches[0][2]
        })
    } else {
        return "Bearer " + JSON.stringify({
            [matches[0][1]]: matches[0][2],
            [matches[1][1]]: matches[1][2]
        })
    }
}